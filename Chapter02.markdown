# Chapter 2 Xcodeの使い方を知る

## 2.1 [プロジェクトを作る Chapter2-1](Chapter02_1)

## 2.2 [Xcodeの画面について Chapter2-2](Chapter02_2)

## 2.3 [画面をデザインする:Interface Builder Chapter2-3](Chapter02_3)

## 2.4 [部品とプログラムをつなぐ:Assistant Editor Chapter2-4](Chapter02_4)

## 2.5 [プログラムを書く:SourceEditor Chapter2-5](Chapter02_5)

## 2.6 [シミュレータでテストする:Simulator Chapter2-6](Chapter02_6)