# Chapter 2-1  プロジェクトを作る

## 新規プロジェクトの作成
* Xcodeを起動すると、 **Welcome to Xcode** 画面が出ます
    * **Create a New Xcode project** をクリックします
* 既に Xcode が起動している場合は、File -> New -> Project を押します

## テンプレートの選択

* Choose a template for your new project と出たらテンプレートを選択します
    * iOS -> Application -> Single View Application を選択します

## プロジェクトの基本情報の入力

* プロジェクトの基本情報を入れます

 | 項目                    |内容                        |
 |------------------------|----------------------------|
 |Product Name            | Chapter04-2                |
 |Organization Name       |oecxxxx(ユーザ名)            |
 |Organization Identifier | jp.co.oec_o.oecxxxx        |
 |Language                | Swift                      |
 |Devices                 | iPhone                     |

    * 上のように入力したら **Next** を押します

## プロジェクトの保存先
* プロジェクトの保存先を入れます
    * 今回は user1¥kensyuを選択し、 **Create** を押します

* 以上で新しいプロジェクトが作成され、Xcode の画面が開きました




