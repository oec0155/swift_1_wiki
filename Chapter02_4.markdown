# Chapter 2-4 部品とプログラムをつなぐ：Assistant Editor

* storyboard 上でGUIで画面が作成できる事が分かったかと思いますが、この部品をプログラムで制御する必要があります。
* Macでは、この為に、以下の２つの機能があります

 | 名前        |機能                 |
 |------------|---------------------|
 |IBOutlet    | 部品オブジェクトのプログラム上での変数  |
 |IBAction    | 部品上で発生したイベントに対応して実行されるメソッド |

* storyboard を開いた状態で Assistant Editor 画面に変更してみてください。
* 左側が狭いので、ドキュメントアウトライン画面を閉じてください

## IBOutlet の作成
* テキスト 41 ページを参照して IBOutletを作成します

 ```swift
class ViewController: UIViewController {
    @IBOutlet weak var myLabel: UILabel!
 ```

上のようにできました


## IBAction の作成
* テキスト 42 ページを参照して IBActionを作成します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
    }
 ```

上のようにできました

## IBOutletやIBActionの確認
* IBOutletやIBActionが正しく設定されているか確認するには以下の２通り方法があります
    * controlを押しながら、部品をクリックする
    * コネクションビューを出して、部品を選択する
* これらの方法で、正しく設定されているか確認してみてください