# Chapter 2-5  プログラムを書く
* テキスト 43ページからの記述に従って、今作成した IBOutlet IBAction を使ってプログラムを作成してください

```swift
@IBAction func tapBtn(_ sender: UIButton) {
    label.text = "こんにちは"
}
```

上のようになります