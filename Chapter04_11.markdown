# 補足１　UIDatePicker: 日付や時刻を選択させたいとき
* テキストにはないのですが、業務では日付や時刻を入力する事はよくあります。その為の部品 UIDatePickerについて説明します

* 新規プロジェクト Chapter04-11 を作成してください

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート           | Single View Application |
   |Product Name         | Chapter04-11            |
   |Language             | Swift                   |
   |Devices              | iPhone                  |

* DatePicker をドラッグドロップして貼り付けてください

 | 部品         | 制約                            |
 |-------------|---------------------------------|
 |日付ピッカー   | 上左右からの距離 0 (マージン有り)高さ 200   |


* アトリビュートは以下のような物があります


　　![Chapter04_11_1](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/f8a3e5169f/Chapter04_11_1.png)


* アトリビュートの内容

 | アトリビュート     | 内容                             |
 |-----------------|----------------------------------|
 |Mode             |日付と時刻、日付のみ、時刻のみ、カウントダウンタイマーが設定できます |
 |Interval         |最小単位の指定　     |
 |Date             |初期表示、又、現在設定されている値   |
 |Constraints Minimum Date | 設定できる最小の値   |
 |　　　　　Maximum Date | 設定できる最大の値   |

    * 注　日付ピッカーでは秒は操作出来ません

* アトリビュートを設定して実行して見てください

　　![Chapter04_11_3](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/d042fb3644/Chapter04_11_3.png)


## プログラムから制御してみます

* 日付ピッカーのIBOutletを作成します

 | 部品         | IBOutlet Name                   |
 |-------------|---------------------------------|
 |日付ピッカー   | datePicker                      |

* 日付ピッカーの下にボタンを追加してください

 | 部品         | 制約                            |
 |-------------|---------------------------------|
 |ボタン        | 水平方向中央に配置、上からの距離 20  |

* ボタンにIBActionを作成

 | 部品         | IBAction                        |
 |-------------|---------------------------------|
 |ボタン        | tapBtn                          |

* 次のようにコーディングしてみてください
   * viewDidLoad()メソッド


 ```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let date = NSDate()        // 現在時刻の取得
        let calendar = NSCalendar.currentCalendar()
        let date0 =  calendar.dateBySettingUnit(.Second, value: 0, ofDate: date, options: NSCalendarOptions(rawValue: 0))    // 秒を0に設定
        datePicker.date = date0!
    }
 ```


    * tapBtn()メソッド

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
        let df = NSDateFormatter()
        df.dateFormat = "yyyy年MM月dd日hh時mm分ss秒"
        let str1 = df.stringFromDate(datePicker.date)
        print("日付=\(str1)")
    }
 ```

* 動作確認してみてください
    * 日付の処理は少し面倒ですが、秒を 0 にしておかないと、ずっとその秒が使われていきます。




