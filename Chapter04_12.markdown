# 補足２ UIPickerView: 複数の値から１つを選択させるとき

* テキストにはないのですが、ピッカービューもよく使われます

* UIDatePickerに似ていますが、ピッカーにいろいろな値を表示して１つを選択させる事ができます

* 新規プロジェクト Chapter04-12 を作成してください

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート           | Single View Application |
   |Product Name         | Chapter04-12            |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* PickerView をドラッグドロップして貼り付けてください

 | 部品         | 制約                            |
 |-------------|---------------------------------|
 |ピッカービュー | 上左右からの距離 0 (マージン無し) 　 |

* ピッカービューのIBOutletを作成します

 | 部品         | IBOutlet Name                   |
 |-------------|---------------------------------|
 |ピッカービュー  | pickerView                      |

* プロトコル
    * 今までの部品はIBOutletとIBActionだけで済んでいましたが、複雑な部品になると、プロトコルを使うようになります。
    * ピッカービューは複数の値を表示したり、その値を選択したりする為に以下のプロトコルを使います
        * UIPickerViewDataSource
        * UIPickerViewDelegate
    * このプロトコルを実装したクラスをピッカービューに設定します
    * このプロトコルを実装したクラスは、ピッカーの要求に応じて様々な処理を行います
    * 通常は ViewController にこのプロトコルを実装させます

## UIPickerViewの dataSource, delegate プロトコルの設定
* dataSource, delegate を ViewController に設定する
    * ピッカービューの上で **control + クリック** して上部の左端のアイコン(View Controller)にドラッグドロップします
    * dataSource と delegate が出るので、それぞれを選択します（２回行う）
    * ViewControllerのソースを開き、ViewControllerのクラス定義に UIPickerViewDataSource,UIPickerViewDelegate プロトコルを定義します

 ```swift
class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
 ```

    上のようになります

* プロトコルで定義されているメソッドを実装します
    * 最初に表示用のデータを定義します

    ```swift
    class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
        @IBOutlet weak var pickerView: UIPickerView!
        let tea_list = ["ダージリン", "アールグレイ", "アッザム", "オレンジペコ"]
    ```

    * dataSource のメソッド
        * ピッカーの列数を返すメソッド

        ```swift
        // ピッカービューの列数
        func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
            return 1
        }
        ```

        * ピッカービューの行数を返すメソッド

        ```swift
        // ピッカービューの行数
        func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return tea_list.count
        }
        ```

    * delegateのメソッド
        * データの内容を返すメソッド

        ```swift
        // ピッカービューに表示する文字列
        func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return tea_list[row]
        }
        ````

        * ピッカービューの行が選択された時のメソッド

        ```swift
        // ピッカービューで行が選択された時に呼ばれる処理
        func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            print("選択されたのは、\(row)行目で、\(tea_list[row])です")
        }
        ```

* できたら実行してみてください

## 選択結果を取得する
* ボタンを下に追加してください


 | 部品         | 制約                            |
 |-------------|---------------------------------|
 |ボタン        |水平の中央に位置、上からの距離 20     |

* ボタンのIBActionを作成します

 | 部品         | IBAction Name                   |
 |-------------|---------------------------------|
 |ボタン        | tapBtn                          |

* ボタンのIBActionに以下のように記述します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
        print("今選択さているのは\(tea_list[pickerView.selectedRowInComponent(0)])")
    }
 ```

    * 引数の 0 は列番号を示します

* できたら実行してみてください





