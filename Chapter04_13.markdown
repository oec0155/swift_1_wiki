# 補足3　UIAlertController: アラートやアクションシートを表示する
* 業務で良く使う、アラートとアクションシートについて説明します
* アラート
    * 画面の中央にポップアップします
    * ＯＫ、キャンセル、どちらかを選択します
* アクションシート
    * 画面の下からせり出してきます
    * 複数の選択候補の中から１つを選びます
* プログラムとしては、まったく同じでスタイルがアラートかアクションシートかを指定するだけです

## サンプルの作成
* 新規プロジェクトChapter04_13 を作成します

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート         　 | Single View Application |
   |Product Name         | Chapter04-13            |
   |Language             | Swift                   |
   |Devices              | iPhone                  |

## アラート

* アラートやアクションはGUIで作成するのではなく、プログラムで生成します

* Button をドラッグドロップして貼り付けてください

 | 部品         | 制約                                |
 |-------------|-------------------------------------|
 |ボタン　　　　　　 | 水平方向の中央に配置、上からの距離 20  |

* ボタンのIBActionを作成します

 | 部品         | IBAction Name                   |
 |-------------|---------------------------------|
 |ボタン　　　　　  | tapBtn                          |

* tapBtn メソッドに以下のように記述します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
        // アラートを作る
        let alertController = UIAlertController(
            title: "タイトル",
            message: "メッセージ",
            preferredStyle: .Alert
        )
        // OKボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "OK",
                style: .Default,
                handler: { (action) in  print("OK") }
            )
        )
        // キャンセルボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .Cancel,
                handler: { (action) in print("キャンセル") }
            )
        )
        // 削除ボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "削除",
                style: .Destructive,
                handler: { (action) in print("削除") }
            )
        )
        // アラートを表示する
        presentViewController(alertController, animated: true, completion: nil)   
    }
 ```

* UIAlertControllerStyleには以下の２種類があります
    * .Alert　　　　　アラートを作成する
    * .ActionSheet　　アクションシートを作成する

* UIAlertActionStyle には以下の３種類がある
    * .Default　　　　OKボタン
    * .Cancel　　　　キャンセルボタン
    * .Destructive　　破壊的な処理をするボタン

## アクションシート

* Button をもう１つドラッグドロップして貼り付けてください

 | 部品         | 制約                              |
 |-------------|-----------------------------------|
 |ボタン　　　　　| 水平方向の中央に配置、上からの距離 20  |

* ボタンのIBActionを作成します

 | 部品         | IBAction Name                   |
 |-------------|---------------------------------|
 |ボタン　　　　  | tapActionBtn                          |

* tapBtn メソッドに以下のように記述します

 ```swift
    @IBAction func tapActionBtn(sender: AnyObject) {
        // アクションシートを作る
        let alertController = UIAlertController(
            title: "タイトル",
            message: "メッセージ",
            preferredStyle: .ActionSheet
        )
        // アクション１ボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "アクション１",
                style: .Default,
                handler: { (action) in  print("アクション１") }
            )
        )
        // アクション２ボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "アクション２",
                style: .Default,
                handler: { (action) in  print("アクション２") }
            )
        )
        // キャンセルボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .Cancel,
                handler: { (action) in print("キャンセル") }
            )
        )
        // 削除ボタンを追加
        alertController.addAction(
            UIAlertAction(
                title: "削除",
                style:  .Destructive,
                handler: { (action) in print("削除") }
            )
        )
        // アラートを表示する
        presentViewController(alertController, animated: true, completion: nil)

    }
 ```

* Alertと同じです。UIAlertControllerStyle が　.ActionSheet になるだけです
* 動作確認してみてください

