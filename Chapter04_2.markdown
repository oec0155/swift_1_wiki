# Chapter 4-2 UILabel: ちょっとした文字を表示させたいとき
* テキスト 119ページからを参照してください

* Chapter 2 で作成したサンプルを修正してみてください

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
        myLabel.textColor = UIColor.blueColor()
        myLabel.backgroundColor = UIColor.cyanColor()
        myLabel.textAlignment = .Center
        myLabel.font = UIFont.systemFontOfSize(20)
        myLabel.numberOfLines = 0
        myLabel.text = "こんにちは"
        print(myLabel.text)
        if let text = myLabel.text {
            print(text)
        }
    }
 ```

* できたら実行してみて結果を確認してください
