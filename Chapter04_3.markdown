# Chapter 4-3 UIButton: ユーザが押して何かをさせたいとき
* テキストを参照してください。
* Chapter 2 で作成したサンプルのボタンを修正してみます
* プログラムからボタンを制御するには、ボタンのIBOutletが必要です。

 | 部品     | IBOutlet名   |
 |----------|--------------|
 |UIButton  | myButton    |

* viewDidLoad() メソッドでボタンの設定をします

```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myButton.setTitle("押してね", forState:UIControlState.Normal)
        myButton.enabled = true
        myButton.selected = true
    }
```

上のようになります

* できたら実行してみてください


