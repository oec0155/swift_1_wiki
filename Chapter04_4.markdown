# Chapter 4-4 UISwitch: オンかオフかを選択させたいとき
* テキストを参照してください。
* Chapter 2 で作成したサンプルのボタンの下に Switch を追加してください

 | 部品        | 制約                           |
 |------------|--------------------------------|
 |スイッチ      |水平方向のセンター、上からの距離 20  |

* IBActionの作成

 | 部品        | Name         |Type      | Event          |
 |------------|--------------|----------|----------------|
 |スイッチ      | changeSwitch |UISwitch  | Value Changed  |

* IBActionに以下のように記述

 ```swift
    @IBAction func changeSwitch(sender: UISwitch) {
        if sender.on == true {
            print("スイッチON")
        } else {
            print("スイッチOFF")
        }
    }
 ```

* 実行してみてください。スイッチを操作するたびに以下のように出ます。

 ```
スイッチOFF
スイッチON
スイッチOFF
スイッチON
 ```

* しかし、実際には Switch はIBActionで操作するよりもIBOutletで値を読み出す事の方が多いです。
* IBOutletの作成

 | 部品        | Name                 |
 |------------|----------------------|
 |スイッチ      | mySwitch            |


 ```swift
@IBOutlet weak var mySwitch: UISwitch!
 ```

* ボタンのIBActionを以下のように修正します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
        myLabel.textColor = UIColor.blueColor()
        myLabel.backgroundColor = UIColor.cyanColor()
        myLabel.textAlignment = .Center
        myLabel.font = UIFont.systemFontOfSize(20)
        myLabel.numberOfLines = 0
        myLabel.text = "こんにちは"
        print(myLabel.text)
        if let text = myLabel.text {
            print(text)
        }
        if mySwitch.on == true {
            print("スイッチはONです")
        } else {
            print("スイッチはOFFです")
        }
    }
 ```

* 実行してみてください

 ```
スイッチはONです
スイッチOFF
スイッチはOFFです
スイッチはOFFです
スイッチON
スイッチはONです
 ```

    * 上のように出ればOKです