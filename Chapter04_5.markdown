# Chapter 4-5 UISlider: スライドして値を入力させたいとき
* テキストを参照してください
* Chapter 2 で作成したサンプルに Slider を追加します

 | 部品        | 制約                           |
 |------------|--------------------------------|
 |スライダー    |上からの距離 20、左右からの距離 0   |

* アトリビュートインスペクタで最大、最小値を設定します

 | 部品        | Value                         |
 |------------|--------------------------------|
 |スライダー    |Minum 0,Maximum 100,Current 20  |

* IBActionの作成

 | 部品        | Name         |Type      | Event          |
 |------------|--------------|----------|----------------|
 |スライダー    | changeSlider |UISlider  | Value Changed  |

* IBActionに以下のように記述

 ```swift
    @IBAction func changeSlider(sender: UISlider) {
        myLabel.text = "値=\(sender.value)"
    }
 ```

* 動作確認してみてください

* 次に、ボタンを押した時に値を読み取るサンプルを作成します

* IBOutletを作成します

 | 部品        | Name                 |
 |------------|----------------------|
 |スライダー    | mySlider             |

 ```swift
@IBOutlet weak var mySlider: UISlider!
 ```


* スイッチのIBActionを以下のように直します

 ```swift
    @IBAction func changeSwitch(sender: UISwitch) {
        if sender.on == true {
            print("スイッチON")
            mySlider.continuous = true
        } else {
            print("スイッチOFF")
            mySlider.continuous = false
        }
    }
 ```

* 又、tapBtn() を以下のように修正します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
//        myLabel.textColor = UIColor.blueColor()
//        myLabel.backgroundColor = UIColor.cyanColor()
//        myLabel.textAlignment = .Center
//        myLabel.font = UIFont.systemFontOfSize(20)
//        myLabel.numberOfLines = 0
//        myLabel.text = "こんにちは"
//        print(myLabel.text)
//        if let text = myLabel.text {
//            print(text)
//        }
        if mySwitch.on == true {
            print("スイッチはONです")
        } else {
            print("スイッチはOFFです")
        }
        print("スライダーの値=\(mySlider.value)")
    }
 ```

* できたら実行してみてください
    * スイッチをON/OFF してスラーダーを動かしてください。
    * ボタンを押してみて下さい



