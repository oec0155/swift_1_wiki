# Chapter 4-6 UITextField: １行のテキストを入力させたいとき
* テキストを参照して下さい
* Chapter 2 で作成したサンプルに TextField を追加します

 | 部品              | 制約                           |
 |------------------|--------------------------------|
 |テキストフィールド    |上からの距離 20、左右からの距離 0   |

* アトリビュートインスペクタに以下の設定を行って下さい

 | 部品              | Placeholder                    |
 |------------------|--------------------------------|
 |テキストフィールド    |文字を入力して下さい             |

* このままで実行してみて下さい
    * テキストを入力した後、キーボードが消えないので邪魔になります。
    * リターンキーでキーボードを消すには、以下の IBAction を設定する必要がありま す


* IBAction の作成

 | 部品              | Name         |Type        | Event           |
 |------------------|--------------|------------|-----------------|
 |テキストフィールド    | tapReturnKey |UITextField |Did End On Exit  |

* 実行してみてください
    * 今度はリターンキーでキーボードが消えました。
    * TextField ではこのようにキーボードを消すためだけにIBActionを定義します

* IBOutlet を作成します

 | 部品              | Name                 |
 |------------------|----------------------|
 |テキストフィールド    | myTextField          |

 ```swift
@IBOutlet weak var myTextField: UITextField!
 ```

* tapBtn() を以下のように修正します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
//        myLabel.textColor = UIColor.blueColor()
//        myLabel.backgroundColor = UIColor.cyanColor()
//        myLabel.textAlignment = .Center
//        myLabel.font = UIFont.systemFontOfSize(20)
//        myLabel.numberOfLines = 0
//        myLabel.text = "こんにちは"
//        print(myLabel.text)
//        if let text = myLabel.text {
//            print(text)
//        }
        if mySwitch.on == true {
            print("スイッチはONです")
        } else {
            print("スイッチはOFFです")
        }
        print("スライダーの値=\(mySlider.value)")
        print("テキストフィールド=\(myTextField.text!)")
    }
 ```

* 実行してみてください









