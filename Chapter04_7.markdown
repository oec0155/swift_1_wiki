# Chapter 4-7 UITextView: 長いテキストを表示したり、入力させたいとき
* テキストを参照してください
* Chapter 2 で作成したサンプルに TextView を追加してください

 | 部品           | 制約                           |
 |---------------|--------------------------------|
 |テキストビュー    |上からの距離 20、左右からの距離 0、高さ 50 |

* 背景が白でわかりにくいので、Background を 黄色にしてください

* 実行してみてください
    * TextFieldと同じでやはりキーボードが閉じません
    * TextFieldと違い、リターンキーでキーボードを閉じる事は出来ません。リターンも改行でデータの一部として必要だからです。
    * このように、TextViewでは閉じるボタンが必要となります。

* テキストビューにIBOutlet を作成します

 | 部品         | Name                 |
 |-------------|----------------------|
 |テキストビュー | myTextView           |

 ```swift
@IBOutlet weak var myTextView: UITextView!
 ```


* tapBtn() を次のように修正します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
//        myLabel.textColor = UIColor.blueColor()
//        myLabel.backgroundColor = UIColor.cyanColor()
//        myLabel.textAlignment = .Center
//        myLabel.font = UIFont.systemFontOfSize(20)
//        myLabel.numberOfLines = 0
//        myLabel.text = "こんにちは"
//        print(myLabel.text)
//        if let text = myLabel.text {
//            print(text)
//        }
//        if mySwitch.on == true {
//            print("スイッチはONです")
//        } else {
//            print("スイッチはOFFです")
//        }
//        print("スライダーの値=\(mySlider.value)")
//        print("テキストフィールド=\(myTextField.text!)")
        myTextView.resignFirstResponder()
        print("テキストビュー=\(myTextView.text)")
    }
 ```

* 実行してみます
    * ボタンを押すとキーボードが閉じればOKです


