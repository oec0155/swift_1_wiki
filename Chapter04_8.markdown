# Chapter 4-8 UIImageView: 画像を表示させたいとき
* テキストを参照してください
* 新規プロジェクト Chapter04-8 を作成してください

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート           | Single View Application |
   |Product Name         | Chapter04-8             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* ImageView をドラッグドロップして貼り付けてください

 | 部品         | 制約                            |
 |-------------|---------------------------------|
 |イメージビュー  | 上左右下からの距離 0 (マージン無し)  |

* イメージビューのIBOutletを作成します

 | 部品         | IBOutlet Name                   |
 |-------------|---------------------------------|
 |イメージビュー  | imageView                      |



* 画像データをプロジェクトに含めるには、２つの方法があります。
    * 直接画像ファイルをプロジェクトに入れて使う
    * AssetCatalog(アセットカタログ)に入れて使う
    
## 画像データを直接プロジェクトに入れて使う方法

* 画像データ(berry.png)をつまんでプロジェクト内に落としてください
    * ポップアップが出たら、Copy Items if needed にチェックが入っている事を確認してFinish を押してください

　　![Chapter04_8_1](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/b81c2b7261/Chapter04_8_1.png)

* viewDidLoad()メソッドの修正
    * 以下のようにviewDidLoad()メソッドを修正して実行してみてください


 ```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imageView.image = UIImage(named: "berry.png")        
    }
 ```

* 画像データが歪んで表示されます。
    * UIImageView のアトリビュートインスペクタで View -> Mode を変更して正しく表示されるようにしてください

 | Mode           |処理方法                                       |
 |----------------|----------------------------------------------|
 |Scale To Fill   |画像を画面いっぱいに引き延ばす                     |
 |Aspect Fit      |縦横比を保って画像全体が見えるように引き延ばす        |
 |Aspect Fill     |縦横比を保ってビューに一杯に表示されるように引き延ばす |

## AssetCatalog(アセットカタログ) を使う方法
* テキストを参照してberry というイメージセットを作成して、画像ファイルを入れます。


 |格納場所    | 画像ファイル名   |
 |----------|----------------|
 | 1x       | berry1.png     |
 | 2x       | berry2.png     |
 | 3x       | berry.png      |


+ viewDidLoad()メソッドを以下のように修正します

 ```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        imageView.image = UIImage(named: "berry.png")
        imageView.image = UIImage(named: "berry")
    }
 ```


* できたら動作確認してみてください









