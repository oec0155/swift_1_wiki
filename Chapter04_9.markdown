# Chapter4-9,10 じゃんけんアプリを作ろう
* テキストに従って作成します
* 新規プロジェクト Chapter04-9 を作成する
* プロジェクトに画像ファイルを追加します
* 作りやすいように、画面サイズをiPhone SE のサイズ（4 inch）に切り替えます
* イメージビューを２つ配置します
* 最初に表示させるパーの画像を設定します
* 「最初はグー」のボタンを配置します
* メッセージを表示するラベルを配置します
* 「グー」「チョキ」「パー」ボタンを配置します
* それでは、どのようにできたかを、実行して確認しましょう
* 今度は、違う画面で試してみましょう
* オートレイアウトの例題のところは既に説明済みなので、飛ばしても良いです。

## AutoLayoutを設定する

* コンピュータ側のUIImageViewを設定します
* プレイヤー側のUIImageViewも設定します
* 「最初はグー」ボタンは、上のImageViewから20の位置に固定します
* どのようにできたかを、実行して確認しましょう
* メッセージ表示のラベル位置を固定します
* ３つのボタンをStackViewを使ってまとめます
* StackViewのAutoLayoutを選択します
* StackViewのAutoLayoutを設定します
* ３つのボタンの幅を均等にして、隙間を20にします
* 画面に黄色いガイドラインが表示されて、制約と表示がずれていると思いますので、制約に合わせます
* どのようにできたかを、実行して確認しましょう

## プログラム製作編

### 手が向かい合うように表示するプログラム

* アシスタントエディターに切り換えます
* ImageViewをアウトレット接続して、名前をつけます

```swift
    @IBOutlet weak var computerImageView: UIImageView!
    @IBOutlet weak var playerImageView: UIImageView!
```

* ソースエディタに切り換えます
* プログラムで上のImageViewを180度回転させます

```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // 180度のラジアンを求めます
        let angle:CGFloat = CGFloat((180.0 * M_PI) / 180.0 )
        // イメージビューを回転させます
        computerImageView.transform = CGAffineTransformMakeRotation(angle)        
    }
```

* どのようにできたかを、確認しましょう
    * 実行してみます

### 「最初はグー」ボタンを押すと、グーを表示するプログラム

* アシスタントエディターに切り換えます
* 「最初はグー」ボタンをプログラムに接続します

```swift
    /*
    * 最初はグーボタン押下時処理
    */
    @IBAction func tapStart(sender: AnyObject) {
    }
```

* メッセージ用ラベルをプログラムに接続します

```swift
    @IBOutlet weak var messageLabel: UILabel!
```

* ソースエディタに切り換えます

* グー表示にするプログラムを作ります

```swift
    /*
    * 最初はグーボタン押下時処理
    */
    @IBAction func tapStart(sender: AnyObject) {
        computerImageView.image = UIImage(named: "gu.png")
        playerImageView.image = UIImage(named: "gu.png")
        messageLabel.text = "じゃんけん！"
    }
```

* どのようにできたかを、実行して確認しましょう
    * 実行してみてください

## 「グー」「チョキ」「パー」ボタンを押すと、その手を表示するプログラム

* アシスタントエディターに切り換えます

* 「グー」「チョキ」「パー」ボタンをプログラムに接続します
    * それぞれのIBActionを作成します

```swift
    /*
    *   グーボタン押下時処理
    */
    @IBAction func tapGu(sender: AnyObject) {
    }
    /*
    *   チョキボタン押下時処理
    */
    @IBAction func tapChoki(sender: AnyObject) {
    }
    /*
    *   パーボタン押下時処理
    */
    @IBAction func tapPa(sender: AnyObject) {
    }
```
 
* ソースエディタに切り換えます

* 「グー」「チョキ」「パー」のプログラムを作ります

```swift
    /*
    *   グーボタン押下時処理
    */
    @IBAction func tapGu(sender: AnyObject) {
        playerImageView.image = UIImage(named: "gu.png")
    }
    /*
    *   チョキボタン押下時処理
    */
    @IBAction func tapChoki(sender: AnyObject) {
        playerImageView.image = UIImage(named: "choki.png")
    }
    /*
    *   パーボタン押下時処理
    */
    @IBAction func tapPa(sender: AnyObject) {
        playerImageView.image = UIImage(named: "pa.png")
    }
```

* どのようにできたかを、実行して確認しましょう
    * 実行してみてください

## コンピュータ側の手をランダムに変えるプログラム

* アプリでランダム（乱数）を扱う準備をします
    * import 文の追加

```swift
import GameplayKit
```

オブジェクトの作成

```swift
    let randomSource = GKARC4RandomSource()
```

* コンピュータ側の手をランダムに表示するメソッドを作ります

    * 乱数の生成

```
    let 変数 = randomSource.nextIntWithUpperBound(最大値)
```

```swift
    /*
    *   コンピュータの手を実行する
    */
    func doComputer() {
        let computer = randomSource.nextIntWithUpperBound(3)
        switch computer {
        case 0:
            computerImageView.image = UIImage(named: "gu.png")
        case 1:
            computerImageView.image = UIImage(named: "choki.png")
        case 2:
            computerImageView.image = UIImage(named: "pa.png")
        default:
            break
        }
    }
```

* 「グー」「チョキ」「パー」のボタンを押したあとで、メソッドを呼び出します

```swift
    /*
    *   グーボタン押下時処理
    */
    @IBAction func tapGu(sender: AnyObject) {
        playerImageView.image = UIImage(named: "gu.png")
        doComputer()
    }
    /*
    *   チョキボタン押下時処理
    */
    @IBAction func tapChoki(sender: AnyObject) {
        playerImageView.image = UIImage(named: "choki.png")
        doComputer()
    }
    /*
    *   パーボタン押下時処理
    */
    @IBAction func tapPa(sender: AnyObject) {
        playerImageView.image = UIImage(named: "pa.png")
        doComputer()
    }
```

* どのようにできたかを、実行して確認しましょう
    * 実行してみてください

## 結果を判定して、表示するプログラム

* 判定プログラムを追加します

```swift
    /*
    *    勝敗判定
    */
    func hantei(player:Int, computer:Int) -> String {
        switch computer {
        case 0:
            switch player {
            case 0: return "あいこ"
            case 1: return "あなたの負け"
            case 2: return "あなたの勝ち"
            default: break
            }
        case 1:
            switch player {
            case 0: return "あなたの勝ち"
            case 1: return "あいこ"
            case 2: return "あなたの負け"
            default: break
            }
        case 2:
            switch player {
            case 0: return "あなたの負け"
            case 1: return "あなたの勝ち"
            case 2: return "あいこ"
            default: return ""
            }
        default: break
        }
        return ""
    }
```


```swift
    /*
    *   コンピュータの手を実行する
    */
    func doComputer(player:Int) {
        let computer = randomSource.nextIntWithUpperBound(3)
        var msg = ""
        switch computer {
        case 0:
            computerImageView.image = UIImage(named: "gu.png")
            msg = hantei(player, computer: computer)
        case 1:
            computerImageView.image = UIImage(named: "choki.png")
            msg = hantei(player, computer: computer)
        case 2:
            computerImageView.image = UIImage(named: "pa.png")
            msg = hantei(player, computer: computer)
        default:
            break
        }
        messageLabel.text = msg
    }
```

* いよいよ完成です。実行してみましょう
    * 実行して確認してください




