# Chater 5-2  Webサイトを表示する

## 新規プロジェクトの作成 
* 新規プロジェクト Chapter05-2 を作成します


   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-2             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |

## 画面の作成

* WebView を ViewController にドラッグドロップします

   | 部品              | コンポ―ネント        |制約                            |
   |-------------------|-------------------|--------------------------------|
   |ウェブビュー          |WebView            |上左右下からの距離 0 (マージン無し) |


* IBOutletの作成
 
   | 部品              | IBOutlet        |
   |-------------------|-------------------|
   |ウェブビュー          |webView            |


## プログラムの作成

* UIWebViewでWeb上のページを表示するには以下のようにします
   * URLオブジェクトを作る

 ```swift
    let myURL = NSURL(string: "https://www.apple.com")
 ```

   * URLリクエストを作る

 ```swift
    let myURLReq = NSURLRequest(URL: myURL)
 ```

   * UIWebView の loadRequestメソッドで、Webビューに表示する

 ```swift
    webView.loadRequest(myURLReq)
 ```

   * では、ViewController の viewDidLoad()メソッドに以下の様にコーディングします

 ```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let myURL = NSURL(string: "https://www.apple.com") {
            let myURLReq = NSURLRequest(URL: myURL)
            webView.loadRequest(myURLReq)
        }
    }
 ```

* できたら実行してください

* 問題なく表示されたら、URL を http://www.pref.okayama.jp に変更して実行してみてください

* うまく表示されないと思います。これは次に述べる、iOS9以降で導入された、セキュリティの機能の為です。


## ATS(App Transfer Security)について
 * iOS9以降で実装
 * httpの接続はできない、httpsでの接続のみ可能
 * TLS v1.2 以上

 * 上記の条件を満たさない接続先はエラーとなります（テキスト187頁参照）


## 全てのページをHTTP通信できるようにする方法

* Appleとしては非推奨なので、実際には行う事は勧めませんが、テストで一時的に設定する方法を説明します。

* info.plist を開きます
* Information Repository List の上で control+クリックして Add Raw を選ぶ
    * ポップアップが出るので、App Transport Security Settings を選択する
* App Transport Security Settings を選択して、 Type を Dictionary にする
* App Transport Security Settings の上で、control+クリックして Add Raw を選ぶ
    * ポップアップが出るので、Allow Arbitary Loads を選択する
* Allow Arbitary Loads を選択して、 Type を Boolean とする
* Value を YES にする

* できたら実行してみてください


## SFSafariViewControllerの利用
 * アプリから、Safariを呼び出す事ができます
 * 新規プロジェクトをChapter05-2-1で作成してください

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-2-1           |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* Main.storyboardにボタンをドラッグドロップします


   | 部品       | コンポ―ネント     |制約                                               |
   |------------|----------------|---------------------------------------------------|
   |ボタン       | Button         |Horizontally in Container にチェック、上からの距離 40  |


### プログラムの作成

* SFSafariViewControllerの利用方法
    * SafariServicesのインポート

 ```swift
import SafariServices
 ```

    * SFSafariViewControllerDelegateプロトコル の設定
        * ViewController に　上記のプロトコルを定義します

 ```swift
class ViewController: UIViewController, SFSafariViewControllerDelegate {
 ```

    * NSURLオブジェクトの作成

 ```swift
    let url = NSURL(string: "http://www.pref.okayama.jp")
 ```


    * SafariViewControllerの作成

 ```swift
    let vc = SFSafariViewController(URL: url, entersReaderIfAvailable: true)
 ```

    * SFSafariViewControllerDelegateプロトコル を ViewController にセット

 ```swift
    vc.delegate = self
 ```

    * SafariViewControllerの表示

 ```swift
    presentViewController(vc, animated: true, completion: nil)
 ```


    * SafariViewControllerDelegate のメソッドの実装

 ```swift
    // SFSafariViewControllerが閉じられた時のメソッド
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        print("Close")
    }
 ```



   * それでは、ボタンのIBActionを作成し、以下のように記述します

 ```swift
    @IBAction func tapBtn(sender: AnyObject) {
        if let url = NSURL(string: "http://www.pref.okayama.jp") {
            let vc = SFSafariViewController(URL: url, entersReaderIfAvailable: true)
            vc.delegate = self
            presentViewController(vc, animated: true, completion: nil)
        }
    }
 ```

    * Delegateのメソッドを作成します

 ```swift
    // SFSafariViewControllerが閉じられた時のメソッド
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        print("Close")
    }
 ```

    * できたら実行してみて下さい
        * 左上の完了ボタンを押すと、SafariViewは閉じて、最初の画面に戻ります。
