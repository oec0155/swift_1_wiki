# Chapter 5-3 Webの画像データをダウンロードする
* UIImageViewはWeb上の画像を表示する事もできます
* しかし、ATSにより、セキュリティの設定レベルが厳しくなっています

## サンプルの作成
* 新規プロジェクト Chapter05-3 を作成します

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート           | Single View Application |
   |Product Name         | Chapter05-3             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* ImageViewwをViewControllerにドラッグドロップします

* ImageViewに制約の追加

 | 部品        | 制約                            |
 |------------|--------------------------------|
 |イメージビュー | 上左右下からの距離 0 (マージン無し) |

* View の Mode 値の設定

 | 部品        | アトリビュートインスペクタ           |
 |------------|----------------------------------|
 |イメージビュー | View - Mode -> Aspect Fit に設定 |


* イメージビューからIBOutletを作成します

 | 部品        | IBOutlet       |
 |------------|-----------------|
 |イメージビュー | imageViwe      |


* Web上から画像をダウンロードして表示するには以下のようにします
* URLオブジェクトを作る

 ```swift
    url = NSURL(string: "http://www.ymori.com/itest/sample.jpg")
 ```

* 指定したURLからデータを読み込んで、生データを作る

 ```swift
    let data = NSData(contentsOfURL: url)
 ```

* 生データからイメージビューに表示する

 ```swift
    imageView.image = UIImage(data: data)
 ```

* ViewDidLoad()メソッドは以下の様になります

 ```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let url = NSURL(string: "http://www.ymori.com/itest/sample.jpg") {
            if let data = NSData(contentsOfURL: url) {
                imageView.image = UIImage(data: data)
            }
        }  
    }
 ```

* ATSによりエラーとなりますので、セキュリティ解除の設定を行います

    * App Transport Security Settings
        * Allow Arbitary loads
            * YES



* できたら実行してみてください