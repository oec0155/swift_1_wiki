# Chapter 5-4 テキストデータをダウンロード する

* 先ほどは画像データをダウンロードして表示しましたが、テキストデータをダウンロードする事も同じようにできます
* しかし、先ほどのNSData(contentsOfURL)メソッドは同期通信でのダウンロードでした
* しかし、同期通信では通信が終わるまで、プログラムが止まってしまい、アプリがフリーズしたように見えるので、一般的に通信処理は非同期で行います

## 非同期通信のやり方

* URLオブジェクトを作る

 ```swift
    let url = NSURL(string: "http://www.ymori.com/itest/test.txt")
 ```

* NSURLSessionオブジェクトを作る

 ```swift
    let urlSession = NSURLSession.sharedSession()
 ```

* ダウンロードタスク（処理）を作ります

 ```swift
    let task = urlSession.dataTaskWithURL(url, completionHandler: {
                (data, response, error) -> Void in
                   通信終了時の処理
               }
 ```

    * 通信終了時の処理はここではクロージャーとなっていますが、別メソッドにしても良いです。
    * この処理はバックグラウンドタスクで実行されます

* タスクを実行する

 ```swift
    task.resume()
 ```

## データを受信した後の処理

* 受信データはバイナリデータなので、Swift言語のString型に変換します
* NSStringに便利なメソッドがあるので、これを使います
* データからNSStringへの変換。データのエンコーディングを指定します

 ```swift
    let nsstr = NSString(data: data!, encoding: NSUTF8StringEncoding)
    let str = String(nsstr)
 ```

## サンプルの作成

* 新規プロジェクトChapter05-4を作成してください


   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-4             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* ボタンを画面に追加

   | 部品        |アトリビュート |制約                                                         |
   |------------|-------------|-------------------------------------------------------------|
   |ボタン１     |Text ボタン１ |Horizontally in Containerにチェック、上からの距離 40,Width 100 |


* IBActionを作成してください

 
   | 部品            | IBAction        |
   |----------------|-------------------|
   |ボタン１         |tapBtn1            |




* プログラムの作成

 ```swift
    @IBAction func tapBtn1(sender: AnyObject) {
        if let url = NSURL(string: "http://www.ymori.com/itest/test.txt") {
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(url, completionHandler: {
                (data, response, error) -> Void in
                if let nsstr = NSString(data: data!, encoding: NSUTF8StringEncoding) {
                    let str = String(nsstr)
                    print("文字列=\(str)")
                }
            })
            task.resume()
        }
    }
 ```


* このサイトもhttp通信なので、ATSの制限をはずしてください
* できたら実行してみます
* ボタンを押すと、下のように出ます

 ```
文字列=テストテキストデータ
 ```

* 非同期処理の部分をクロージャーで作成していますが、ここは以下のように関数(メソッド)にする事もできます

 ```swift
    @IBAction func tapBtn2(sender: AnyObject) {
        if let url = NSURL(string: "http://www.ymori.com/itest/test.txt") {
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(url, completionHandler: onFinish)
            task.resume()
        }
    }

    /*
    *   受信終了後呼ばれる処理
    */
    func onFinish(data: NSData?, response: NSURLResponse?, error: NSError?) {
        if let nsstr = NSString(data: data!, encoding: NSUTF8StringEncoding) {
            print("文字列=\(nsstr)")
        }
    }

 ```