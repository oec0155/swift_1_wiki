# Chapter 5-5 JSONデータをパースする

* JSONはJavaScript Object Notationの略で、JavaScript用のオブジェクト指向型のデータ記述方法だったのですが、便利なので、Webでのデータの受け渡しなどに使われだしました

## JSONデータとは？

* 書き方
    * 配列　
        * [値,値]

        ```javascript
    ["ダージリン","アールグレイ","オレンジペコ"]
        ```
    * 辞書（オブジェクト） 
        * {キー:値,キー:値}

        ```javascript
{"name":"ダージリン","price":600,"在庫":true}
        ```

    * 辞書（オブジェクト）の配列 
         * [{キー:値,キー:値},
            {キー:値,キー:値}]

        ```javascript
[
	{"name":"ダージリン","price":600},
	{"name":"アールグレイ","price":550}
]
        ```

    * 辞書（オブジェクト）の辞書
        * {キー:{キー:値,キー:値},
            キー:{キー:値,キー:値}}

        ```javascript
{
  "岡山":{"おみやげ":"吉備団子","名所":"後楽園"},
  "広島":{"おみやげ":"もみじ饅頭","名所":"宮島"}
}
        ```

## JSONデータを配列や辞書に変換（シリアライゼーション）

* 配列に変換

 ```swift
var myArray = NSJSONSerialization.JSONObjectWithData
               (myJsondata, options: nil, error: nil) as [String]
 ```

* 辞書に変換

 ```swift
var myDict = NSJSONSerialization.JSONObjectWithData
              (myJsondata, options: nil, error: nil) as [String:AnyObject]
```

 * シリアライズは正常にパースできない時にエラーを返します。従って、do try catch でエラーを捕捉して処理する必要があります

 ```swift
do {
   var myDict = try NSJSONSerialization.JSONObjectWithData
              (myJsondata, options: nil, error: nil) as [String:AnyObject]
      正常時処理
} catch {
      エラー時処理
}

 ```

## サンプルプロジェクトの作成 

* 新規プロジェクト Chapter05-5 を作成してください



   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-5             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* JSONデータ（配列）をプロジェクトの中に準備します
    * プロジェクトフォルダーをクリックして File -> New -> File を選択してください
    * iOSの Other -> Empty -> Next を押す
    * ファイル名は json.txt とし、create ボタンを押す
    * 今作成した json.txt をクリックし、エディターエリアに次のように入れます

 ```javascript
["ブルーマウンテン","キリマンジャロ","コロンビア"]
 ```

* プロジェクト内のファイルを読み込む方法
    *  以下の様にしてデータを読み込みます

 ```swift
var myPath = NSBundle.mainBundle().pathForResource("ファイル名", ofType: "拡張子")
var myJsondata = NSData(contentsOfFile: myPath)
 ```

### JSONデータの読み込み(配列)

* ボタンを画面に追加

   | 部品        |アトリビュート |制約                                                         |
   |------------|-------------|-------------------------------------------------------------|
   |ボタン１     |Text ボタン１ |Horizontally in Containerにチェック、上からの距離 40,Width 100 |


* IBActionを作成してください

 
   | 部品            | IBAction        |
   |----------------|-------------------|
   |ボタン１         |tapBtn1            |


* IBAction の中に以下のようにコーディングします  

 ```swift
    /*
    *   json1.txt読み込み(配列)
    */
    @IBAction func tapBtn1(sender: AnyObject) {
        if let myPath = NSBundle.mainBundle().pathForResource("json1", ofType: "txt") {
            if let jsondata = NSData(contentsOfFile: myPath ) {
                do {
                    let myArray = try NSJSONSerialization.JSONObjectWithData(jsondata, options: NSJSONReadingOptions.MutableContainers)
                                     as! [String]
                    for data in myArray {
                        print("data=\(data)")
                    }
                } catch  {
                    print("パースエラー")
                }
            }
        }
    }
 ```

* 実行してみて下さい
    * ボタンを押すと以下のようにコンソールに出れば成功です

 ```
data=ブルーマウンテン
data=キリマンジャロ
data=コロンビア
 ```
 
### JSONデータの読み込み(辞書)

* 次に辞書のJSONデータの読み込みを行います
* 同じようにしてjson2.txtデータを作成してください

 ```
{ "名前" : "ダージリン", "値段" : 600 }
 ```
 
* ボタンを１つ追加してIBActionを作成し以下の様にコーディングします


   | 部品        |アトリビュート |制約                                                         |
   |------------|-------------|-------------------------------------------------------------|
   |ボタン２     |Text ボタン２ |Horizontally in Containerにチェック、上からの距離 40,Width 100 |

 
   | 部品            | IBAction        |
   |----------------|-------------------|
   |ボタン２         |tapBtn2            |



 ```swift
    /*
    *   json2.txt読み込み(辞書)
    */
    @IBAction func tapBtn2(sender: AnyObject) {
        if let myPath = NSBundle.mainBundle().pathForResource("json2", ofType: "txt") {
            if let jsondata = NSData(contentsOfFile: myPath ) {
                do {
                    let myDict = try NSJSONSerialization.JSONObjectWithData(jsondata, options: NSJSONReadingOptions.MutableContainers) 
                       as! [String:AnyObject]
                    for (key, val) in myDict  {
                        print("key=\(key) data=\(val)")
                    }
                } catch  {
                    print("パースエラー")
                }
            }
        }
    }
 ```

* できたら実行してみて下さい
    * ボタンを押すと以下のようにコンソールに出れば成功です

 ```swift
key=名前 data=ダージリン
key=値段 data=600
 ```

### JSONデータの読み込み(辞書の配列)

* 次は、辞書の配列のJSONデータの読み込みを行います
* 同じようにしてjson3.txtデータを作成してください

 ```
[
{ "name": "ダージリン", "price": 600 },
{ "name": "アールグレイ", "price": 550 },
{ "name": "セイロン", "price": 500 }
]
 ```

* ボタンを１つ追加してIBActionを作成し以下の様にコーディングします

 ```swift
    /*
    *   json3.txt読み込み(辞書の配列)
    */
    @IBAction func tapBtn3(sender: AnyObject) {
        if let myPath = NSBundle.mainBundle().pathForResource("json3", ofType: "txt") {
            if let jsondata = NSData(contentsOfFile: myPath ) {
                do {
                    let myArrDict = try NSJSONSerialization.JSONObjectWithData(jsondata, options: NSJSONReadingOptions.MutableContainers) 
                        as! [[String:AnyObject]]
                    for ardata in myArrDict {
                        print("--------------")
                        for (key, val) in ardata  {
                            print("key=\(key) data=\(val)")
                        }
                    }
                } catch  {
                    print("パースエラー")
                }
            }
        }
    }
 ```

* できたら実行してみて下さい
    * ボタンを押すと以下のようにコンソールに出れば成功です

 ```
--------------
key=price data=600
key=name data=ダージリン
--------------
key=price data=550
key=name data=アールグレイ
--------------
key=price data=500
key=name data=セイロン
 ```

### JSONデータの読み込み(辞書の辞書)

* 次は、辞書の辞書のJSONデータの読み込みを行います
* 同じようにしてjson4.txtデータを作成してください

 ```
{ "関東" : { "餅" : "切り餅", "醤油" : "濃口", "月見団子" : "丸形" } ,
  "関西" : { "餅" : "丸餅", "醤油" : "薄口", "月見団子" : "サトイモ形" }
}
 ```

* ボタンを１つ追加してIBActionを作成し以下の様にコーディングします

 ```swift
    /*
    *   json4.txt読み込み(辞書の辞書)
    */
    @IBAction func tapBtn4(sender: AnyObject) {
        if let myPath = NSBundle.mainBundle().pathForResource("json4", ofType: "txt") {
            if let jsondata = NSData(contentsOfFile: myPath ) {
                do {
                    let myDictDict = try NSJSONSerialization.JSONObjectWithData(jsondata, options: NSJSONReadingOptions.MutableContainers) 
                        as! [String:AnyObject]
                    for (key, val) in myDictDict {
                        print("--------------")
                        print("key=\(key)")
                        let data = val as! [String:AnyObject]
                        for (key2, val2) in data  {
                            print("key2=\(key2) data2=\(val2)")
                        }
                    }
                } catch  {
                    print("パースエラー")
                }
            }
        }
    }
 ```
 * できたら実行してみて下さい
   * ボタンを押すと以下のようにコンソールに出れば成功です

 ```
key=関東
key2=醤油 data2=濃口
key2=月見団子 data2=丸形
key2=餅 data2=切り餅
--------------
key=関西
key2=醤油 data2=薄口
key2=月見団子 data2=サトイモ形
key2=餅 data2=丸餅
 ```

### インターネットからJSONデータの取得

* 今までの復習でインターネットから、AEDの設置場所のデータを取得して地図上に表示してみます
* AED設置場所データ
  	[Link "https://aed.azure-mobile.net/api/aedinfo/岡山県"](https://aed.azure-mobile.net/api/aedinfo/岡山県)

* 辞書の配列として取得できます

* インターネットからJSONデータの取得
    * URL

 ```html
  	https://aed.azure-mobile.net/api/aedinfo/岡山県
 ```

   * URLに日本語が含まれますので、エンコーディングが必要です

 ```swift
        let pref = "岡山県"
        let encPref = pref.stringByAddingPercentEncodingWithAllowedCharacters
                      (NSCharacterSet.alphanumericCharacterSet())
 ```

* ボタンを１つ追加してIBActionを作成し以下の様にコーディングします

 ```swift
    /*
    *   インターネットからAED設置場所データを取得
    */
    @IBAction func tapBtn5(sender: AnyObject) {
        let pref = "岡山県"
        let encPref = pref.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())
        if let myURL = NSURL(string: "https://aed.azure-mobile.net/api/aedinfo/" + encPref!) {
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(myURL, completionHandler: { (data, response, error) -> Void in
                do {
                    let myArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [[String:AnyObject]]
                    for data in myArray {
                        for (key,val) in data  {
                            if key == "LocationName" ||
                                key == "City" ||
                                key == "Latitude" ||
                                key == "Longitude" {
                                    print("key=\(key) data=\(val)")
                            }
                        }
                    }
                } catch  {
                    print("パースエラー")
                }
            })
            task.resume()
        }
    }
 ```

* できたら実行してみます
* 少し時間がかかりますが、以下のように出ればOKです

 ```
key=City data=久米郡久米南町
key=Longitude data=133.9661247
key=LocationName data=岡山県立誕生寺支援学校
key=Latitude data=34.9541378
key=City data=久米郡久米南町
key=Longitude data=133.9661247
key=LocationName data=岡山県立誕生寺支援山県立誕生寺支援学校
key=Latitude data=34.9541378
key=City data=久米郡久米南町
key=Longitude data=133.9661247

 ```



## 応用　（以下は時間があったらやってみてください）

* サンプルプロジェクトの作成
    * 新規プロジェクト Chapter05-5-1 を作成します


   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-5-1           |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* Map Kit View を画面に貼り付ける


   | 部品        |制約                             |
   |------------|---------------------------------|
   |マップビュー  |上左右下からの距離 0　（マージン無し） |


* MapViewのIBOutletを作成して"mapView"とする

   | 部品        |IBOutlet       |
   |------------|---------------|
   |マップビュー  | mapView       |



* MapViewの準備
    * 中心位置と縮尺を指定します 
    * MapViewの中心位置は岡山駅にします

 ```swift
    let location = CLLocationCoordinate2DMake(34.666169, 133.917977)
    var region = self.mapView.region
    region.center = location
    region.span.latitudeDelta = 0.01
    region.span.longitudeDelta = 0.01
    mapView.setRegion(region, animated: true)
 ```


* ViewDidLoadの中に以下のようにコーディングします

```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let location = CLLocationCoordinate2DMake(34.666169, 133.917977)
        var region = self.mapView.region
        region.center = location
        region.span.latitudeDelta = 0.01
        region.span.longitudeDelta = 0.01
        mapView.setRegion(region, animated: true)
        requestData()
    }
```

  * データを受信する処理を作成します

```swift
    /*
    *   インターネットのサービスよりAED設置場所のデータを取得する
    *   データを受信すると、MapViewのピンを立てる
    */
    func requestData() {
        let pref = "岡山県"
        let encPref = pref.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())
        if let myURL = NSURL(string: "https://aed.azure-mobile.net/api/aedinfo/" + encPref!) {
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(myURL, 
                       completionHandler: { (data, response, error) -> Void in
                do {
                    let myArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                             as! [[String:AnyObject]]
                    for data in myArray {
                        guard let lat = data["Latitude"] as? Double else { continue }
                        guard let lng = data["Longitude"] as? Double else { continue }
                        let locationName = data["LocationName"] as? String
                        let city = data["City"] as? String
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = CLLocationCoordinate2DMake(lat,lng)
                        annotation.title = locationName
                        annotation.subtitle = city
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.mapView.addAnnotation(annotation)
                        })
                    }
                } catch  {
                    print("パースエラー")
                }
            })
            task.resume()
        }
    }
```
 * できたら実行してみます

![スクリーンショット 2016-03-23 10.40.40](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/f03b1d98a3/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-23_10.40.40.png)