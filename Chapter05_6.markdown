# Chapter 5-6　SNSへ投稿する

## Social Framework:Twitterへ投稿する

* Social Frameworkは投稿用のフレームワークで、これを利用する事で、TwitterやFacebookへの投稿、タイムラインの取得ができます
* 現在サポートされているのは以下のみです

| SLServiceType |
|---------------|
| SLServiceTypeFacebook |
| SLServiceTypeSinaWeibo |
| SLServiceTypeTensentWeibo |
| SLServiceTypeTwitter |

## Social Framewok の利用方法

* Social Framework のライブラリの追加

 ```swift
import Social
 ```

* Twitter投稿用ViewControllerの作成

 ```swift
var twitterVC = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
 ```

* デフォルト文字を入れる

 ```swift
twitterVC.setInitialText("つぶやくよ")
 ```

* 画像を入れる

 ```swift
twitterVC.addImage(UIImage(named: "sky.jpg"))
 ```

## サンプルアプリを作成します

* 新規プロジェクトChapter05-6を作成します


   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-6             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* 以下の部品をMain.storyboardに貼り付けてください

   | 部品        |アトリビュート            |制約                                      |
   |------------|------------------------|-----------------------------------------|
   |テキストビュー |                        |上左右からの距離 0（マージン有り）,Height 150 |
   |ボタン       |Text "Twitterでシェアする" |Horizontally in Container にチェック、上からの距離 20,Width 150 |

* IBOutlet,IBAction の作成

   | 部品            | IBOutlet          |
   |----------------|-------------------|
   |テキストビュー     | textView     　　　　   |

   | 部品            | IBAction        |
   |----------------|-------------------|
   |ボタン           |tapBtn             |


* Twitter投稿用ビューを表示する処理を以下のようにコーディングします

 ```swift
@IBAction func tapBtn(sender: AnyObject) {
       // Twitterの投稿用ダイアログを作ります
        let twitterVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        // デフォルト文字を追加します
        twitterVC.setInitialText(textView.text)
        // 投稿ダイアログを表示します
        presentViewController(twitterVC, animated: true, completion: nil)
    }
 ```

* できたら実行してみてください

    * ボタンを押すと、Twitter投稿用ダイアログが出ます
    * 投稿ボタンを押すと、Twitterに投稿します

![スクリーンショット 2016-03-22 14.44.13](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/30b3cc1e0a/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-22_14.44.13.png)
![スクリーンショット 2016-03-22 14.43.53](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/e677bb9740/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-22_14.43.53.png)

* Twitterアカウントが設定されていないと入力要求が来ます。以下のように設定してください
    * ユーザ　　  Oec0155test
    * パスワード  oec0155

## 共有アクションシートの利用

* Social Frameworkを使うと、Twitterの時と同じようにFcebookへも投稿できます
* でもそれではそれぞれにボタンを準備しないといけません。
* 共有アクションシートを使うと、いろいろなアプリとデータが共有できます

## 共有アクションシートの作成方法

* シェアする文字列、画像、URLを配列で準備する(順番は関係ありません)

 ```swift
 let shareText = textView.text
 let shareURL = NSURL(string: "https://www.takashimaya.co.jp")
 let shareImg = UIImage(named: "berry.png")
 let shareItems:[AnyObject] = [shareText, shareURL!, shareImg! ]
 ```

* 共有アクションシートの作成

 ```swift
let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
 ````

* 使用しないアクティビティタイプを指定する方法
    * 使用しないアクティビティタイプを配列にセットしてプロパティにセットします

 ```swift
let excAct = [ UIActivityTypePrint ]
avc.excludedActivityTypes = exact
 ```

* 共有アクションシートの表示

 ```swift
presentViewController(avc, animated: true, completion: nil)
 ```

## サンプルの作成

* ボタンを１つ追加します

   | 部品        |アトリビュート            |制約                                      |
   |------------|------------------------|-----------------------------------------|
   |ボタン       |Text "シェアする"         |Horizontally in Container にチェック、上からの距離 20,Width 150 |


* IBAction("tapBtn2")を作成します

   | 部品            | IBAction        |
   |----------------|-------------------|
   |ボタン           |tapBtn2             |


* 以下のようにコーデイングします

 ```swift
@IBAction func tapBtn2(sender: AnyObject) {
        let shareText = textView.text
        let shareURL = NSURL(string: "https://www.takashimaya.co.jp")
        let shareImg = UIImage(named: "berry.png")
        // シェアするデータの配列
        let shareItems:[AnyObject] = [shareText, shareURL!, shareImg! ]
        // 共有アクションシートの作成
        let avc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        // 使用しないアクティビティタイプを指定する
        let excAct = [ UIActivityTypePrint ]
        avc.excludedActivityTypes = excAct
        // 共有アクションシートの表示
        presentViewController(avc, animated: true, completion: nil)
        
    }
 ```

* できたら実行してみてください

![スクリーンショット 2016-03-22 18.05.22](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/8345e98c06/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-22_18.05.22.png)
