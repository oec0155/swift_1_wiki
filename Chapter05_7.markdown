# Chapter 5-7 JSONデータでアプリを作ろう！

## 住所検索アプリを作ってみよう
* 郵便番号を入力してボタンを押すと、Webから住所を取ってきて、表示するアプリを作成します
* 郵便番号ー住所検索API( http://zipaddress.net ) を使います
    * 使い方
        * http://api.zipaddress.net/?zipcode=郵便番号ハイフン無し
    * 結果
        * JSON形式で返ってくる

## 新規プロジェクトの作成

* 新規プロジェクト Chapter05-7 を作成します

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter05-7             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |


* Main.storyboard上に部品を配置します

  ![スクリーンショット 2016-03-23 14.18.30](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/8b033b8b4e/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-23_14.18.30.png)


* 部品一覧

    | 部品　      | API 　　　 |アトリビュート  |制約                                             |
    | -----------|-----------|--------------|-------------------------------------------------|
    |郵便番号入力 | TextField  |               |上からの距離 40,左右からの距離 0                       |
    |検索ボタン    | Buton     |Text "住所検索" |Horizontally in Container にチェック、上からの距離 40 |
    |都道府県ラベル| Label     |Text "都道府県" |上(郵便番号入力)からの距離 100,左からの距離 0            |                    
    |都道府県データ| Label     |背景色 緑       |上からの距離 20,左右からの距離 0,Height 24              |
    |住所ラベル    | Label     |Text "住所"    |上からの距離 20,左からの距離 0                        |
    |住所データ    | Label     |背景色 緑       |上からの距離 20,左右からの距離 0,Height 24             |
    
* IBOutletとIBActionの作成

    | 部品　      | IBOutlet         |IBAction                   |
    | -----------|------------------|---------------------------|
    |郵便番号入力  | zipTextField     |tapReturn(Did End On Exit) |
    |検索ボタン    |                  |tapSearch                  |
    |都道府県ラベル |                  |                           |
    |都道府県データ | prefLabel        |                           |
    |住所ラベル    |                  |                           |
    |住所データ    | addressLabel     |                           |

  * 検索ボタンを押された時の処理を以下のように作成します

 ```swift
    @IBAction func tapSerach(sender: AnyObject) {
        guard let ziptext = zipTextField.text else {
            // 値がnilだったら、終了します
            return
        }
        // リクエストするURLを作ります
        let urlStr = "http://api.zipaddress.net/?zipcode=\(ziptext)"
        // 確認のためurlStrを表示します
        print(urlStr)
        if let url = NSURL(string:urlStr) {
            let urlSession = NSURLSession.sharedSession()
            let task = urlSession.dataTaskWithURL(url, completionHandler: self.onGetAddress )
            task.resume()
        }
    }
 ```

* データを受信した時の処理を以下のようにコーディングします

 ```swift
// 検索処理が終了したら実行します
    func onGetAddress(data: NSData?, res: NSURLResponse?, error: NSError? ) {
        // 確認のためデータを表示します
        print(data)
        // 受け取ったdataをJSON解析します。もしエラーならcatchへジャンプします
        do {
            // dataをJSON解析実施します
            let jsonDic = try NSJSONSerialization.JSONObjectWithData(
                data!, options: NSJSONReadingOptions.MutableContainers)
            as! [String:AnyObject]
            // 解析できた値を調べていきます
            if let code = jsonDic["code"] as? Int {
                // codeという項目が整数なら、住所検索APIからのコード情報です
                if code != 200 {
                    // コードが200でない時は、検索エラーです
                    if let errmsg = jsonDic["message"] as? String {
                        print(errmsg)
                    }
                }
                
            }
            if let data = jsonDic["data"] as? [ String : AnyObject ] {
                // dataという項目の中身が辞書データなら、その中身を調べます
                if let pref = data["pref"] as? String {
                    // data内のprefという項目が文字列なら、県名です
                    print("県名は\(pref)です")
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.prefLabel.text = pref
                    })
                }
                if let address = data["address"] as? String {
                    // data内のaddressという項目が文字列なら、住所です
                    print("住所は\(address)です")
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.addressLabel.text = address
                    })
                }
            }
            
        } catch {
            // JSON解析に失敗した時に実行します
            print("エラーです")
        }
    }
 ```

    * データ受信後に画面描画を行いますので、メインスレッドを呼び出して、その中で画面描画処理を行っています。

* できたら実行してみてください


