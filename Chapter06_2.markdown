# Chapter 6-2　Single View Applicationから複数画面(モーダル)アプリを作る

##  1.ボタンで遷移(モーダル)

### 色あてアプリを作ってみよう
### プロジェクトを作る
* 新規プロジェクトを作ります


   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート           | Single View Application |
   |Product Name         | Chapter06-2             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |

### 画面をデザインする

* 第１画面
    * 画面上に部品を配置します
        * 部品

       |部品            |コンポーネント|テキスト|制約                                   |
       |----------------|----------|-------|--------------------------------------|
       | RGB表示ラベル  |Label   　|RGB      |上からの距離 100、左右の距離 0、高さ 24    |
       | 色を見るボタン |Button    |色を見る   |水平方向中心を中央にセット、上からの距離 80 |

    * できたら実行してみましょう


* 第２画面
    * 画面２を追加します
        * ViewControllerをMain.storyboardにドラッグドロップする
    * 戻るボタンを配置します

       |部品            |コンポーネント|テキスト |制約                                   |
       |----------------|----------|--------|--------------------------------------|
       | 戻るボタン       |Button    |戻る    |上からの距離   20、左からの距離    0      |

   
    * 第２画面用のViewController.swift を作ります
        * プロジェクトフォルダをクッリクして、File -> New -> File を選択
        * iOS -> Cocoa Touch Class を選択してNext を押す
        * 次のように設定してCreateを押す

        | パラメータ    | 内容                |
        |---------------|---------------------|
        | Class         |ColorViewController  |
        | SubClass of   |UIViewController     |
        | Language      |swift                |

    * ColorViewControllerを第２画面に設定する 

        | 画面      |  アイデンティティーインスペクタ                  |
        |----------|---------------------------------------------|
        | 第２画面  |Custom Class -> Class を ColorViewController |
 
      (テキスト252頁参照)
   
* 第１画面から第２画面への遷移の設定
    * 画面１の「色を見る」ボタンから画面２へ接続します
        * 第１画面の **色を見る** ボタンで control+クリック して第２画面へドラッグドロップする

* できたかどうか、実行して確認しましょう
    * 画面が遷移しますが、元の画面に戻る事が出来ませんね
    * これは戻りの処理が作成されていないからです


* 戻り処理を追加します
    * 最初に第１画面のViewController.swift に戻り口を作ります
        * ViewController.swift に以下のコーディングを追加します

        ```swift
           @IBAction func returnTop(segue: UIStoryboardSegue) {
                print("戻ったよ")
           }
        ```

* 第２画面から第１画面へ接続します
    * 第２画面の **戻る** ボタンに戻りの処理を設定します
        * **戻る** ボタンの上で control+クリック して第２画面上部にあるExitアイコンまでドラッグします
            * ドロップすると、ポップアップが出ますので、先ほど作成した、戻った時に実行するメソッド **returnTop** を選択します
            
* できたかどうか、実行して確認しましょう
    * 画面が遷移したり戻ったりするようになったら成功です
 

### 処理を作成する
画面遷移はできましたので、処理を作成します  

画面が表示されるたびにランダムにRGBを作成し、ラベルに表示するようにします  

* ラベルをアウトレット接続して名前をつけます

    * 第１画面
        * RGBラベルにIBOutletを作成します

   | 部品            | IBOutlet          |
   |----------------|-------------------|
   |RGBラベル        | colorLabel　　　   |


* アプリでランダムと３つの変数を使う準備をします
    * まず GameplayKit を導入します

    ```swift
import GameplayKit
    ```

    * ランダムな変数を作成する為に以下の変数を定義します

    ```swift
        // ランダムを扱う準備
        let randomSource = GKARC4RandomSource()
        var colorR = 0
        var colorG = 0
        var colorB = 0
    ```

* 画面が表示される時に、ランダムな値を作って表示します

    * 以下の処理を追加します

    ```swift
         override func viewWillAppear(animated: Bool) {
             super.viewWillAppear(animated)
             // 0〜255のランダムな値を３つ求めます
             colorR = randomSource.nextIntWithUpperBound(256)
             colorG = randomSource.nextIntWithUpperBound(256)
             colorB = randomSource.nextIntWithUpperBound(256)
             // 3つの値を表示します
             colorLabel.text = "R=\(colorR) ,G=\(colorG) ,B=\(colorB)"
         }
    ```   

    viewWillAppear は画面が表示される度に呼ばれるメソッドです
    最初にスーパークラスのメソッドを呼ぶ処理が抜けていますので追加しておいてください

* どのようにできたか、実行してみましょう

    * 画面に以下のように表示されればOKです(数字は毎回変わります)

    ```text
        R=114 ,G=178 ,B=83
    ```

### 第２画面を表示した時、RGBの値で背景色を塗る

* ３つの変数を準備して、画面を表示した時、その色で背景色を塗ります

    * 第２画面
        * RGBの値を保持する変数をcolorViewControllerに準備します

    ```swift
      // 3原色を入れる変数
      var colorR = 0
      var colorG = 0
      var colorB = 0
    ```
 
    * 画面が表示される度に画面の色を設定する為に、viewWillAppearメソッドに次の様にコーディングします

    ```swift
         override func viewWillAppear(animated: Bool) {
             super.viewWillAppear(animated)
             // RGBの値から色を作ります
             let backColor = UIColor(
                 red: CGFloat(colorR) / 256.0 ,
                 green: CGFloat(colorG) / 256.0 ,
                 blue: CGFloat(colorB) / 256.0 ,
                 alpha: 1.0)
             // 作った色を背景色に設定します
             view.backgroundColor = backColor
         }
    ```

    
* どのようにできたか、実行して確認しましょう

    * 第２画面はいつも黒色です。これは、第１画面から第２画面にRGBの値が渡されていないからです。

### 第１画面から第２画面にRGBの値を渡す
    
* 画面１から画面２に色の値を受け渡す
    * 画面遷移する直前にprepareForSegueメソッドが呼ばれます。ここでRGB値を次画面にセットします

     ```swift
         // 画面が切り替わる時呼び出されます
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
             // 切り変わり先の画面を取り出します
             let nextvc = segue.destinationViewController as! colorViewController
             // 切り変わり先の変数に、この画面の変数の値を入れて、受け渡します
             nextvc.colorR = colorR
             nextvc.colorG = colorG
             nextvc.colorB = colorB
         }
     ```

* どのようにできたか、実行して確認しましょう


![スクリーンショット 2016-03-24 14.19.13](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/f29f28843b/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-24_14.19.13.png)
![スクリーンショット 2016-03-24 14.19.26](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/e5bae5df7e/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-24_14.19.26.png)

* 毎回RGBの値が変わり、第２画面の色が変更すればOKです。

### プログラムで画面遷移

* 今作成したサンプルではボタンに次画面へのセグエを作成しました。しかし、同じボタンでも、プログラムで遷移先を変えたい時もあります。プログラムで遷移を実行させることができます。
* 次にプログラムから画面遷移をさせる方法をやってみます
* 第一画面にボタンを１つ追加します

    |部品                   |コンポーネント|テキスト        |制約                    |
    |----------------------|----------|----------------|------------------------|
    | プログラムで遷移ボタン   |Button    |プログラムで遷移  |上からの距離 0、水平方向に中央|

* セグエの作成
    * 先ほどはボタンから次画面へセグエを作成しましたが、プログラムから遷移させる時には画面上部にある、ViewControllerアイコンからセグエを作成します
    * 第１画面上部のView Controller アイコンの上で control+クリックし、第２画面にドラッグドロップしてください
    * 新しく出来たセグエをプログラムから実行できるように、セグエに名前をつけます
        * セグエをクリックし、アトリビュートインスペクタの Identifier に "manualSegue" と入れます

* ボタンに遷移処理を作成します

    * **ﾌﾟﾛｸﾞﾗﾑで遷移** ボタンに IBAction("tapProg")を作成します

   | 部品              | IBAction           |
   |-------------------|-------------------|
   |プログラムで遷移ボタン |tapProg            |


    * tapProg()メソッドで"manualSegue" を実行します

    ```swift
        @IBAction func tapProg(sender: AnyObject) {
            performSegueWithIdentifier("manualSegue", sender: nil)
        }
    ```
* できたら実行してみてください
* ボタンからのセグエと同じように動作すればOKです
* このように簡単な画面遷移はボタンに直接つけるとコーディング無しでできますが、複雑な画面遷移（チェックがあったり、内容ににより遷移先が変更になったりする場合）はプログラムから遷移させるようにします

