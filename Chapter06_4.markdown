# Chapter 6-4 Tabbed Applicationから複数画面アプリを作る

* 次にTabで切り替わるアプリを作成します

## 単位換算アプリを作ってみよう

* 新規プロジェクト Chapter06-4 を作成します


 |  項目               | 内容                    |
 |---------------------|-------------------------|
 |テンプレート            | Tabbed Application     |
 |Product Name         | Chapter06-4             |
 |Language             | Swift                   |
 |Devices              | iPhone                  |


* Create を押します
* Main.storyboard を開きます
* 画面Size を 4inchに変更します
    * Tab Bar Controller があり、その下に２つ画面(First View,Second View)があります
    * 各画面の下にはその画面のタブがついています

* 実行してみてください

    * タブで切り替わるのが確認できたかと思います
    * ではこれを元に単位換算アプリを作成します

    * 画面は全部で３つあります
    * それぞれ cm , inch , 寸 で数値を表します

## 画面をデザインする

* 第１画面に「cm」のテキストフィールドとラベル、入力ボタンを用意します。

    * 部品の配置

      |部品          |          |アトリビュート                  |制約                                           |
      |--------------|----------|---------------------------|-------------------------------------------------|
      |入力エリア     |TextField |Keyboard Type -> Number Pad |上からの距離 100、左右からの距離 0                   |
      |単位ラベル     |Label     |Text -> "cm"                |上からの距離 20、左からの距離 0                      |
      |入力ボタン     |Button    |Title -> "入力"              |上からの距離 20  (入力エリアからの距離)、右からの距離 0 |



* 画面２を修正します

    * 第１画面と同じです
    * 画面上の２つのラベルを削除します
    * 部品の配置

      |部品          |          |アトリビュート                  |制約                                           |
      |--------------|----------|---------------------------|-------------------------------------------------|
      |入力エリア     |TextField |Keyboard Type -> Number Pad |上からの距離 100、左右からの距離 0                   |
      |単位ラベル     |Label     |Text -> "inch"              |上からの距離 20、左からの距離 0                      |
      |入力ボタン     |Button    |Title -> "入力"             |上からの距離 20  (入力エリアからの距離)、右からの距離 0 |



* 画面３を追加します
    * 第３画面はまだないので、新しく作成します
        * ライブラリペインから View Controller を Main.storyboard にドラッグドロップします
    * Tab Bar Controller の配下にする為に、Tab Bar Controller とセグエ接続します
        * Tab Bar Controller の上で、control+クリック して第３画面までドラッグドロップします
        * セグエの種類の選択がポップアップしますので、タブ切替の **view controllers** を選択します
    * 第３画面も同じように部品を配置します

      |部品          |          |アトリビュート                  |制約                                           |
      |--------------|----------|---------------------------|-------------------------------------------------|
      |入力エリア     |TextField |Keyboard Type -> Number Pad |上からの距離 100、左右からの距離 0                   |
      |単位ラベル     |Label     |Text -> "寸"              |上からの距離 20、左からの距離 0                      |
      |入力ボタン     |Button    |Title -> "入力"             |上からの距離 20  (入力エリアからの距離)、右からの距離 0 |


* 画面３用のThirdViewController.swiftを作成します
    * プロジェクトのフォルダーをクリックして File -> New -> File を選択
    * iOS -> Cocoa Touch Class -> Next を押す
    * 以下のように設定する

     | パラメータ    | 内容                |
     |---------------|---------------------|
     | Class         |ThirdViewController  |
     | SubClass of   |UIViewController     |
     | Language      |Swift                |

    * create を押す

* ThirdViewController.swift を画面３に設定します
    * 第３画面のアイデンティティー・インスペクタのClassに今作成したThirdViewControllerを設定する

* タブの名前を変更します
    * 以上で３つ画面ができました
    * それぞれの画面の下にあるタブの名前とアイコンを変更します
    * タブ用のアイコンをプロジェクトにドラッグドロップして入れます
    * タブをクリックして、アトリビュート・インスペクタで以下の設定をします

       |画面      |タイトル(Title)     |アイコン(image)    |
       |----------|------------------|-----------------|
       |第１画面   | cm               | cm.png          |
       |第２画面   | inch             | inchi.png       |
       |第３画面   | 寸               | sun.png         |

* 以上で画面ができました、動作確認してみて、画面が切り替わる事を確認してください


## プログラム製作編
### 画面１、２、３のテキストフィールドとボタンをプログラムに接続する
* IBOutlet, IBActionをそれぞれ作成します

    * ３画面とも同じように作成します

      | 部品　　　　　　　　　| 　        | 名前　　　　　　  |
      |-----------------|----------|--------------|
      |入力テキストフィールド|IBOutlet  |dataTextField |
      |入力ボタン         |IBAction  |tapInput      | 


### AppDelegate.swiftに共有データの変数を用意  
３つの画面で値を共有する為に、AppDelegate.swiftに共有変数を作成します
    * AppDelegateとは 
        * プロジェクト作成時に既に作成されています
        * アプリ全体のライフタイムイベントを管理する処理を委譲されるクラスです
        * ライフタイムイベントとは、アプリの起動、休止、停止時に発生するイベントです
    * AppDelegateに共有変数を作ります
 
       ```swift
       @UIApplicationMain
       class AppDelegate: UIResponder, UIApplicationDelegate {

           var window: UIWindow?
    
           // 共有して使う変数
           var cmValue:Double = 1.0

        ```

### 画面１で共有データを表示して、入力されたら変更する
* 画面が表示される時に、値を表示します
    * ViewController から AppDelegate には以下のようにしてアクセスします

       ```swift
       class FirstViewController: UIViewController {

           @IBOutlet weak var dataTextField: UITextField!
    
           // AppDelegate にアクセスするオブジェクトを作成します
           let ap = UIApplication.sharedApplication().delegate as! AppDelegate    

       ```

    * 画面が表示されるときにAppDelegateの共有変数の値を入力テキストフィールドにセットします

       ```swift
           override func viewWillAppear(animated: Bool) {
               // 共有変数の値をテキストフィールドに設定します
               dataTextField.text = String(ap.cmValue)
           }

       ```

* 入力ボタンが押されたら、入力テキストフィールドの内容をAppDelegateの共有変数にセットします

       ```swift
       @IBAction func tapInput(sender: AnyObject) {
            // キーボードを閉じます
            dataTextField.resignFirstResponder()
            if let text = dataTextField.text {
                // テキストフィールドに値があって
                if let cmValue = Double(text) {
                    // 小数の値に変換できたら、共有変数に書き込みます
                    ap.cmValue = cmValue
                }
            }
        }
       ```


### 画面２、３で共有データを表示して、入力されたら変更する
* 画面２、３も同じです
* ただし、表示する時には cm からそれぞれの単位に変換します
* また、AppDelegateの共有変数に書き込む時は、それぞれの単位から cm に変換します

   |           |出力(cm）   |出力（inch)   |出力（寸）   |
   |-----------|-----------|-------------|-----------|
   |入力（cm）  |   -       |入力 X 0.3937 |入力 X 0.33 |
   |入力（inch）|入力 / 0.3937|   -        | -          |
   |入力（寸）  |入力 / 0.33  |   -         | -         |


* 画面２　
    * 表示する時の処理

       ```swift
       class SecondViewController: UIViewController {

           @IBOutlet weak var dataTextField: UITextField!
    
           // AppDelegate にアクセスするオブジェクトを作成します
           let ap = UIApplication.sharedApplication().delegate as! AppDelegate
    
           override func viewWillAppear(animated: Bool) {
               // 共有変数の値をinchに単位変換して、テキストフィールドに設定します
               let inchValue = ap.cmValue * 0.3937
               dataTextField.text = String(inchValue)
           }

       ```

    * ボタンが押された時の処理

       ```swift
           @IBAction func tapInput(sender: AnyObject) {
               // キーボードを閉じます
               dataTextField.resignFirstResponder()
               if let text = dataTextField.text {
                   // テキストフィールドに値があって
                   if let inchValue = Double(text) {
                       // 小数の値に変換できたら、cm に単位変換して、共有変数に書き込みます
                       ap.cmValue = inchValue / 0.3937
                   }
               }
           }

       ```

* 画面３
    * 表示する時の処理

       ```swift
       class ThirdViewController: UIViewController {

           @IBOutlet weak var dataTextField: UITextField!
    
           // AppDelegate にアクセスするオブジェクトを作成します
           let ap = UIApplication.sharedApplication().delegate as! AppDelegate
    
           override func viewWillAppear(animated: Bool) {
               // 共有変数の値を寸に単位変換して、テキストフィールドに設定します
               let sunValue = ap.cmValue * 0.33
               dataTextField.text = String(sunValue)
           }

       ```

    * ボタンが押された時の処理

       ```swift
           @IBAction func tapInput(sender: AnyObject) {
               // キーボードを閉じます
               dataTextField.resignFirstResponder()
               if let text = dataTextField.text {
                   // テキストフィールドに値があって
                   if let sunValue = Double(text) {
                       // 小数の値に変換できたら、cmに単位変換して、共有変数に書き込みます
                       ap.cmValue = sunValue / 0.33
                   }
               }
           }

       ```

* 以上で完成です。実行してみてください


![スクリーンショット 2016-03-25 11.05.50](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/bd0c3b7567/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-25_11.05.50.png)
![スクリーンショット 2016-03-25 11.06.05](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/3e3d42473e/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-25_11.06.05.png)
![スクリーンショット 2016-03-25 11.06.20](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/c40909a22e/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-25_11.06.20.png)
