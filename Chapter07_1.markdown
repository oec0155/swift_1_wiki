# Chapter 7-1 UITableView:リスト表示させたいとき

## UITableViewとは

* 複数画面アプリのところで、もう１つのナビゲーション切替えをまだ説明していませんが、Chapter7-5で出てきますので、そこで説明します
* UITableViewは複数のデータをリスト表示する部品です
* iOSで特徴的な点は通常のリスト表示の他にグループ形式の機能がある点です(グループは１レベルのみです)
* 表示スタイル
    * 通常形式(Plain)
    * グループ形式(Grouped)

## UITableViewの使い方

* プロトコル
    * SwiftのプロトコルはJavaなどのインターフェイスの事です
    * UITableViewでは以下のプロトコルを実装する必要があります
        * UITableViewDataSource(データソース)
        * UITableViewDelegate(デレゲート)
    * データソースとデレゲートはUIPickerViewの所でも出てきました。iOSの部品ではよく使われます
    * 通常はViewControllerに実装します
* UITableViewDataSourceが実装する必要があるメソッド
    * データの行数を返すメソッド(必須)

        ```swift
           func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
               return 行数
           }
        ```

    * 表示するセル（中身）を返すメソッド(必須)

        ```swift
           func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
               let cell = UITableViewCell(style: .Default, reuserIdentifier: "myCell")
               cell.textLabel?.txt = "文字列"
               return cell
           }
        ```

    * 以下のメソッドは実装必須ではありませんが、必要な時には実装します
        * セクション数を返すメソッド(省略時は1となる)

        ```swift
             func numberOfSectionsInTableView(tableView: UITableView) -> Int {
                 return セクション数
             }
        ```

        * セクションのタイトル文字列を返すメソッド

        ```swift
             func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
                 return "セクションのタイトル文字列"
             }
        ```

        * セクションのフッター文字列を返すメソッド

        ```swift
             func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String {
                 return "セクションのフッター文字列"
             }
        ```

* UITableViewDelegateプロトコルの実装メソッド
    * セクションのタイトルの高さを返すメソッド

        ```swift
             func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
                 return セクションのタイトルの高さ
             }
        ```

    * セクションのフッターの高さを返すメソッド

        ```swift
             func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
                 return セクションのフッターの高さ
             }
        ```

    * セルの高さを返すメソッド

        ```swift
             func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
                 return セルの高さ
             }
        ```

    * テーブルビューで行を選択した時に呼ばれるメソッド

        ```swift
             func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
                 // 行が選択された時の処理
             }
        ```

