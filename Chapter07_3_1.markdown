# Chapter 7-3_1  部品にタグをつけて、タグでアクセスする方法

## タグでアクセスするセルの作り方
### 新規プロジェクト Chapter07-3-1 を作成します  
* 先ほどの Chapter07-4 のサンプルプロジェクトをコピペして作成しますので開いておいてください

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter07-3-1           |
   |Language             | Swift                   |
   |Devices              | iPhone                  |
   |View ControllerのSize| iPhone 4-inch           |

    * Main.storyboard 上の ViewController を削除します
    * Chapter07-4 の Main.storyboard 上のViewController をクリックして青くして、command+c(コピー)します
    * Chapter07-3-1 の Main.storyboard をクリックして　command+v(ペースト)します
    * Chapter07-3-1 の Main.storyboard で ViewControllerをクリックして青くして、アトリビュート・インスペクタの "is initial View Controller"のチェックが外れていたら、チェックを入れてください（ViewControllerに矢印が付きます）
    * ViewController.swift はChapter07-4 を開いて、全体を選択して command+c(コピー）　してChapter07-3-1 のViewController.swift の中身を消してから command+v(ペースト）してください
    * できたら実行してみて、Chapter07-4 と同じように動作する事を確認してください
        * 正常に動作する事が確認できたら、間違えないように、Chapter07-4プロジェクトは閉じてください

### テーブルビューに自作用のセルを追加します
* Main.storyboard を開きます
* TableView の上に Table View Cell をドラッグドロップします
* 上の方に Prototype Cells と出ます。ここに部品を配置します。この文字は実際に動作する時には出ませんので、あまり気にしないでください

* 追加したセルに Identifier をつけます
* セルの設定
    * 高さを 65.0 にする
    * 名前を "myCell2" にする

      |部品       |                 |サイズ・インスペクタ  |アトリビュート・インスペクタ     |
      |-----------|-----------------|-----------------|--------------------------|
      |セル        |Table View Cell  |Row Height -> 65 |Identifier -> "myCell2"   |


### 追加したセルの中に部品を追加します
* このセルの上に、ラベルを２つ左右に配置してください
    * 長い文字の改行の設定と、Tagの設定を行います

      |部品       |          |アトリビュート・インスペクタ                           |制約(Pin)                           |
      |-----------|----------|-----------------------------------------------|--------------------------------------|
      |ラベル１     |Label     |Lines -> 3, Line Breaks -> Word Wrap, Tag -> 1 |上 -> 0, 左 -> 0, 下 -> 0, 幅 -> 155  |
      |ラベル２     |Label     |Lines -> 3, Line Breaks -> Word Wrap, Tag -> 2 |上 -> 0, 左 -> 0, 右 -> 0, 下 -> 0    |



### プログラムでタグを使ってオブジェクトにアクセスします
* ViewControllerの セルを返すメソッドを書き換えます
* セル(MyCell2)を取得するには以下のようにします
    * 名前を指定してキューから取得します  
      （画面がスクロールされる事により不要となったセルは、キューに蓄えられます。それを再使用するとメモリーの節約になります）

    ```swift
            // セルを作ります
            let cell = tableView.dequeueReusableCellWithIdentifier("myCell2", forIndexPath: indexPath)
    ```

    * セル上の部品にタグでアクセスするには以下のように行います

    ```swift
            if let label1 = cell.viewWithTag(1) as? UILabel {
                // ラベルがnilでない時だけ処理を行う
            } 
   ```

    * ではセルを返すメソッドを以下のように書き換えます

    ```swift
        // セルを返す
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            // セルを作ります
            let cell = tableView.dequeueReusableCellWithIdentifier("myCell2", forIndexPath: indexPath)
            // このセルに表示するフォント名を取得します
            let fontName = fontName_array[indexPath.row]
            // 指定したフォントでサンプル文字を表示します
            if let label1 = cell.viewWithTag(1) as? UILabel {
                label1.font = UIFont(name: fontName, size: 14)
                label1.text = "ABCDE abcde 012345 あいうえお"
            }
            // サブテキストにフォント名を表示します
            if let label2 = cell.viewWithTag(2) as? UILabel {
                label2.textColor = UIColor.brownColor()
                label2.text = fontName
            }
            return cell
        }

    ```

    * できたら実行してみてください


![スクリーンショット 2016-03-25 17.11.17](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/ddd5fc109d/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-25_17.11.17.png)
