# Chapter 7-3_2 セルのカスタムクラスを作って、アクセスする方法

* 次にセルのカスタムクラスを作って、セルの部品にアクセスする方法をやってみます

## 先程作成したChapter07-3-1 を修正します
## UITableViewCellを継承してカスタムクラスを作ります

* プロジェクトのフォルダーを選択し、 File -> New -> File を選択します
* iOS -> Source -> Cocoa Touch Class を選択して　Next を押します
* 次の画面では以下の様にいれてクラスを作成します


    | 項目名     | 内容            |
    |------------|-----------------|
    |Class       |MyTableViewCell  |
    |Subclass of | UITableViewCell |
    |Language    | Swift           |

## 自作用のセルに、カスタムクラスを設定します

* クラスが作成できたら、セルとプログラムの紐付けをします
    * Main.storyboard で myCell2 を選択し、アイデンティティー・インスペクタで以下の様に設定します

     | 項目名                | 内容            |
     |-----------------------|-----------------|
     |Custome Class -> Class |MyTableViewCell  |

## セル上のラベルをアウトレット接続して、ラベル名をつけます
* 画面をアシスタントエディターにして、右画面にMyTableViewCellのソースが出るようにします
* セル上の部品のIBOutletを作成します


    |部品         |　        |IBOutlet名    |
    |------------|----------|--------------|
    |ラベル１     |Label     |sampleLabel   |
    |ラベル２     |Label     |fontNameLabel |


## プログラムでラベル名を使ってオブジェクトにアクセスします
* プログラムを次のように修正します

     ```swift
       // セルを返す
       func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
          // セルを作ります
          let cell = tableView.dequeueReusableCellWithIdentifier("myCell2", forIndexPath: indexPath) as! MyTableViewCell
          // このセルに表示するフォント名を取得します
          let fontName = fontName_array[indexPath.row]
          // 指定したフォントでサンプル文字を表示します
          cell.sampleLabel.font = UIFont(name: fontName, size: 14)
          cell.sampleLabel.text = "ABCDE abcde 012345 あいうえお"
          // サブテキストにフォント名を表示します
          cell.fonaNameLabel.textColor = UIColor.brownColor()
          cell.fonaNameLabel.text = fontName
          return cell
      }
     ```

* できたら実行してみてください。前と同じ画面になります

* こちらは、単なる番号でなく、ちゃんと意味のある名前でアクセスしますので、後でわかりやすくなります。通常このやり方で作成します








