# Chapter 7-4 テーブルでアプリを作る
## フォントリストアプリを作ってみよう
* それではフォントリストアプリを作成してみます
* フォントリストをテーブル表示するアプリです

### プロジェクトを作る
* 新規プロジェクトChapter07-4を作成します

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter07-4             |
   |Language             | Swift                   |
   |Devices              | iPhone                  |

### 画面をデザインする
* Main.storyboard を選択します
* 最初にViewControllerのサイズを変更しておいてください

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |View ControllerのSize| iPhone 4-inch           |

* テーブルビューを配置して、画面一杯に表示します
ライブラリペインから TableView をドラッグドロップして配置します 

   | 部品          |                |制約                     |
   |---------------|----------------|-------------------------|
   |テーブル        | TableView      |上下左右からの距離  0      |
    
* できたら実行してみてください

### プロトコルを設定する
* テーブルビューから dataSource と delegate を ViewController に設定します
    * TableView の上で control+クリックし、上部のViewControllerのアイコンまでドラッグしてドロップします
    * ポップアップが出ますので、datasource と delegate を設定します（２回行います）
    * コネクションインスペクタで正しく設定されているか確認してください

* ViewControllerクラスに２つのプロトコルを追加する
    * ViewControllerクラスに UITableViewDataSource と UITableViewDelegate のプロトコルを追加します

     ```swift
     class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

     ```

    * エラーが出ますが、これはメソッドがまだ定義されていないからです。メソッドを作成したら消えるので、そのままとします

### プログラミングする
* 画面の初期化を行うときに、フォント名を配列に入れる
    * 配列の用意

    ```swift
    // フォント名を入れる配列（文字列型の配列）を用意します
    var fontName_array = [String]()
    ```

    * 画面の初期化を行う時に、フォントを配列に入れます

    ```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // フォントファミリー名を全て調べます
        for fontFamilyName in UIFont.familyNames() {
            // そのフォントファミリー名が持っているフォント名を全て調べます
            for fontName in UIFont.fontNamesForFamilyName(fontFamilyName) {
                fontName_array.append(fontName)
            }
        }
    }
    ```

* プロトコルのメソッドの実装
    * 行数を返すメソッドを実装する

    ```swift
        // テーブルの行数を返す
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return fontName_array.count         }
    ```

    * 表示するセル（中身）を返すメソッド(必須)を実装する

    ```swift
       // セルを返す
       func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
           // セルを作ります
           let cell = UITableViewCell(style: .Subtitle, reuseIdentifier: "myCell")
           // このセルに表示するフォント名を取得します
           let fontName = fontName_array[indexPath.row]
           // 指定したフォントでサンプル文字を表示します
           cell.textLabel?.font = UIFont(name: fontName, size: 18)
           cell.textLabel?.text = "ABCDE abcde 012345 あいうえお"
           // サブテキストにフォント名を表示します
           cell.detailTextLabel?.textColor = UIColor.brownColor()
           cell.detailTextLabel?.text = fontName
           return cell
       }

     ```

* できたら実行してみます


![スクリーンショット 2016-03-25 14.16.44](http://repos.secnet.oec.local/uploads/OEC0155/swift_1/55c5d30ff1/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88_2016-03-25_14.16.44.png)
