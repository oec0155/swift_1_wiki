# Chapter 7-5 テンプレートMaster-Detail Application の説明
* iOSのテンプレートの中にMaster-Detail というパターンがあります
* Master Detail Application とは TableView画面(Master)とSimglePageView画面(Detail)をナビゲーション切替えで遷移するアプリです。
* このパターンでは、Split View Controller が使われています。これは、iPhone6 PLUS やiPadの横向きの場合に、画面を左右に分割して同時に表示できる機能です。

## プロジェクトの作成
* 新規プロジェクトChapter07-5-1 を Master-Detail Application で作成してください

   |  項目                     | 内容                      |
   |---------------------------|---------------------------|
   |テンプレート                  | Master-Detail Application |
   |Product Name               | Chapter07-5-1               |
   |Language                   | Swift                     |
   |Devices                    | iPhone                    |
   |Split View ControllerのSize| iPhone 4-inch             |


## 編集機能
* できたら iPhone5s のシミュレータで実行してください
    * ＋ボタンを押すと、行が新規追加されます
    * Editボタンを押すと、赤いマークが出て、そこをクリックするとDeleteボタンが出て、削除できます
    * Doneを押すと、赤いマークが消えます
    * 行をクリックすると、詳細画面に遷移します
    * ナビゲーターバーの **<Master** をクリックするとマスター画面に戻ります
    * なお、行の上で左にドラッグしてもDeleteボタンが出て、削除できます

## SplitViewControllerの機能
* 次に iPhone6S Plus で実行してみてください
    * 縦の状態だと同じ動きになります
    * 画面を横向きに変更してみてください
    * 左右に画面が分かれて、１画面でMasterとDetail画面が見えるようになります
    * これはSplitViewControllerの働きです

## プログラムソースの説明
* MasterViewController のソースを見ていきます
    * まず最初に起動した時に実行される、viewDidLoadメソッドです

     ```swift
       override func viewDidLoad() {
           super.viewDidLoad()
           // Do any additional setup after loading the view, typically from a nib.
           self.navigationItem.leftBarButtonItem = self.editButtonItem()

           let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
           self.navigationItem.rightBarButtonItem = addButton
           if let split = self.splitViewController {
               let controllers = split.viewControllers
               self.detailViewController = (controllers[controllers.count-1] as!
                   UINavigationController).topViewController as? DetailViewController
           }
      }
     ```

    * TableViewは編集機能を標準で備えており、そのボタンをナビゲーションバーの左に表示するのが、以下の部分です

     ```swift
           self.navigationItem.leftBarButtonItem = self.editButtonItem()
     ```

    * 追加ボタンの機能を右に表示するのは以下の部分です

     ```swift
           let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
           self.navigationItem.rightBarButtonItem = addButton
     ```

        * 実際に追加の処理を行うメソッドは insertNewObject で以下の部分です

     ```swift
      func insertNewObject(sender: AnyObject) {
          objects.insert(NSDate(), atIndex: 0)
          let indexPath = NSIndexPath(forRow: 0, inSection: 0)
          self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
     ```

    * 以下はテーブルビューのプロトコルの実装部分です

     ```swift
      // セクション数を返すメソッド
      override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
          return 1
      }
      // 行数を返すメソッド
      override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return objects.count
      }
      // テーブルのセルの中身を返すメソッド
      override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

          let object = objects[indexPath.row] as! NSDate  
          cell.textLabel!.text = object.description
          return cell
      }

     ```

    * 編集モードの時に実際にどの行が編集可能なのかは、以下で制御します

     ```swift
       override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
           // Return false if you do not want the specified item to be editable.
           return true
       }
     ```

    * 実際に編集する処理は以下です

     ```swift
      // テーブルの編集実行処理（今回は削除のみ）
      override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
          if editingStyle == .Delete {
              objects.removeAtIndex(indexPath.row)
              tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
          } else if editingStyle == .Insert {
              // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
          }
      }

     ```

        * 削除処理は既にできています
        * ここの Insert 処理は、行を指定して挿入する機能です。そのような機能も準備されています

    * 以下は画面遷移時の処理です

     ```swift
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
          if segue.identifier == "showDetail" {
              if let indexPath = self.tableView.indexPathForSelectedRow {
                  let object = objects[indexPath.row] as! NSDate
                  let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                  controller.detailItem = object
                  controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                  controller.navigationItem.leftItemsSupplementBackButton = true
              }
          }
      }

     ```

        * detail画面のナビゲーションバーの左ボタンに特殊な処理をしていますが、これはSPlitViewで向きによりボタンを切り替えるためです


## Master-Detail 画面の手作業での作成

* このようにテンプレートから作成すると、SplitViewNavigationで作成されますが、この機能は必ずしも必要ない場合もあります。
* iPhone5 での動作のみを行わせたい場合は、手作業で作成します。
* テキストのサンプルも特にSplitViewの説明は無いので、次で、手作業でのMaster Detail Application作成を行います。
