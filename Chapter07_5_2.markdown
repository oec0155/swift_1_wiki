# Chapter 7-5_2 手作業での Master-Detail アプリの作成
* Master-Detailテンプレートを使用すると、SplitViewControllerが使われ、複雑になります。
* テキストとは違い、手作業で Master-Detail アプリを作成してみます

## 新規プロジェクトを作る
* 新規プロジェクトを Single View Application で作成します

   |  項目               | 内容                    |
   |---------------------|-------------------------|
   |テンプレート            | Single View Application |
   |Product Name         | Chapter07-5-2           |
   |Language             | Swift                   |
   |Devices              | iPhone                  |
   |View ControllerのSize| iPhone 4-inch           |

## 詳細画面の作成
* 現在できている View Controller は詳細画面(detail)として使いますので、ラベルを中央に１つ配置します。
    * 制約の設定

    |  部品             | 制約                         |
    |------------------|------------------------------|
    | ラベル            | Horizontally in Container、Vertically in Continerにチェック、左右からの距離 0 |


    * プロパティの設定

    |  部品             | アトリビュートインスペクタ       |
    |------------------|------------------------------|
    | ラベル            | Alignment 中央、 View Controller の Title を Detailに |


* IBOutlet の作成
    * ラベルより ViewController に IBOutlet を作成します

    |  部品             | IBOutlet               |
    |------------------|------------------------|
    | ラベル            | dispLabel              |


* Master 画面からデータを受け取る為の変数の作成 
   * データ受け渡し用変数を ViewController に作成します


 ```swift
    @IBOutlet weak var dispLabel: UILabel!
    var object : String!
 ```


## Master 画面の作成
* ライブラリペインから Navigation Controller をドラッグドロップする
    * サイズを iPhone 4-inch にする
    * 以下の２つができています

    |  画面                 | 内容                          |
    |-----------------------|-------------------------------|
    |Navigation Controller  | ナビゲーション遷移を制御する       |
    |Root View Controller   | Master画面                     |

* 今作成した Navigation Controller を初期画面にします
    * 詳細画面の初期画面設定を外して、Navigation Controller に設定します

    |  画面                 | アトリビュート・インスペクタ                                         |
    |-----------------------|--------------------------------------------------------------|
    |View Controller        | View Controller -> is Initial View Controller のチェックをはずす |
    |Navigation Controller  | View Controller -> is Initial View Controller のチェックを入れる |


    * これで初期起動画面が Root 画面になりました。
 
## Master画面とDetail画面のセグエ接続
* Master画面とDetail画面をナビゲーション切り換えでセグエ(show)接続します
    * Root View Controller の上のアイコンの左端の　Root View Controlelr をcontrol+クリックして ViewController にドラッグドロップします
    * セグエのタイプは **show** です
    * セグエができたら、セグエに名前を付けます "showDetailSegue" とします

## Cell に名前をつける
* Root View Controller のセルに名前をつける
    * セルを選択してアトリビュートインスペクタの identifier に myCell と名前をつけます

## Master画面用の ViewController の作成
* UITalbeViewController のサブクラスとして View Controller を作成します
    * プロジェクトのフォルダーを選択して、 File -> New -> File を選択
    * iOS -> Source -> Cocoa Touch Class を選んで Next を押す
    * 以下のように入れます

    | 項目            | 内容                   |
    |-----------------|------------------------|
    |Class            | MasterViewController   |
    |Subclass of      | UITableViewController  |
    |Language         | Swift                  |

     （スーパークラスが **UITableViewController** になっている所を気をつけてください）


* Master画面と MasterViewController の紐付け
    * Root View Controller を選択してアイデンティティー・インスペクタでクラス名を設定します


    | 画面                 | アイデンティティー・インスペクタ                           |
    |----------------------|----------------------------------------------------|
    | Root View Controller | Custom Class -> Class に MasterViewControllerを選択 |


## Navigation バー表示用文字の設定
* Navigation Item のタイトルを変更

    | 画面                 | アトリビュート・インスペクタ                           |
    |----------------------|----------------------------------------------------|
    | Root View Controller | Navigation Item -> Title を Master に変更 |

* 以上で手作業での Master Detail Application ができました。これは Split View の機能はありません。

## プログラムの作成

* ここで出来た、MasterViewController を見てみてください。
* テンプレートから作成した時と違い、編集機能はコメントになっています。必要ならこのコメントを外して実装してください。
* 新規追加機能はありません。必要なら、テンプレートから出来たソースを参考に、右ボタンアイテムに実装してください。
 
### Master画面のリスト表示処理

* リスト表示データの準備

 ```swift
      // 初期値としてデータをセット
      let objects = ["マグロ","サーモン","エビ","はまち","いか","うなぎ"]
      // 選択されたデータを格納する変数
      var selObject  = ""
 ```


* テーブルのdataSource,delegateプロトコルのメソッドを作成


 ```swift
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return objects.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
        cell.textLabel!.text = objects[indexPath.row]
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selObject = objects[indexPath.row]
        performSegueWithIdentifier("showDetailSegue", sender: nil)
    }
 ```


### 画面遷移時の処理
* 画面遷移時に選択データを詳細画面の変数にセットします

 ```swift
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetailSegue" {
            let nextvc = segue.destinationViewController as! ViewController
            nextvc.object = selObject
        }
    }
 ```

### Detail 画面の処理

* 画面表示時に受け取ったデータをラベルに表示します

 ```swift
    override func viewWillAppear(animated: Bool) {
        dispLabel.text = object
    }
 ```

## 動作確認

* 以上でサンプルができました。動作確認してみてください
 

