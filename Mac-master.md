# Macの操作方法

## ドックの設定

* システム環境設定のDockで大きさ、自動的に隠す設定ができる
* ドックにアプリを追加したい時は、Lanunchpad よりドラッグドロップする

## 画面追加の方法

* 3本指で上にスライドして＋を押す
* デスクトップ上で control+クリックでデスクトップのバックグラウンドを変更で壁紙を変更できる

## 画面移動の方法

* 3本指で左右にスワイプ
* アプリのタイトルをつまんで左右に移動

## アプリの終了

* タイトルで赤ボタンで閉じるが、アプリは残っている
* メニューバーで終了でアプリは終了する

## スクロール方法

* エディタ、ブラウザでは、スクリーン上にカーソルを置き、2本指で上下するとスクロールする

## コピー、ペースト、削除のやり方

* command + c　でコピー
* command + v　でペースト
* command + x  でカット
* delete　は Windows の BS (前の文字が消える)
* fn+delete で Windows の Delete (後ろの文字が消える)

## ファイルの操作

* ドラッグドロップは　ファイルの移動
* option + ドラッグドロップ　でファイルをコピーできる
* command + クリック　で任意の複数ファイル選択ができる　 
* Finder の横展開表示では、シングルクリックでフォルダーを開く、ダブルクリックすると選択される
* フォルダーを選択して　ファイル-新規フォルダとしてもトップフォルダに作成される
* ファイルやフォルダーを選択して、command + delete で削除される（delete キーを押しても削除されない）

