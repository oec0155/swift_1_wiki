# 2.変数、型、文字列(Chapter03)

## ここでplaygroundを切り換えます
playground を一旦閉じて、新しく開いてください。 File → New → Playground を選び、myPlayground2 と入れます  
以下の章でも同じ様にplaygroundを切り換えてください。あまり多くなると、動きが悪くなります


## 変数
* 変数を作って、値を入れる

    ```
    var 変数名 = 値
    ```

    * 変数の宣言の頭に var をつけます  
    * 初期値も同時に入れられます  

* 例

    ```swift
    //  変数
    var fruit = "りんご"   // fruitという変数を作って"りんご"という値を入れる
    fruit = "みかん"       // fruitに"みかん"を入れる
    ```

## 定数
* 定数を作って、値を入れる

    ```
    let  変数名 = 値
    ```
    * 定数の宣言の頭には let を付けます

* 例

    ```swift
    let vegetable = "にんじん" 			//begerableという定数を作り、"にんじん"を入れる
    vegetable =  "じゃがいも"			//定数を変更する事はできない エラーとなる
    ```

    * 定数も変数と同じように、名前を付けて、値を入れておけます  
    * 但し、定数の値は変えられません。エラーとなります。左の赤いマークをクリックしてみてください。修正候補が提示されます

## 大文字と小文字の区別
* Swiftでは大文字と小文字は区別されます。これはクラス名、メソッド名、プロパティ名、予約語、変数、定数で影響します

    ```swift
    var device  =  "iPhone 7"
    var Device  =  "iPhone 7 Plus"
    print(device)
    print(Device)
    ```

    * 上の device と Device は別物となります  
    * 日本語や絵文字も使えるのですが、あまりお勧めしません  

## データ型
* 変数には他の言語と同じように型があります
* 基本データ型

    |データ型     | 表記   | サイズ      |説明          |
    |------------|--------|-------------|--------------|
    |整数型       |Int     |32bit環境では32bit、64bit環境では64bit | 符号付整数  |
    |実数型       |Float   |32bit        |単精度実数     |
    |実数型       |Double  |64bit        |倍精度実数     |
    |文字型       |String  |不定          |文字列        |
    |文字型       |Character|不定         |文字          |
    |真偽型       |Bool     |             |true 又は false|

* 整数型(Int)
    * 整数型(Int)は環境により、精度が変わりますが、精度を指定する以下の書き方もできます

    | 整数型  |サイズ   |符号  | 説明                       |
    |---------|--------|------|---------------------------|
    |Int8     |8bit    |あり  | -128～127                  |
    |Int16    |16bit   |あり  | -32,768～32,767            |
    |Int32    |32bit   |あり  | -2,147,483,648～2,147,483,647 |
    |Int64    |64bit   |あり  | -9,223,372,036,854,775,808～9,223,372,036,854,775,804 |
    |Uint8    |8bit    |なし  | 0～255                     |
    |Uint16   |16bit   |なし  | 0～65,535                  |
    |Uint32   |32bit   |なし  | 0～4,294,967,295           |
    |Uint64   |64bit   |なし  | 0～18,446,744,073,709,551,645|


## 型注釈
* 変数の宣言時に、型を明示的に指定する事ができます
* 型を指定する時は変数の後ろにコロン( **:** )を付けて型名を指定します

    ```swift
    var  myData10:Int         =  10             //Int型
    var  myData20:Double      =  10.1           //Double型
    var  myData30:String      =  "Hello"        //String型
    var  myData40:Bool        =  true           //Bool型
    var  myData50:Double      =  myData20       //Double型
    ```

## 型推論
* 変数には型があるのですが、いちいち指定しなくても、型を自動的に推論してくれる仕組みがあります

    ```swift
    var  myData1  =  10           //Int型
    var  myData2  =  10.1         //Double型
    var  myData3  =  "Hello"      //String型
    var  myData4  =  true         //Bool型
    var  myData5  =   myData2     //Double型  代入元が変数でも、型推論されます
    ```

* 型の確認方法

    1.調べたい変数をクリックする  
    2.Help -> Quick Help for Selected Item を選択する  
    3.型が表示される  

* myData5 の型を調べてみてください  

## 文字列データ
* 文字列データを作る時は、前後を"(ダブルコーテーション)で囲みます

    ```
    var  変数名	=	"文字列"
    ```

* 例

    ```swift
    var  helloStr	=	"Hello"
    ```

* 文字列データを結合する
    * 2つの文字列データは、「＋」で１つの文字列に結合する事ができます

    ```
    変数名    =    "文字列"    +    "文字列"
    ```
* 例

    ```swift
    var  helloStr1	=	"Hello"
    var  helloStr2	=	helloStr1	+	"World"
    print(helloStr2)
    ```

### 文字列補完

* 文字列中に変数の値を埋め込む
    * 文字列中に **\（変数）** で変数の値を入れられます（'\'はバックスラッシュでMacの場合「Optionキーを押しながら￥」を押してください）

    ```
    "文字列\(変数)文字列"
    ```

    ```swift
    let price = 100
    let message = "料金は\(price)円です"
    ```
    

## 型の機能

* 基本データ型はストラクチャでありプロパティとメソッドを持ちます

### プロパティ

* 公開されているプロパティには値や定数、変数に . (ピリオド)をつけ、続けてプロパティを書くことによりアクセスできます

    ``` swift
    変数.プロパティ
    ```

* String型のプロパティ isEmpty を使ってみます

    ``` swift
    var message10 = "Hello!"
    message10.isEmpty
    message10 = ""
    message10.isEmpty
    ```

### メソッド

* メソッドの呼び出しは以下のようにします

    ``` swift
    値.メソッド名()
    ```

* String型のメソッド append(_:) を使ってみます

    ``` swift
    var message20 = "Bonjour"
    message20.append("!!");
    ```

### イニシャライザ（コンストラクタ）

* 今まで変数を定義する時に、同時に値を代入して初期化してきましたが、変数はストラクチャーなので、イニシャライザ（コンストラクタ）で生成する事もできます
* イニシャライザ（コンストラクタ）は以下のように書きます

    ``` swift
    型名()
    ```

* 中身は決められた値で初期化されます

    ``` swift
    var string10 = ""
    var string12 = String()
    var int10 = Int()
    var double10 = Double()
    var float10 = Float()
    var bool10 = Bool()
    ```




## 型変換

* 異なる型の変数間では値を入る事ができません、そのような時には、型を変換する必要があります
* 型を変換するには、イニシャライザを使って簡単にできます
* 整数を文字列に変換する場合

    ```
    var  文字列の変数　　=   String(整数の変数)
    ```

* 例

    ```swift
    var  int20           =  100              //Int型
    var  str20:String    =  String(int20)    //String型
    ```

* 整数を実数に変換する場合

    ```
    var  実数の変数　　=   Double(整数の変数)
    ```

* 例

    ```swift
    var  int21          = 100               //Int型
    var  num21:Double   = Double(int21)     //Double型

    var  price20  = 100                       //Int型
    var  pay20    = Double(price20) * 1.085   //Double型
    ```

* 実数を整数に変換する場合

    ```
    var  整数の変数　　=   Int(実数の変数)
    ```

* 例

    ```swift
    var  num23          = 100.0         //Double型
    var  int23:Int      = Int(num23)    //Int型
    ```

* 文字列を整数に変換する場合

    ```
    var  整数の変数	=  Int(文字列の変数)
    ```

* 例

    ```swift
    var  str24           =  "100"        //String型
    var  int24:Int       =  Int(str24)!  //Int型
    ```

    * ここに出ている **!** マークについては後で説明します

* その他の変換

    |      | 整数     | 実数     |文字列               |
    |------|----------|----------|--------------------|
    |整数   |  -       |Int(実数) | Int(文字列)!        |
    |実数   |Double(整数)| -      |Double(文字列)!      |
    |文字列 |String(整数)|String(実数)| -              |






## タプル（複数の変数をまとめて利用する）
* 複数の変数をまとめて１つの変数のように扱う事ができます
* 書き方１

    ```swift
    var (name, age) = ("太郎", 21)
    print(name)
    print(age)
    ```

* 書き方２

    ```swift
    var person:(String,Int) = ("花子",22)
    print(person.0)
    print(person.1)
    ```

* 書き方３

    ```swift
    var student:(name:String,age:Int) = (name:"桜井",age:23)
    print(student.name)
    print(student.age)
    ```


タプルは関数やメソッドの引数や戻り値にも使えます  


# 基本データ型は参照型ではなく、値型です
* Swiftの基本データ型は値型です。従ってStringも、代入すると、コピーされます

    ```swift
    var  helloStr11  =  "Hello"
    var  helloStr12  =  helloStr11
    helloStr11 = "こんにちは"
    print(helloStr11)
    print(helloStr12)
    ```

* 比較すると中身の値を比較します

    ```swift
    var  helloStr21  =  "こんにちは"
    var  helloStr22  =  "hello"
    helloStr22 = "こん" + "にちは"
    if  helloStr21 == helloStr22 {
     	print("等しい")
    } else {
        print("違う")
    }
    ```






 


