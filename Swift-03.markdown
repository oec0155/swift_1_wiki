# 3. オプショナル
## 通常の変数に、nilを入れるとエラーになる
* プログラムの異常終了の原因の中で、変数の中身が **nil** (Javaなどのnullの事)である事が多くあります
* Swiftでは、これをコンパイル段階でチェックしてエラーとします
* 通常の変数にnilを入れるとコンパイルエラーとなります

```swift
var norStr:String =  "A"
norStr = nil		//←通常の変数にnilを入れようとしているのでコンパイルエラーとなる
```

## オプショナルの書き方
* しかし、プログラムによってはわざとnilを入れたい時があります。そのような時に、使うのがオプショナルです
* 型定義の後ろに「?」を付けます

```swift
var optInt:Int?
```

この変数 optInt はnil値を許します  
このような変数を「オプショナル型にラップされている」と言います  


```swift
var optInt:Int? = 2
optInt = nil		//←nilを入れてもコンパイルエラーとならない
```

### Optional型変数は通常変数に代入できない
* Optional型変数はnilの入る可能性がある変数であり、実際の中身がnilでなくても、通常変数に代入する事はできません（コンパイルエラー）

```swift
var norInt3:Int
var optInt3:Int? =  2
norInt3 = optInt3		//←通常変数にOptional Value変数を入れるとコンパイルエラーとなる
```

### Optional型変数を他の変数に代入する場合は、受け側もOptional型でないといけない
* Optional型変数からOptional型変数への代入はエラーとなりません

```swift
var optInt3:Int? =  2
var optInt4:Int?
optInt4 = optInt3		//←Optional型どうしなのでコンパイルエラーとならない（optInt3 の中身がnilでもエラーとならない）
```

### Optional型のアンラップ
#### 1.強制アンラップ(Forced Unwrapping) **「!」** 
* Optional型変数だが、中身はnilでない事をプログラマーが保障するので、通常変数として扱いたい時に使います
* Optional型変数を強制アンラップするには、変数の後ろに「!」を付けます

```swift
var norInt13:Int                 //通常変数
var optInt13:Int? =  2	         //Optional型変数
norInt13 = optInt13!             //←optInt3にはnilは入っていない事をプログラマが保障する。コンパイルエラーとならないがもしnilが入っていた場合は実行時エラーとなる
```

* しかし、もしnilが入っていたら、プログラムが異常終了するので、プログラムでは以下のようにチェックしてから代入するのが望ましい事になります

```swift
var norInt14:Int  =  1	 //通常変数
var optInt14:Int? =  2	 //Optional型変数
if   optInt14 != nil {
	norInt14 = optInt14!		
} else {
	 norInt14 = 0
}
print(norInt14)           // 2
```
optInt14がnilでない事を確認してから、Forced Unwrappingで通常の変数に代入しています

#### 2.オプショナル・バインディング)
* 前のようにプログラムでチェックしてからアンラップする処理は頻繁に発生する事になります
* そのような時は以下のようにOptional Bindingを使います

```swift
if  let  tmp  =  optInt14 {
	norInt14 =  tmp
} else {
	norInt14  =  0
}
print(norInt14)                  // 2
```
上のような処理を **オプショナル・バインディング** と呼びます  

* オプショナル・バインディングの簡略記法
    * 前の処理は以下の様にも書けます  

```swift
norInt14 =  optInt14 ?? 0
print(norInt14)
```

##### guard 文
* オプショナル・バインディングの代わりにguard文を使う事もできます
* nilのデータを処理したくない場合に使います

```swift
func validation() {
  guard let  tmp =  optInt14 else {
    print("error occurred")
    return
  }
  norInt14 = tmp
}
norInt14     // 2
```

#### 3.オプショナルチェーン **「?」**
* Optional型変数で、中身がnilの可能性がある時、nil以外の時だけ処理をして、nilの時は何もせずにnilを返してほしいという時、オプショナル値変数に？を付けて使います

```swift
var myStr21:String? = "abc"
var myStr23:String? = myStr21.uppercased()
``` 

上の例では　myStr21はオプショナル型変数に uppercased()（大文字変換メソッド）を呼び出そうとしています  
しかし、myStr21 の中身が nil の場合は変換できません。従って、Optional型へのこのメソッドの適用はコンパイルエラーとなります  

* myStr21の中身が nil でない事が確実なら、「!」をつけてForced Unwrapping する事ができます。しかし、中身が、nilであったら、実行時エラーが発生します  

```swift
var myStr24:String  = myStr21!.uppercased()
``` 

* 中身が nil の時だけ、変換して、 nil の場合はそのまま nil を返して欲しい時にオプショナルチェーンを使います

```swift
var myStr23:String? = myStr21?.uppercased()
``` 

* 上のようにOptional Chaining を使うと、nil の時もエラーは出なくなります  


#### Optional Value　のアンラップまとめ
* Optional Valueを uppercaseString に変換する場合は以下のいずれかの方法をとる事になります
    * 中身が確実にnilでない
        * 強制アンラップを利用
        * 変数に **!** を付ける
        * これはあまり勧められません。
    * 中身がnilの事もあるが、それ以外の時だけ変換する
        * オプショナルチェーンを利用
        * 変数に **?** を付ける
    * 中身が nil の時は "err" に変換し、それ以外のみ変換する
        * オプショナルバインディングを利用　　　

```swift
var  myStr31:String? = "abc"

//  Forced Unwrapping
var myStr34:String  =  myStr31!.uppercased()
//  Opional Chaining
var myStr33:String?  =  myStr31?.uppercased()
//  Optional Binding
if  let  tmp =  myStr31 {
	myStr34  =  tmp.uppercased()
} else {
	myStr34  =  "err"
}
```

* 前に、文字列を数字に変換する所で **!** 記号が出てきていました。そこでは Forced Unwrapping が使われていました

```swift
var  numStr:String = "1234"  
var  numInt2:Int = Int( numStr)!	
```

文字列は必ず数字に変換できるとはかぎらないので、オプショナル型が返ります、通常型に代入するには **!** を付ける必要があったのでした  
しかし、実際には以下のように オプショナルバインディングでちゃんとチェックを行うべきです  

```swift
if let tmp = Int( numStr) {
	numInt2 = tmp
} else {
	numInt2 = 0
}
```

#### 4.暗黙的アンラップ
* Optional型だが、アンラップが必要な時に、自動的にアンラップしてくれる
* 型定義時に **!** を付けます

```swift
var  myStr51:String! 
myStr51 = nil                                  //Optional型なのでnilを入れられる
myStr51 = "abc"

var myStr52:String  =  myStr51.uppercased()    // !を付けなくても自動的にアンラップしてくれる
```

これは変数定義時にはまだ値がセットできないが、プログラムの中で、必ずセットするのでいちいち ! をつけたくない時に使用します  

