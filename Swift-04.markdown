# 4. コレクション（配列 Array）
## 配列を作る

```
配列名 =  [ 値１，値２，値３]
```

配列の変数定義と初期値の設定を同時にできます  
初期値により、型推論されます

```swift
var intArray =  [1,2,3]                 // 整数の配列
var strArray =  ["A","B","C"]           // 文字列の配列
```

## 配列を作る（要素の型指定付き）

```
配列名：[ 型 ] =  [ 値１，値２，値３]
```

配列の変数の型定義をして、初期値を設定できます  
型定義と初期値の型が違っていたら、エラーとなります

```swift
var intArray2:[Int]    =  [1,2,3]  
var strArray2:[String] =  ["A","B","C"]
var intArray3:[Int]    =  ["A","B","C"]     //←型が合っていないのでエラー
```

## 空の配列を作る

2通りの方法があります

```
配列名：[型] = []
```

型指定有りの変数定義をして、初期値を与えていないので、空の配列ができます

```
配列名      = [型]()
```

イニシャライザで配列を作成します  
[型] は Array<型> のシンタックスシュガーです

どちらの書き方でもできます

```swift
var emptyArray1:[String] = []       //文字列型配列
var emptyArray2          = [Int]()  //整数型配列
```

型指定無しで、空の配列を作る事はできません

## サイズを指定して配列を作る（同じ値で初期化）

```
配列名      =  Array(repeating: 値, count: 個数)    //型推論
配列名:[型] =  Array(repeating: 値, count: 個数)    //型指定あり
配列名      =  [型](repeating: 値, count: 個数)     //型指定あり
```

いずれの書き方もできます

```swift
var intArrayIni1          = Array(repeating: 0, count: 3)            //整数型
var strArrayIni1          = Array(repeating: "Apple", count: 5)      //文字列型
var intArrayIni2:[Int]    = Array(repeating: 0, count: 3)            //整数型
var strArrayIni2:[String] = Array(repeating: "Apple", count: 5)      //文字列型
var intArrayIni3          = [Int](repeating: 0, count: 3)            //整数型
var strArrayIni3          = [String](repeating: "Apple", count: 5)   //文字列型
var intArrayIni4          = Array<Int>(repeating: 0, count: 3)            //整数型
var strArrayIni4          = Array<String>(repeating: "Apple", count: 5)   //文字列型
```

## 配列の個数を取得する

```
配列名.count
```

これはプロパティです  


```swift
var intArray10 =  [1,2,3]
print(intArray10.count)		// 3
var emptyArray11:[String] =  []
print(emptyArray11.count)	// 0
```

## 配列が空かどうか調べる

```
配列名.isEmpty
```

これもプロパティです。iOSの場合、よく使う機能は、プロパティで準備されています

```swift
var intArray20:[Int] =  [1,2,3]  
print(intArray20.isEmpty)		// false
var emptyArray20 =  [Int]() 
print(emptyArray20.isEmpty)		// true
```

## 配列の要素を取り出す（要素番号指定）

```
配列名[番号]	
```

最初の要素は 0番 です

```swift
var intArray21 =  [1,2,3]
print(intArray21[ 0 ])		// 1
```

## 配列の要素を取り出す（要素番号の範囲指定）

```
配列名[ 開始番号...終了番号 ]
```
 **...** は開始番号から終了番号までの範囲を表します。開始番号と終了番号はどちらも含まれます  
 **..<** は開始番号は含まれますが、終了番号は含まれません  
 **<..** や **<.<** はありません

```swift
var intArray30 =  [1,2,3]
print(intArray30[1...2])		// 2,3
print(intArray30[0..<2])		// 1,2
```

## 配列の全要素を取り出す

```
for  配列を一時的に入れる変数  in  配列名 {
    // 配列の処理
}
```

```swift
var strArray30 =  ["A","B","C"] 
for  val  in  strArray30  {
    print( "要素=\(val)" )        // 要素=A  要素=B  要素=C
}
```

playground で実行結果を見るには、画面下方の▽を押して、コンソールを表示してください


## 配列の全要素を取り出す(要素番号も必要な時)

```
for  （要素番号,配列を一時的に入れる変数)  in 配列名.enumerated() {
    // 配列処理
}
```

enumerated()メソッドを使用して、タプルで受け取ります

```swift
var strArray40 =  ["A","B","C"] 
for  (index, val)  in  strArray40.enumerated()  {
    print( "要素[\(index)]=\(val)" )
}
```

## 配列の最後に要素を1つ追加する
* 配列の一番最後に、要素を1つ追加するには、 append(要素)を使います


``` 
配列名.append(追加要素)
```

```swift
var strArray43 = ["A","B","C"]
strArray43.append("D")		// ["A","B","C","D"]
```

## 配列の指定位置に要素を1つ追加する
* 配列の指定位置に、要素を1つ追加するには　insert(要素,at:位置)を使います

```
配列名.insert(要素, at: 指定位置)
```

```swift
var strArray44 = ["A","B","C","D"]
strArray44.insert("X", at:1)	// ["A","X","B","C","D"]
```

指定の位置に新しい要素が入り、残りの要素は順次後ろにずれる    
（ここで引数が２つのメソッドが出ています。SwiftはObjective-cの後継言語なので、Objective-cの規則を引き継いでおり、第２引数以下については、引数名が必要です。引数の前に名前とコロンを付けます。）



## 配列の一番最後の要素を1つ削除する
* 一番最後の要素を1つ削除するには　removeLast()を使います

```
配列名.removeLast()
```

```swift
var strArray45 = ["A","X","B","C","D"]
strArray45.removeLast()		// 戻り値は削除したデータ　"D"
strArray45			// ["A","X","B","C"]
```

## 配列の指定位置の要素を1つ削除する
* 指定位置の要素を1つ削除するには　remove(at:指定位置)を使います

```
配列名.remove(at:位置)
```

```swift
var strArray46 = ["A","X","B","C"]
strArray46.remove(at:1)		// 戻り値は削除したデータ　"X"
strArray46			// ["A","B","C"]
```

## 配列の要素を全て削除する
* 配列の要素を全部削除するには、removeAll()を使います

```
配列名.removeAll()
```

```swift
var strArray47 = ["A","B","C"]
strArray47.removeAll()
print(strArray47.isEmpty)		// true
```

## 配列に別の配列を追加する
* 配列と配列を結合するには + を使います

```
var 新配列 = 配列1 + 配列2		//二つの配列を結合して新しい配列を作る
配列3 += 配列2				//配列の最後に別の配列を追加する
```
最初のやりかたでは配列1,配列2 の中身は変更されません

```swift
var strArray50 = ["A","B","C"]
var plusArray = ["D","E"]
var newArray = strArray50 + plusArray
newArray				// ["A","B","C","D","E"]
strArray50				// ["A","B","C"] 前のまま
strArray50 +=  plusArray
strArray50				// ["A","B","C","D","E"]
```

## 配列をソートする
* 配列のソートをするには、 **sorted(by: 関数 )** を使います、引数には並べ替えの大小判定関数を指定します

```
配列名.sorted(by: { $0 < $1 } )	// 昇順
配列名.sorted(by: { $0 > $1 } )	// 降順
```
 **{ }** はクロージャーと言って、関数の中身だけを書いたものです（クロージャーについては後で説明します）
$0,$1 はそれぞれクロージャーに渡される第１引数、第２引数を表します  
 **条件式**  は条件を満たせば true 、満たさなければ false を返します

```swift
var intArray53 = [5,3,1,4,2,21,10]
var outArray53 = intArray53.sorted(by: { $0 < $1 } )
outArray53                                      // [1,2,3,4,5,10,21]
outArray53 = intArray53.sorted(by: { $0 > $1 } )
outArray53                                      // [21,10,5,4,3,2,1]
intArray53.sort(by: { $0 < $1 })
intArray53                                      // [1,2,3,4,5,10,21]
```
sorted(by:)メソッドは配列の中身は変更しません。変更された配列を別に作って返します  
配列その物を置き換えたい時（破壊的ソート）は、sort(by:) を使います

## 配列の要素を置き換える
* 配列の要素（中身）を置き換えるには要素番号で指定します

```
配列名[要素番号] =  新しい要素
```

```swift
var strArray54 = ["A","B","C"]
strArray54[1] = "D"
strArray54                      // ["A","D","C"]
```

## 配列の複数の要素を置き換える
* 配列の並んだ複数の要素を同時に置き換える事もできます

```
配列名[開始要素番号...終了要素番号] =  新しい要素配列
```
置き換える元と新しい配列の長さが違ってもできます

```swift
var strArray55 = ["A","B","C","D"]
strArray55[1...3] = ["X","Y" ]
strArray55						// ["A","X","Y"]
strArray55[1...2] = ["B","C","D" ]
strArray55						// ["A","B","C","D"]
```

* 一部を削除したり、空配列にする事もできます

```swift
var strArray56 = ["A","B","C","D"]
strArray56[1...1] = []
strArray56					// ["A","C","D"]
```
上の、[1...1]の部分は、[1] だと、右辺に要素("D"など)が来るべきなので、エラーとなります

```swift
var strArray57 = ["A","B","C"]
strArray57[0...2] = []
strArray57					// []
```




