# 6.コレクション（集合 Set）
## Set（重複不可データ）
* 集合（Set）は異なる物の集まりを表し、順番も管理しません
* Setデータは同じデータは１件しか登録できません

## Setデータを作る

* 各括弧 [ ] を使って、値を並べて作ります。
* Array型と同じです。 
* Setの場合、型注釈（:Set）が必要です。省略すると、Array型となってしまいます  


```
セットデータ名:Set       =  [値, 値, .... ]
```

```swift
var  favorite1:Set = [ "ロック", "クラシック", "ヒップポップ"]
favorite1                                            // [ "クラシック", "ヒップポップ", "ロック"]
var  favorite2:Set = [ "ロック", "クラシック", "ロック"]  
favorite2                                            // [ "クラシック", "ロック"]
```

同じデータを２件入れる事はできません。又、入れた順番も管理されません


## Setデータを作る（型指定あり）

* Set の後ろに<>をつけて型を指定する事ができます。初期値を指定しない場合は必要です

``` swift
セットデータ名:Set<型名>  =  [値, 値, .... ]
```


```swift
var favorite3:Set<String> = ["ロック", "クラシック", "ヒップポップ"]
favorite3                                            // [ "クラシック", "ヒップポップ", "ロック"]
```

## 空のSetデータを作る
* 空のSetデータを作る時は、初期値が無いので、型指定<型>が必要です

``` swift
セットデータ名:Set<型>  = []  又は  
セットデータ名  =  Set<型>()
```

```swift
var emptySet1:Set<String> = []
var emptySet2  = Set<String>()
```

## Setデータの個数を取得する
*Setデータの個数を調べるには、countプロパティを使います

```
Setデータ名.count
```

```swift
var strSet3:Set<String> = ["ロック", "クラシック", "ヒップポップ"]
print(strSet3.count)                  // 3
print(emptySet1)                      // 0
print(emptySet2)                      // 0
```

## Setデータが空かどうか調べる
* Setデータが空かどうか調べるには、isEmptyプロパティを使います

```
Setデータ名.isEmpty
```

```swift
print(strSet3.isEmpty)                 // false
print(emptySet1.isEmpty)               // true
print(emptySet2.isEmpty)               // true
```

## Setデータの要素を取り出す
* 配列のように、インデックスを指定して特定の要素を取り出す事はできません

```swift
print(favorite1[0])                 // エラー
```

## Setデータの要素を全部見る
* for in 文を使うと、全要素を見ていくことができます
* 但し、Setデータに入れた順番に並んでいるわけではありません

```
for 変数 in Setデータ名{
    //Setの処理
}
```

```swift
var set1:Set = [1, 2, 3, 4, 5]
for  val in set1 {
	print("set1=\(val)")		// seta=5
                                        // seta=2
                                        // seta=3
                                        // seta=1
                                        // seta=4  
}

```

## Setデータに要素を追加する
* 要素を追加するには、insertメソッドを使用します


```
Setデータ名.insert(要素)
```

```swift
favorite1.insert("ジャズ")
favorite1.insert("ジャズ")
favorite1                  // {"クラシック", "ロック", "ジャズ", "ヒップポップ"}
```

* 追加した要素が最後に入る訳ではありません、又、Setデータの特性上、同じデータを複数登録しても１件しか入りません

## Setデータの要素を削除する
* 要素を削除するには、remove(要素)メソッドを使用します
* 戻り値には削除された要素の値が返ります

```
辞書データ名.remove(要素) -> 削除された要素
```

```swift
var removed = favorite1.remove("ロック")
favorite1                               // {"クラシック", "ジャズ", "ヒップポップ"}
print( removed )                        // Optional("ロック")
```

削除データは必ずあるとは限りません、無い場合nilが返るので Optional 型が返ります  

 
## Setデータの要素を全て削除する
* 要素を全て削除するには、removeAll()を使用します

```
Setデータ名.removeAll()
```

```swift
favorite1.removeAll( )
favorite1                         // []
```

以下でも削除できます  

```swift
favorite2 = []
favorite2
```

## 集合演算
* Set型では２つのSet型の集合演算を行う事ができます

    |集合     | メソッド    |
    |--------|-------------|
    |和集合   |union()      |
    |積集合   |intersection() |
    |排他的論理和|symmetricDifference() |
    |差集合   |subtracting() |


``` swift
let seta:Set = [1, 2, 3, 4, 5]
let setb:Set = [3, 4, 5, 6, 7]
seta.union(setb)                   // {2, 4, 5, 6, 7, 3, 1}
seta.intersection(setb)            // {5, 3, 4}
seta.symmetricDifference(setb)     // {6, 7, 2, 1}
seta.subtracting(setb)             // {2, 1}
```






