# 5.コレクション（辞書）
## Dictionary（辞書データ）
* 辞書データは連想配列の事です

```swift
var  miyage = [ "岡山":"吉備団子", "広島":"もみじ饅頭"]
var  kaimono = miyage["岡山"]!
print(kaimono)				// "吉備団子"
```
辞書の場合、指定したキーに対応した値があるかどうかわからないので、Optional型(String?)が戻ります。  
上の例では、強制アンラップしています  

## 辞書データを作る
* 各括弧 [ ] を使って、キーと値をセットで並べて作ります。キーと値の間にはコロン「:」 、各ペアはカンマ「,」 で区切って並べます  
(JavaScript などは { } を使いますので間違えないようにしてください)

```
var 辞書データ名  =  [キー:値, キー:値, .... ]
```
辞書の変数定義をして、初期値をセットします。  
初期値により、型推論されます

```swift
var intDict = ["a":1, "b":2, "c":3]
var strDict = ["a":"A", "b":"B", "c":"C"]
var chugokuDict = [1:"岡山",2:"広島",3:"山口",4:"島根",5:"鳥取"]
```

## 辞書データを作る（型指定付き）
* 型指定をする事もできます

```
var 辞書データ名:[キーの型:値の型]  =  [キー:値,キー:値, .... ]
```
キー、値、それぞれの型を指定します。  
初期値と型が違うとエラーとなります

```swift
var intDict2:[String:Int] = ["a":1, "b":2, "c":3]
var strDict2:[String:String] = ["a":"A", "b":"B", "c":"C"]
var chugokuDict2:[Int:String] = [1:"岡山",2:"広島",3:"山口",4:"島根",5:"鳥取"]
```
キーには、String,Int,Double型が使えます  

## 空の辞書データを作る
* ２通りの作り方があります  

```
var 辞書データ名:[キーの型:値の型]  = [:]
```

型指定をして辞書の変数を作成し、中身を空で初期化しています


```swift
var emptyDict:[String:Int] = [:]
```

イニシャライザを使ってもできます

```
var 辞書データ名  =  [キーの型:値の型]()        // これは以下のシンタックスシュガーです
var 辞書データ名  =  Dictionary<キーの型,値の型>()  
```

```swift
var emptyDict1 = [String:Int]()
```



## 辞書データの個数を取得する
* 辞書データの個数を調べるには、countプロパティを使います

```
辞書データ名.count
```

```swift
print(intDict.count)				// 3
print(strDict.count)				// 3
print(emptyDict.count)				// 0
```

## 辞書データが空かどうか調べる
* 辞書データが空かどうか調べるには、isEmptyプロパティを使います

```
辞書データ名.isEmpty
```

```swift
print(intDict.isEmpty)				// false
print(strDict.isEmpty)				// false
print(emptyDict.isEmpty)			// true
```

## 辞書データの要素を取り出す
* 辞書データの要素にアクセスする時は、[] の中にキーを指定します

```
辞書データ名[キー]
```

```swift
var strDict5 = ["a":"A", "b":"B", "c":"C"]
var val  = strDict5["b"]!
print(val)				// B
```

辞書データは、キーがみつからない場合はnilを返します。従って、受け取る値はOptionalValueとなります  

* キーが無い可能性がある場合は、オプショナルバインディングでnilかどうか確認して使います

```swift
var strDict6 = ["a":"A", "b":"B", "c":"C"]
if  let  tmpVal = strDict6["c"] {
	print("find=\(tmpVal)")
}else{
	print("not found.")
}
```

## 辞書データの要素を全部見る
* for in 文を使うと、全要素を順番に見ていくことができます
* 但し、辞書データに入れた順番に並んでいるわけではありません

```
for (キー用変数,値用変数) in 辞書データ名{
    //辞書の処理
}
```

```swift
var strDict10 = ["a":"A", "b":"B", "c":"C"]
for  (key ,val) in strDict10 {
	print("strDict10[\(key)]=\(val)")		// strDict[b]=B  strDict[a]=A  strDict[c]=C  
}
```
全要素を取り出しているので、キーは必ず存在するので、ノーマル変数となります

* （参考）キーの昇順にリストするサンプル

```swift
var strDict11 = ["a":"A", "b":"B", "c":"C"]
var keys = Array(strDict11.keys)
keys.sort{ $0 < $1 }
for  key in keys {
	print("strDict11[\(key)]=\(strDict[key]!)")
}
```
キー指定でデータを取り出しているので、Optional Value となります

## 辞書データに要素を追加する
* 要素を追加するには、新しいキーを指定して、要素を代入します

```
辞書データ名[新しいキー] = 値
```

```swift
var strDict20 = ["a":"A", "b":"B", "c":"C"]
strDict20["z"] = "Z"				// ["a":"A", "b":"B", "c":"C", "z":"Z"]
strDict20
```

(もし、既にキーが存在したら、置き換えとなります)

## 辞書データの要素を削除する
* 要素を削除するには、removeValueForKey(キー値)を使用します
* 戻り値には削除された要素の値が返ります

```
辞書データ名.removeValueForKey(キー値) -> 古い値
```

```swift
var strDict21 = ["a":"A", "b":"B", "c":"C"]
let oldValDel = strDict21.removeValue(forKey: "a")
print(oldValDel)                                       // Optional("A")
strDict21                                              // ["b":"B", "c":"C"]
```
oldValDel は指定したキーが無い場合nilが返るので Optional Value です  

 
* 値に nil をセットしても削除できます

```swift
var strDict22 = ["a":"A", "b":"B", "c":"C"]
strDict22["a"] = nil
strDict22                                       // ["b":"B", "c":"C"]
```
逆に、値として nil をセットしたい時は、そのままではできません（後で説明します）

## 辞書データの要素を全て削除する
* 要素を全て削除するには、removeAll()を使用します

```
辞書データ名.removeAll()
```

```swift
var strDict30 = ["a":"A", "b":"B", "c":"C"]
strDict30.removeAll( )
strDict30                                       // [:]
```

以下でも削除できます  

```swift
var strDict31 = ["a":"A", "b":"B", "c":"C"]
strDict31 = [:]
strDict31
```

## 辞書データの値に nil を入れたい場合
### 辞書データの型定義でオプショナル型を定義します

```swift
var strDict32:[String:String?] = ["a":"A", "b":"B","c":"C"]
strDict32.updateValue(nil , forKey: "a" )
strDict32
```

以下のように書くと、Optional Value でも削除されてしまいます

```swift
var strDict33:[String:String?] = ["a":"A", "b":"B","c":"C"]
strDict33["b"] = nil
strDict33
```

## 辞書データの値を変更する
* 以下の２通りの方法があります
    * 要素の値に直接代入する
    * updateValue(キー値)を使用する

```
辞書データ名[キー値] = 値
辞書データ名.updateValue( 値 , forKey: "キー値")		-> 	古い値
```

```swift
var strDict40 = ["a":"A", "b":"B", "c":"C"]
strDict40["a"] = "AB"
strDict40                                         // ["a":"AB", "b":"B", "c":"C"]

let oldValUp = strDict40.updateValue( "BB" ,forKey:"b" )
print(oldValUp)						//Optional("B")
strDict40							// ["a":"AB", "b":"BB", "c":"C"]
```
updateValue() の場合、変更前の値が戻ります  
もし該当キーが無い場合、追加となり、nilが返ります  
単なる代入の場合も該当キーが無い場合は追加となります  

