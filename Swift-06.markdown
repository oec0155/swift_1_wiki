# 6.制御文
## if文
* if文は以下のように書きます

```
if 条件式 {
    条件が満たされていたらする処理
}
```

* 条件式に ( ) は不要です
* 処理の { } はたとえ１行でも、省略できません

```swift
let price = 1000
if price >= 100 {
    print("価格は100円以上")
}
```

## if else 文
* if else 文は以下のように書きます

``` 
if 条件式 {
    条件が満たされていたらする処理
} else {
    条件を満たさない時にする処理
}
```

## 条件式
* 比較演算子

 |比較演算子   | 説明                  |
 |-----------|-----------------------|
 |a == b     | a と b が同じかどうか    |
 |a != b     | a と b が違うかどうか    |
 |a < b      | a が b より小さいかどうか |
 |a <= b     | a が b 以下かどうか      |
 |a > b      | a が b より大きいかどうか |
 |a >= b     | a が b 以上かどうか      |


    ```swift
    var strA = "abc"
    var strB = "ab" +  "c"
    if strA == strB {
        print(" strA と strB は等しい")
    }
    ```

* java と違い、String型の場合、 == は　strA と strB が同じオブジェクトかどうかでなく、中身が同じかどうか比較します

* オブジェクトの比較演算子(===)

    |比較演算子 |説明                     |
    |---------|-------------------------|
    |a === b  | a と b が同じオブジェクト  |
    |a !== b  | a と b が異なるオブジェクト |

    (String型は構造体なので使えません）

    ```swift
    class A{
        var prop:Int = 0
    }

    var instanceA = A()
    var instanceB = instanceA
    instanceA.prop = 1
    if instanceA === instanceB {
        print("instanceA と instanceB は同じオブジェクト")
        print("従って、instanceB.prop=\(instanceB.prop)")
    }
    ```

## 制御文
### switch 文
* switch 文は以下のように書きます

```
switch 変数 { 
case 状態1:
    状態1のときにする処理 
case 状態2:
    状態2のときにする処理
default:
    どの状態でもないときにする処理 
}
```

変数に ( ) は不要です  


* 暗黙的 break
    * swift ではswitch文にbreak は不要です

```swift
// switch文
var dice = 1
switch dice {
case 1:
    print("振り出しに戻る")    // "振り出しに戻る"
case 2,5:
    print("もう１回振ります")
default:
    print("出た目の数だけ進む")
}
```

dice の値を変えてみてください

* fallthrough
    * 次の処理に流れ込ませたい時は fallthrough を使います

```swift
// fallthrough
dice = 5
switch dice {
case 1:
    print("振り出しに戻る")
case 2,5:
    print("もう１回振ります")    // "もう１回振ります"
    fallthrough
default:
    print("出た目の数だけ進む")  // "出た目の数だけ進む"
}
```

* case文の処理を省略することはできません
    * 複数の値で同じ処理をする場合はコンマでつなげます、又、範囲演算子も使えます


```swift
dice = 5
// 以下はエラー
switch dice {
case 2:
case 5:
    print("もう１回振ります")    // "もう１回振ります"
default:
    print("出た目の数だけ進む")
}
// switch文での範囲演算子の利用
dice=10
switch dice {
case 2,5...7,9..<11:
    print("もう１回振ります")    // "もう１回振ります"
default:
    print("出た目の数だけ進む")
}
```

範囲演算子には数値型(Int,Float,Double)しか使用できません



* default文 は必須です。
    * enum型等で、明らかにすべての条件がチェックされている時以外は、省略するとエラーとなります

* switch文のバリューバインディング
    * 条件の中に変数を定義し、処理で使う事ができます

```swift
// バリューバインディング
dice = 6
switch dice {
case 1:
    print("振り出しに戻る")
case 2,5:
    print("もう１回振ります")
case let x where x < 7:
    print("\(x)だけ進む")
default:
    print("不正です")
}
```


### while 文

* 条件を満たす間処理を繰り返します

```
while 条件式 {
    繰り返す処理
}
```
* if文と同じく、条件式に ( ) は不要です

```swift
// while文
var count = 0
while count < 5 {
    count += 1
    print("\(count)回目です");
}
```

### repeat-while 文
* 条件を満たす間処理を繰り返します。最低でも１回は処理を実行します

```
repeat {
   繰り返す処理
} while 条件式
```

```swift
// repeat-while文
count = 0
repeat{
    count += 1
    print("\(count)回目です");
}while count < 5
```

### for 文
* カウンターを使って、指定した回数だけ処理を繰り返します
* 以下の書き方は廃止されました

```
for 初期化処理; 繰り返し条件; 増分処理 {
    繰り返す処理
}
```


### for-in レンジ
* 初期値から終了値まで処理を繰り返す

```
for 変数 in 初期値...終了値 {
    繰り返す処理
}
```

``` swift
// for 文
for count in 1...5 {
    print("\(count)回目です");
}
```

* 変数の前に、var は不要です
* 範囲演算子は **..<** も使えます

### for-in コレクション
* 配列の値を順に取り出して処理を行う

```
for 変数 in 配列 {
    繰り返す処理
}
```

* 変数の前に、var は不要です

```swift
// for-in コレクション(配列)
let pets = ["ポチ", "タマ", "チョコ", "マロン", "モモ" ]
for pet in pets {
    print(pet)
}
```

* 辞書も同じようにできます

```swift
// for-in コレクション(辞書)
let pets = ["犬":"ポチ", "猫":"タマ", "うさぎ":"チョコ", "ハムスター":"マロン", "トカゲ":"モモ" ]
for (pet, namae) in pets {
    print("\(pet)=\(name)")
}
```

* 上の場合、出力される、順番は入れた順番とはなりません

