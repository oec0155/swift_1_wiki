# 8.クラス
## クラスの基本形

```
class クラス名:親クラス名 , プロトコル名 {
    var プロパティ名 = 値        // プロパティ

    init(){                // イニシャライザ
        // 初期化処理
    }

    func メソッド名() {       // メソッド
        // メソッドの処理
    } 
}
```

* 言葉の意味

 | 言葉          | 意味                               |
 |--------------|------------------------------------|
 |プロパティ      | フィールドと同じ                     |
 |プロトコル      | インターフェイスと同じ                |
 |イニシャライザ   |コンストラクタと同じ                   |
 |メソッド        |メソッド。記法は関数と同じ。親クラスのメソッドをオーバーライドする時は、頭に override が必要 |
 |スコープ        | 以下があります                       |


 | スコープ       |説明                    |
 |--------------|------------------------|
 |public        |どこからでもアクセス可      |
 |internal      |同じプロジェクト内でアクセス可（規定値） |
 |private       |同じファイル内からのみアクセス可 |

## クラスの生成
クラスの生成（ジェネレート）は以下のようにします

```
var インスタンス変数 = クラス名()
```

* Javaのように、new は必要ありません

## プロパティ
プロパティは以下のようにして定義します

```
class クラス名 {
    var プロパティ名 = 初期値    // 初期値も設定する場合
    var プロパティ名:型         // 変数の定義だけで初期値は設定しない場合（OptionalValue　となる）
    var プロパティ名:型!        // イニシャライザで初期化するのでOptional Valueとしたくない場合
}
```

* プロパティのスコープは初期値は internal と
なります

* プロパティへのアクセスは以下のように行います

```
インスタンス変数.プロパティ名 = 値      // 代入 
変数 = インスタンス変数.プロパティ名    // 参照
```

```swift
// クラスの例
class MyClass {
    var myProperty = "Hello"
}

var myObj = MyClass()
myObj.myProperty = "こんにちは"
var msg = myObj.myProperty
print("myProperty=\(msg)")        // myProperty=こんにちは
```

* このように、swiftではプロパティへのアクセスにアクセッサを準備するのではなく、直接プロパティにアクセスします
* その場合プロパティが無防備になるので、他のファイルからは参照のみに制限したい時は以下のように書きます

```swift
class MyClass {
    private(set) var myProperty = "Hello"
}
```

* 上の場合、myProperty には他のファイル（クラス）からは、値をセットできません。

## メソッド
メソッドは以下のように書きます

```
class クラス名{
    func メソッド名( 引数名:型名, ・・・ ) -> 型名 {
        // 処理 
    }
}
```

* 書き方は関数と同じです
* 引数、戻り値がないメソッドも関数と同じように定義できます

```swift
// メソッド
class MyClass2 {
    var myProperty = "Hello"
    func myFunc(msg:String, who:String) {
        print("myFunc:メソッド実行時=\(msg) \(who)")
    }
}
var myObj2 = MyClass2()
myObj2.myFunc(msg: "GoodBye", who: "Mark")    // myFunc:メソッド実行時=GoodBye Mark
```

* 上のように外部引数名が必須（省略時は内部引数名が使われる）も関数と同じです

## イニシャライザ
イニシャライザは以下のように書きます

```
class クラス名{
    init( 引数名:型名,・・・ ){
        // 初期化処理 
    }
}
```

* イニシャライザはクラスを生成する時に実行されます（Javaのコンストラクタ）
* やる事がない場合は、イニシャライザは省略できます
* 関数と同じ書き方ですが、func は不要です
* 名前は init です
* 引数は取れますが、戻り値はありません。
* 引数は無いもの、複数あるものがそれぞれ定義できます。
* イニシャライザは名前が init しかないので、第１引数から外部引数名となります

```swift
// クラスの例（イニシャライザ付き）
class Person {
    var age = 0
    var name = ""
    
    init(age:Int){
        self.age = age
    }
    
    init(name:String){
        self.name = name
    }
    
    init(age:Int,name:String){
        self.age = age
        self.name = name
    }
    func desc(){
        print("age=\(age),name=\(name)")
    }
}

var tom   = Person(age:5, name:"トム")
var jerry = Person(age:3)
jerry.name = "ジェリー"
tom.desc()                // age=5,name=トム
jerry.desc()              // age=3,name=ジェリー
```

* self は Java の this と同じです

## 継承
クラスを継承したい時は以下のように書きます

```
class クラス名:親クラス名 , プロトコル名 {
    var プロパティ名 = 値
    init(){
        // 初期化処理
    }
    func メソッド名() {
        // メソッドの処理
    } 
}
```

* 上のように、クラス名に続けて、 **:親クラス名** と書きます。
* 必要なら、後ろにプロトコルを好きなだけ書きます

```swift
// 継承
class Man:Person{
    var weight = 0
    init(age:Int,name:String,weight:Int){
        self.weight = weight
        super.init(age:age,name:name)
    }
    override func desc(){
        print("age=\(age),name=\(name),weight=\(weight)kg")
    }
}
var popeye = Man(age:20,name:"ポパイ",weight:50)
popeye.desc()            // age=20,name=ポパイ,weight=50kg
```

## computed プロパティ
実際にプロパティがあるわけではないが、あたかもあるかのように見せる

```
class クラス名 {
    var プロパティ名:型 {
        get { 
            処理
            return 値 
        }
        set(値){ 
            処理
        } 
    }
}
```


```swift
// extension と computed property
extension Man{
    var pound:Int{
        get{
            return Int(Double(weight) / 0.45)
        }
        set(pound){
            weight = Int(0.45 * Double(pound))
        }
    }
}

popeye.pound = 100
popeye.desc()
```

* 上の例では pound というプロパティがあたかもあるかのように見えます
* set を省略すると、読み込み専用となります


## extension



* extension は、既存のクラスにメソッドやcomputedプロパティが追加できます。但し、プロパティは追加できません

``` swift
    extension 対象の型名 {
        拡張させる機能
    }
```

## 構造体 struct

ストラクチャの定義はクラスに非常に似ています。

``` swift
struct ストラクチャ名 {
}
```


``` swift
struct Fuel {
    let max = 30
    var used = 0
    var remaining: Int {
        return max - used 
    }
}
```

これは自動車の燃料を表現したストラクチャです


* ストラクチャはクラスと違い、値型です

``` swift
var fuelA = Fuel()
fuelA.used = 10
var fuelB = fuelA
fuelB.used = 20
fuelA.remaining        // 20
```

* ストラクチャはインスタンスを定数に代入した場合、インスタンスのプロパティは変更できません。（クラスの場合は変更できました）

``` swift
// ストラクチャ
let fuelC = Fuel()
fuelC.used = 10       // エラー
// クラス
class Car {
    var type = "Unknown"
}
let car = Car()
car.type = "ワゴン"
car
```

* ストラクチャはプロパティを引数に取るイニシャライザが自動で定義される

``` swift
struct Fuel2 {
    var max: Int
    var current: Int
}
var fuel = Fuel2(max: 30, current: 25)
fuel.max
fuel.current
```


## 列挙型 enum
列挙型も使えます

```
enum 列挙型名
    case 値１
    case 値２
    case 値３
}
```
* case に値をセットします

```swift
// enum
enum Fruits {
    case Apple
    case Banana
    case Orange
}

var myFruits = Fruits.Apple
print("myFruits=\(myFruits)")        // myFruits=Apple
```

* 型がわかると、型推論で以下のように書けます

```swift
var myFruits2:Fruits = .Banana
print("myFruits2=\(myFruits2)")        // myFruits2=Banana
```

* enum を上のように定義すると、値はenum型となり、ファイルとかに出力しにくいので値に raw value を割り振る事ができます

```swift
// enum に値を割り振る場合
enum Prize:Int {
    case Gold    = 100
    case Silber  =  50
    case Bronze  =  30
}
var myPrize:Prize = .Gold
print("myPrize=\(myPrize)")                     // myPrize=Gold
print("myPrize.rawValue=\(myPrize.rawValue)")   // myPrize.rawValue=100
```

* enumにはメソッドが定義できます

```swift
enum Team: String {
    case Giants
    case Tigers
    case Carp
    
    func toJapanese() -> String {
        switch self {
        case .Giants:
            return "巨人"
        case .Tigers:
            return "阪神"
        case .Carp:
            return "広島"
        }
    }
}
var team = Team.Giants
print("My Team is \(team.toJapanese())")        // My Team is 巨人
```

* enum には引数でデータを持たす事ができます

```swift
// enum 値を格納できる(Assocated Value)
enum Player {
    case Pitcher(Int, Int)
    case Batter(Int)
    
    func seiseki() -> String {
        switch self {
        case .Pitcher(let kati, let make):
            return "\(kati)勝 \(make)敗"
        case .Batter(let honruida):
            return "\(honruida)本塁打"
        }
    }
}

var kaneda = Player.Pitcher(400, 298)
var oh = Player.Batter(868)
print("金田の成績 \(kaneda.seiseki())")        // 金田の成績 400勝 298敗
print("王の成績 \(oh.seiseki())")              // 王の成績 868本塁打
```


