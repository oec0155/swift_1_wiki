# 9.エラー処理
## エラータイプ
エラーが発生すると、エラータイプでエラーを知らせます（JavaのException）  
エラータイプは enum 型です  
エラータイプは ErrType プロトコルを継承する必要があります

```swift
// エラーは列挙形で定義する
enum MyErrorType : ErrorType {
    case Empty
    case Numeric(data: String)
    case Large
}
```

上の例では MyErrorType はデータエラーのタイプです  
このエラーは、中身が無い、数字で無い、値が大きすぎるの３種類のエラーを表せます  
数字で無いエラーの場合は何が入力されたか、その値をセットして返せるようになっています

## エラーを投げる
エラーが発生した時、エラータイプを作成して返します  
エラーを返すには throw を使います
又、エラーを返すメソッドには throws をつけます

```swift
func chkData(input:String?) throws {
    if let tmp = input {
        if let inno = Int(tmp) {
            if inno > 5000 {
                throw MyErrorType.Large
            }
        } else {
            throw MyErrorType.Numeric(data: tmp)
        }
    } else {
        throw MyErrorType.Empty
    }
}
```

## エラーの補足
エラーを返す可能性のある処理はそのエラーを補足して、適切に処理をしなければなりません  
エラーの補足にはいかのような方法があります

* do-catch 文

```
do {
    try エラーを返す可能性のあるメソッド
} catch エラーパターン１ {
    処理１
} catch エラーパターン２ {
    処理２
} catch {
    それ以外のエラー処理
}
```

```swift
do {
    try chkData("12345")
} catch MyErrorType.Empty {
    print("データを入れてください")
} catch MyErrorType.Numeric(let inputData) {
    print("\(inputData)は数字ではありません")
} catch MyErrorType.Large {
    print("大きすぎます")
}
```

上の例では "大きすぎます" のエラーが出力されます。chkData の引数を変えて見てください  

## guard 文

上の例でエラーチェック処理の中で if の入れ子がたくさん発生して複雑になっています。guard文を使うとこれを簡潔に書く事ができます

```swift
func chkData2(input:String?) throws {
    guard let tmp = input else {
        throw MyErrorType.Empty
    }
    guard let inno = Int(tmp) else {
        throw MyErrorType.Numeric(data:tmp)
    }
    guard inno <= 5000 else {
        throw MyErrorType.Large
    }
}
```

guard 文には必ず else が必要です
