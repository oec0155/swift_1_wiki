# 7.関数

## 関数の定義と呼び出し

* 実際の開発では関数を使う事はありません。クラスを作りますので、関数ではなく、メソッドとなります。
* swiftはスクリプト言語としても使えますので、その場合は関数が出てきます。（playgroundの中でも関数を使います）
* しかし、メソッドは関数と同じ規則に従いますのでここで関数の説明を行います。


* 関数は以下のように書きます

## 引数なし、戻り値無しの場合

関数の定義
```
func 関数名( ) {
   処理
}
```

関数の呼び出し
```
関数名()
```

``` swift
// 引数無し関数
func sayHello() {
    print("こんにちは");
}
sayHello()
```

## 引数有り、戻り値有りの場合
* Swift では、引数にラベル（外部引数名）が付けられます

関数の定義

```
// 引数有り関数
func 関数名(ラベル 引数名:引数の型) -> 戻り値の型 {
    処理
    return 戻り値
}
```

関数の呼び出し
```
関数名(ラベル: 値)
```


``` swift
// 引数有り関数
func sayHello2(name myname: String) {
    print("こんにちは、\(myname)さん！")
}
sayHello2(name: "太郎")
```

## ラベルを省略すると内部引数名がラベルとなります

``` swift
// ラベルを省略
func sayHello3(name: String) {
    print("こんにちは、\(name)さん！")
}
sayHello3(name: "サブロー")
```

* 複数の引数をとる事もできます

```
// 複数の引数
func 関数名(ラベル1 引数名1:型, ラベル2 引数名2:型, ... ) -> 戻り値の型 {
    処理
    return 戻り値 
}
```

``` swift
func sayHello4(first: String, second: String, third: String) {
    print("こんにちは\(first)さんと、\(second)さんと、\(third)さん！")
}
sayHello4(first:"イチロー", second:"次郎", third:"サブロー")
```

## ラベルをなくしたい時
* ラベルをなくしたい時は _ を書きます

```
関数名（_ 引数名1:型, _ 引数名2:型, ... )
```

```swift
// ラベルをなくしたい時
func sayHello5(_ first: String, _ second: String, _ third: String) {
    print("こんにちは\(first)さんと、\(second)さんと、\(third)さん！")
}
sayHello5("犬", "猿", "雉")
```


* ラベルをつけることにより、引数の意味が分かりやすくなります
* 但し、ラベルがあるからと言って、引数の順番を変える事はできません。


## 関数名が同じ関数で、引数名が異なる関数は別物として定義されます

```swift
// 関数名は同じで引数のラベルが異なる関数
func myFunc3(tanka val1:Int, suryo val2:Int) -> Int {
    return 0;
}
func myFunc3(kingaku val1:Int, suryo val2:Int) -> Int {
    return 1;
}
```

上の２つの関数は別々の関数として定義されます


## 関数の引数のデフォルト値
* 関数の引数にデフォルト値を指定できます。
* 引数の型の後ろに = を書いて、デフォルト値を指定します
* デフォルト値が指定されていると、引数が省略できます。
* 但し、最後から任意の数省略できますが、途中だけとか、頭だけの省略はできません

```
func 関数名(引数名:型 = デフォルト値) -> 戻り値の型 {
}
```

関数を呼ぶ時に引数を省略できます

```
関数名()
```

```swift
// デフォルト値
func sayHello6(name: String = "名無し") -> String {
    return "こんにちは、\(name)さん！"
}
print(sayHello6(name: "太郎") )   // こんにちは、太郎さん！
print(sayHello6() )              // こんにちは、名無しさん！
```

## 複数の戻り値、タプルの利用
* タプルを使用する事により、複数の戻り値を返す事ができます

関数定義

```
func 関数名(ラベル 引数名:型, ... ) -> (値１の名前:型, 値２の名前:型, ...) { 
    処理
    return (値1,値2, ...) 
}
```

関数を呼ぶ時

```
var (変数1,変数2, ...) = 関数名(引数1, ...) 
var タプル型変数 = 関数名(引数1, ...)
```

```swift
// 複数の戻り値 タプルの利用
func minMax(array:[Int]) -> (min: Int, max: Int){
    var currentMin = array[0]
    var currentMax = array[0]
    for value in array {
        if value < currentMin {
            currentMin = value
        } else if value > currentMax {
            currentMax = value
        }
    }
    return (currentMin, currentMax)
}
var array = [20, -5, 1122, 221]
let result = minMax(array: array )
print("最小値=\(result.min)");
print("最大値=\(result.max)");
```

## 入出力用引数
* 通常引数は値渡しで、関数の中で引数の値を変えても関数を呼んだ方の変数の中身は変わりません。しかし、これを連動させたい時があります
* 入出力兼用の引数が定義できます
    * 引数の型の前に **inout** を付けます

関数の定義

```
func 関数名(ラベル1 引数1:inout 型, ... )  {
    処理
}
```

関数を呼ぶ時は引数の変数に **&** を付けます

```
関数名(ラベル1:&変数, ...)
```

```swift
// 入出力引数
print("処理前の array の内容")
for data in array {
    print(data)
}
// ソート関数の定義
func sortArray( array: inout [Int] ){
    let size = array.count
    for i in 0..<(size - 1) {
        for j in (i + 1)..<size {
            print("i = \(i) j = \(j)")
            if array[i] > array[j] {
                let tmp = array[i]
                array[i] = array[j]
                array[j] = tmp
            }
        }
    }
}
print("関数で array のソートを実施")
sortArray(array: &array)
print("関数処理後の array の内容")
for data in array {
    print(data)
}
```

## 関数オブジェクト
* 関数を変数に入れたり、引数として渡したりできます

```
func 関数名(引数1:型, ... ) -> 戻り値の型 {
    処理
}
var 変数名 = 関数名
```

* 上の変数を使って、以下のように関数が呼べます

```
変数名(引数1, ...)
```

```swift
// 関数の代入
func hello() -> String{
    return "Hello"
}
func konnichiwa() -> String{
    return "こんにちは"
}
var say = hello
say()                // Hello
say = konnichiwa
say()                // こんにちは
```