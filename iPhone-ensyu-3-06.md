# おまけ

テキストでは、TextField で return が押された時の処理を delegate を使って行なっていますが、IBActionを使って行うこともできます。  
IBActionでのやり方をやってみます  

最初に delegate の設定をコメントアウトします

``` swift
        //inputText.delegate = self
```

## 検索フィールドより IBAction を作成する

アシスタントビューに切り替えます  
検索フィールドから ViewController.swift に以下のように IBAction を作成します

|項目       |内容                  |
|-----------|----------------------|
|Name       |tapSearchText  |
|Event      |Did End On Exit |


IBAction 内で、return キーが押された時の処理を呼びます

``` swift
    @IBAction func tapSearchText(_ sender: Any) {
        textFieldShouldReturn(inputText)
    }
```

実行して見て下さい。同じように実行できるのが確認できると思います。  
なお、この場合、キーボードを隠す処理も自動的に行われます。以下をコメントアウトして動作して見て下さい

``` swift
        // キーボードを閉じる(1)
        //textField.resignFirstResponder()
```

これは Event を Did End On Exit にする事によりキーボードが閉じるようになります。その他の場合は閉じません。













