# パーツとプログラムの関連付け

Optionキーを押しながら、ViewController.swift をクリックして アシスタントエディタ画面に切り替えます  
邪魔なら、Document Outline 画面を閉じます  

## Image View の Outlet の作成

Image View を control を押しながらクリックして、ViewController.swift の中の class の中にドラッグドロップしてください  


ポップアップに以下のように入れてください

|項目        |内容            |
|------------|----------------|
|Connection  |Outlet          |
|Name        |answerImageView |
|Type        |UIImageVIew     |

## Label の Outlet の作成

Label を control を押しながらクリックして、ViewController.swift の中の class の中にドラッグドロップしてください  

ポップアップに以下のように入れてください

|項目        |内容            |
|------------|----------------|
|Connection  |Outlet          |
|Name        |answerLabel     |
|Type        |UILabel         |

## Button の Action の作成

Button を control を押しながらクリックして、ViewController.swift の中の class の中にドラッグドロップしてください  

ポップアップに以下のように入れてください

|項目        |内容            |
|------------|----------------|
|Connection  |Action          |
|Name        |shuffleAction     |
|Type        |Any             |
|Event       |Touch Up Inside |

(AnyObject は Any に変更になりました )





