# プログラムコードの作成

プログラムを少しずつ作成しなが動かしてみます

## 1.ボタンを押すと、じゃんけんの「グー」を表示する

さき程作成した Action に以下のようにコーディングをします

``` swift
    @IBAction func shuffleAction(_ sender: Any) {
        answerLabel.text = "グー"
        answerImageView.image = UIImage(named: "gu" )
    }
```

できたらシミュレータで実行してみてください  
Image View にグーの画像が出て、Label に "グー" と出たら OK です  

しかし、よく見るとグーの画像が縦長となっています。これを修正する為に、Image View のアトリビュートに以下の設定を追加します

|項目        |内容           |
|------------|--------------|
|Content Mode|Aspect Fit    |


### Aspect Fit と Aspect Fill

この２つはどちらも Aspect比(縦横比)を保ったまま拡大縮小します。 Fit が全体をもれなく表示するようにする(長辺優先)のに対して、FIllはなるべく大きく表示するようにして、はみ出た部分を切り捨てます(短辺優先)

## 2.ボタンを押すと「グー」、「チョキ」、「パー」と切り替わる

現在の手の状態を保持する変数を定義します

``` swift
    //じゃんけん（数字）
    var answerNumber:UInt32 = 0
```

ボタンが押された時の処理を Action に記述します（現在の記述は削除します）

``` swift
    @IBAction func shuffleAction(_ sender: Any) {
        if answerNumber == 0 {
            answerLabel.text = "グー"
            answerImageView.image = UIImage(named: "gu")
        } else if answerNumber == 1 {
            answerLabel.text = "チョキ"
            answerImageView.image = UIImage(named: "choki")
        } else if answerNumber == 2 {
            answerLabel.text = "パー"
            answerImageView.image = UIImage(named: "pa")
        }
        // 次のじゃんけん
        answerNumber = answerNumber + 1        
    }
```

動作確認してみてください。最初は「グー」でボタンを押すと「チョキ」、「パー」と変わり、その後は変更しなくなります

## 3.じゃんけんの結果をランダムに表示する

今度はじゃんけんの結果をランダムに変更するようにしてみます  
その為に乱数を使用します  
0,1,2 の数値をランダムに発生させるには以下のようにします

``` swift
    answerNumber = arc4random_uniform(3)
```

Action を以下のように変更します

``` swift
    @IBAction func shuffleAction(_ sender: Any) {
        // 0,1,2の数値をランダムに算出（乱数）
        answerNumber = arc4random_uniform(3)
        if answerNumber == 0 {
            answerLabel.text = "グー"
            answerImageView.image = UIImage(named: "gu")
        } else if answerNumber == 1 {
            answerLabel.text = "チョキ"
            answerImageView.image = UIImage(named: "choki")
        } else if answerNumber == 2 {
            answerLabel.text = "パー"
            answerImageView.image = UIImage(named: "pa")
        }
    }
```

動作させてみてください。これで完成したのですが、乱数は同じ数字を発生する事もあります。従って、時々ボタンを押してもじゃんけん画像が変わらない事が時々あります。これは動作していないかのように見えます。そこで、同じ結果は出されないように変更してみます

## 4.前回とは異なるじゃんけんの結果が表示されるようにする

以下のように Action を修正します  

``` swift
    @IBAction func shuffleAction(_ sender: Any) {
    //新しいじゃんけんの結果を一時的に格納する変数を設ける
    //arc4random_uniform()の戻り値がUInt32なので明示的に型を指定する
    var newAnswerNumber:UInt32 = 0
        // 次のじゃんけん
        repeat {
            // 0,1,2 の数値をランダムに算出（乱数）
            newAnswerNumber = arc4random_uniform(3)
            // 前回と同じ結果のときは、再度、ランダムに数値を出す。
            // 異なる結果のときは、repeat を抜ける。
        } while answerNumber == newAnswerNumber
        // 新しいじゃんけんの結果を格納
        answerNumber = newAnswerNumber
        if answerNumber == 0 {
            answerLabel.text = "グー"
            answerImageView.image = UIImage(named: "gu")
        } else if answerNumber == 1 {
            answerLabel.text = "チョキ"
            answerImageView.image = UIImage(named: "choki")
        } else if answerNumber == 2 {
            answerLabel.text = "パー"
            answerImageView.image = UIImage(named: "pa")
        }
    }
```

これで実行してみてください

