# 起動画面(LaunchScreen)を設定する

起動時の画面を設定してみます 
起動時には画面中央にじゃんけんの画像を表示します 

## 起動画面用の画像データの取り込み

* Assets.xcassets を選択します  

* 「画像データ」から launchlogo.png をドラッグドロップして Assets.xcassets に落とします

## 起動画面に Image View を配置

位置は画面の中央に 200X200 のサイズで表示するようにします  

* LaunchScreen.storybopard を開きます

* ログ画像を表示する為の Image View をオブジェクトライブラリから配置します  

* AutoLayout で以下のように設定します



Pin画面

|制約         |内容                   |
|-------------|-----------------------|
|幅           |200                     |
|高さ         |200                     |

Align画面

|制約         |内容                   |
|-------------|-----------------------|
|Horizontally in Container |チェック   |
|Vertically in Container   |チェック   |

* 画像の設定

Image View のアトリビュートを以下のように設定します

|項目        |内容         |
|------------|--------------|
|Image       |launchlogo    |

## 起動画面の設定

以下の場所で起動画面の設定を行います

[MyJanken(プロジェクト)] - [Target MyJanken ] - General - App Icons and Launch Images - Lanuch Screen File 

ここを LaunchScreen にすると起動時に LaunchScreen.storyboard が出ます  


できたら実行してみてください


