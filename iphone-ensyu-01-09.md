# 応用編　加速度センサーの利用

次にボタンの代わりに端末を振ってじゃんけんをするようにしてみましょう  

## 加速度センサー

iPhoneには加速度センサーが付いています。端末には常時重力加速度がかかっているのでこれを検出する事により端末の向きを知ることができます。  

### 端末の座標

端末を縦に持ち、前面を自分の方に向けて立ててみてください。この時、以下のように座標が決められています

|座標 |端末の方向    |
|-----|-------------|
|X    |右方向        |
|Y    |上方向        |
|Z    |手前方向      |

今回はじゃんけんをするので、最初に端末を立てて持ち、手を前に出して、端末が下向きになった時にじゃんけんをします


端末を縦に持つと Y 座標のマイナス方向に重力加速度がかかります  
端末を下向きにすると Y 座標のプラス方向に重力加速度がかかります  
従って、Y座標の加速度がマイナスからプラスになった時にじゃんけんをするようにします

## 加速度センサーの利用

最初に加速度センサーを使うので、CoreMotion をインポートします

``` swift
import CoreMotion
```

現在のじゃんけんの処理を、加速度判定から起動できるように別のメソッドにします

``` swift
    @IBAction func shuffleAction(_ sender: Any) {
        execJyanken()
    }
    
    func execJyanken() {
        //新しいじゃんけんの結果を一時的に格納する変数を設ける
        //arc4random_uniform()の戻り値がUInt32なので明示的に型を指定する
        var newAnswerNumber:UInt32 = 0
        // 次のじゃんけん
        repeat {
            // 0,1,2 の数値をランダムに算出（乱数）
            newAnswerNumber = arc4random_uniform(3)
            // 前回と同じ結果のときは、再度、ランダムに数値を出す。
            // 異なる結果のときは、repeat を抜ける。
        } while answerNumber == newAnswerNumber
        // 新しいじゃんけんの結果を格納
        answerNumber = newAnswerNumber
        if answerNumber == 0 {
            answerLabel.text = "グー"
            answerImageView.image = UIImage(named: "gu")
        } else if answerNumber == 1 {
            answerLabel.text = "チョキ"
            answerImageView.image = UIImage(named: "choki")
        } else if answerNumber == 2 {
            answerLabel.text = "パー"
            answerImageView.image = UIImage(named: "pa")
            
        }

    }
```

加速度センサーの値は CMMotionManager で取得しますので、そのインスタンスを作成します

``` swift
    let cmManager = CMMotionManager()
```

端末が起動したら　viewDidLoad() メソッドが呼ばれるので、ここに実装します
端末に加速度センサーが使える状態か（装備されているか）を判定して、使える状態なら、処理を行います

``` swift
        if cmManager.isAccelerometerAvailable {
        }
```

加速度センサーの設定を行います  
データ取得インターバルは 1秒単位とします  

``` swift
            cmManager.accelerometerUpdateInterval = 1
```

加速度センサーの値取得処理は CMMotionManager の次のメソッドを呼びます

``` swift
    startAccelerometerUpdates(to queue: OperationQueue, withHandler handler: @escaping CMAccelerometerHandler)
```

引数

|引数   |型              |内容          |
|-------|----------------|--------------|
|queue  |OperationQueue  |オペレーションキュー  |
|handler |@escaping CMAccelerometerHandler|センサーの値を取得した時の処理 Handler    |

CMAccelerometerHandler は以下のように定義されています

``` swift
(CMAccelerometerData?, Error?) -> Void 
```

|引数   |型              |内容          |
|-------|----------------|--------------|
|第一引数|CMAccelerometerData? |加速度データ |
|第二引数|Error?                |エラー  |



オペレーションキューは現在実行中のキューを指定します。これは Optional型が戻りますので強制アンラップします

``` swift
    OperationQueue.current!
```

Handler の中で、加速度センサーのデータが取得された時の処理を記述します


加速度センサーの値が取れたら、端末の向きを判定し、下向きになっていたら、じゃんけんをします。一回じゃんけん処理を呼ぶと、再び端末の向きが上を向くまでじゃんけんはしません  
一回じゃんけんをしたか、まだしてないかを判定するスイッチを定義します

``` swift
    var swStart = true
```

加速度センサーデータ取得処理を以下のように作成します

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        if cmManager.isAccelerometerAvailable {
            cmManager.accelerometerUpdateInterval = 1
            cmManager.startAccelerometerUpdates(to: OperationQueue.current! , withHandler: { (acldata, error) in
                if let data = acldata {
                    let acl = data.acceleration.y
                    if self.swStart {
                        if acl > 0 {
                            self.execJyanken()
                            self.swStart = false
                        }
                    } else {
                        if acl < 0 {
                            self.swStart = true
                        }
                    }
                }
            })
            
        }
    }
```

できたら、実機で実行してみてください
