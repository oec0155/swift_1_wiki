# じゃんけんアプリを作ろう

## 1.[アプリの説明](iphone-ensyu-01-01)

## 2.[プロジェクトの作成](iphone-ensyu-01-02)

## 3.[パーツの配置と設定](iphone-ensyu-01-03)

## 4.[パーツの表示位置、幅や高さの設定](iphone-ensyu-01-04)

## 5.[パーツとプログラムの関連付け](iphone-ensyu-01-05)

## 6.[プログラムコードの作成](iphone-ensyu-01-06)

## 7.[起動画面(LaunchScreen)を設定する](iphone-ensyu-01-07)

## 8.[アイコンを設定する](iphone-ensyu-01-08)

## 9.[応用編　加速度センサーの利用](iphone-ensyu-01-09)
