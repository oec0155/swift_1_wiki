# タップで音を鳴らす

ボタンが押された時に音を鳴らす処理を作成するには、ボタンに IBAction を作成する必要があります

## IBAction の作成

シンバル、ギター、Play,Stop の各ボタンに IBAction を作成しましょう

``` swift
    @IBAction func cymbal(_ sender: Any) {

    }

    @IBAction func guitar(_ sender: Any) {

    }
    
    @IBAction func play(_ sender: Any) {

    }
    
    @IBAction func stop(_ sender: Any) {

    }
```



## 音源を配置する

「画像データ」フォルダーの中の sound フオルダーに以下の音源データも入っています。

|音源ファイル  |内容         |
|-------------|-------------|
|cymbal.mp3   |シンバルの音源 |
|guitar.mp3   |ギターの音源  |
|backmusic.mp3|バックグラウンドミュージックの音源|

sound フォルダーをプロジェクトの MyMusic フォルダーの中にドラッグドロップします  
ポップアップが出ますので、以下のように入れてください

|項目          |内容              |
|--------------|------------------|
|Destination   |Copy Items if needed にチェックを入れる |
|Added folders |Create Groups を選択する   |

Finish を押します
プロジェクトのフォルダーの中に sound グループができて、音源ファイルが入っていれば OK です

## 処理のコーディング

音源を鳴らす処理を作成します  

音源を取り扱う為には AVFoundation フレームワークを使います

### AVFoundation　処理

* 最初に、AVFoundation をimportします

``` swift
import AVFoundation
```


* AVAudioPlayer のインスタンスを作成します

``` swift
    // シンバル用のプレイヤーインスタンスを作成
    var cymbalPlayer = AVAudioPlayer()
```

* サウンドファイルを再生するには、AVAudioPlayer の play()メソッド を使います

``` swift
cymbalPlayer.play()
```

* サウンドファイルをAVAudrioPlayer インスタンスにセットするには、 AVAudioPlayer をインスタンス化する時に、ファイルURL で指定します

AVAudioPlayer の initializer

``` swift
    AVAudioPlayer.init(contentsOf url: URL, fileTypeHint utiString: String?) throws
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|url           |URL               |サウンドファイルの URL |
|utiString     |String?           |ファイルフォーマット文字列 Uniform Type Identifier(UTI)で指定(mp4,wav,aiffなど) |



``` swift
    // シンバル用のプレイヤーに、音源ファイル名を指定
    cymbalPlayer = AVAudioPlayer(contentsOf: cymbalPath, fileTypeHint: nil)
```

* サウンドファイル URL を取得するには以下のメソッドを使います

``` swift
    Bundle.main.bundleURL.appendingPathComponent(_ pathComponent: String) -> URL
```

引数

|引数         |型            |内容                  |
|-------------|--------------|---------------------|
|pathComponent|String      　|リソースファイル名      |

このメソッドでアプリにバンドルされているリソースへのファイルURLパスを作成して返してくれます  
必ず指定のリソースファイルがあるとは限らないので Optional で返ります

``` swift
    // シンバルの音源ファイル
    let cymbalPath = Bundle.main.bundleURL.appendingPathComponent("cymbal.mp3")
```

* ではシンバルがタップされた時の処理を作成してみましょう

``` swift
    // シンバルの音源ファイルを指定
    let cymbalPath = Bundle.main.bundleURL.appendingPathComponent("cymbal.mp3")
    // シンバル用のプレイヤーインスタンスを作成
    var cymbalPlayer = AVAudioPlayer()
    @IBAction func cymbal(_ sender: Any) {
        // シンバル用のプレイヤーに、音源ファイル名を指定
        cymbalPlayer = try AVAudioPlayer(contentsOf: cymbalPath, fileTypeHint: nil)
        // シンバルの音楽再生
        cymbalPlayer.play()
    }
```

しかし、エラーが出ます。サウンドファイルがなかったりして、エラーが発生する可能性があるので、例外処理が必要です。  
Swift の例外処理は以下のように書きます

``` swift
    do {
           try 例外が発生する処理
    } catch let 変数 {
           例外処理
    }
```

エラーを受け取らない場合は、let 変数は省略できます  


例外処理を組み込んでみます

``` swift
    @IBAction func cymbal(_ sender: Any) {
        do {
            // シンバル用のプレイヤーに、音源ファイル名を指定
            cymbalPlayer = try AVAudioPlayer(contentsOf: cymbalPath, fileTypeHint: nil)
            // シンバルの音楽再生
            cymbalPlayer.play()
        } catch let err {
            print("シンバルで、エラーが発生しました！ msg=\(err.localizedDescription)")
        }
    }
```

* できたらシミュレータで実行してみてください  

* わざとファイル名を変えて、エラーを発生させてみてください

### ギターの処理

ギターも全く同じようにしてできます。自分でコーディングしてみてください

### Play、Stop　ボタンの処理

Play ボタンはバックグラウンドミュージックを鳴らします。Stop ボタンはバックグラウンドミュージックを止めます  
Play ボタンの処理は、シンバルやギターと同じようにできますが、バックグラウンドミュージックの場合、一旦開始すると、Stop ボタンが押されるまでずっと鳴らし続けます。それには、以下のようにします  

``` swift
    //　バックミュージックの音楽再生
    backmusicPlayer.numberOfLoops = -1
    backmusicPlayer.play()
```

ループ回数に -1 をセットすると、永久にループします

Stop ボタンの処理は、バックグラウンドミュージックの AVAudioPlayerインスタンスの以下のメソッドを呼びます

``` swift
    // バックミュージック停止
    backmusicPlayer.stop()
```

ではこちらも同じように自分で実装してみてください


* できたら実行してみてください