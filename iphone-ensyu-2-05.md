# カスタマイズ編　リファクタリングでコードをスッキリ

リファクタリングとは、プログラムの機能や動作を変えずに、プログラムを見やすくしたり、コードの重複を省く事です。 

今回のプログラムも、今のままでも一応動いてはいるのですが、プログラムコードが見にくくなっていますので,以下の点を修正します

* サウンドを再生する処理が各サウンド毎に作成されているので、メソッドを作成して、それを呼ぶように修正する

## サウンドを再生する処理

以下のようなメソッドを作成して呼ぶようにしましょう

``` swift
    fileprivate func soundPlayer(_ player:inout AVAudioPlayer, path: URL, count: Int) {
        do {
            player = try AVAudioPlayer(contentsOf: path, fileTypeHint: nil)
            player.numberOfLoops = count
            player.play()
        } catch  let err  {
            print("エラーが発生しました msg=\(err.localizedDescription)")
        }
    }
```



ここで、第１引数に inout が付いています、これはこの引数が参照渡しであるという事です。通常の引数は、値渡しで、関数の中で変更する事はできません。今回は、player をインスタンス化する処理もこの関数内で行いますので、参照渡しとします。  
なお、参照渡しの引数は関数を呼ぶ時に、以下のように、引数の前に & を付ける必要があります

``` swift
    soundPlayer(&cymbalPlayer, path: cymbalPath, count: 0)
```

この関数には fileprivate というスコープが設定されています。詳しい事は省略しますが、ほぼ private と同じと考えて下さい  

では、この関数を呼ぶように修正して見て下さい


