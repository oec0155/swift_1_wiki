# 処理のコーディング

検索フィールドに文字を入れて、完了したら（returnが押されたら）処理を実行します  
入力が完了したら実行される処理を、delegate で作成します  
delegate というのはオブジェクトで何らかのイベントが発生した時に呼ばれる処理の事です  
今回は、検索フィールドで returnキーが押された時に呼ばれる delegate の中に処理を作成します  

## 検索フィールドへの delegate の設定

最初に ViewController に UITextFieldDelegate プロトコルを定義します

``` swift
class ViewController: UIViewController,UITextFieldDelegate {
```

このプロトコルで定義されているメソッドは以下です

``` swift
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|textField     |UITextField       |return キーが押された UITextField のインスタンス |


UITextField の delegate プロパティをセットします  
アプリが起動した直後の処理、ViewDidLoad()メソッドでセットします

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Text Field の delegate通知先を設定
        inputText.delegate = self
    }
```

returnキーが押されたら呼ばれるメソッドを実装します

``` swift
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // 検索フィールドで return キーが押された時にする処理
    }
```

## 検索フィールドで return キーが押された時に実行する処理

### 仮想キーボードの消去

iOSの場合、キーボードがありません、下から仮想キーボードが出てきます。これは return キーを押しても自動的には消えません。このまま残ると、せっかく地図を表示しても見えません。これを消す処理が必要です。消すには以下のようにします

``` swift
        // キーボードを閉じる(1)
        textField.resignFirstResponder()
```


### 検索フィールドに入力された文字をデバッグエリアへ表示する

次に、とりあえずは、入力された文字をデバッグエリアへ表示して見ましょう


``` swift
        // 入力された文字を取り出す(2)
        let searchKeyword = textField.text
        // 入力された文字をデバッグエリアに表示(3)
        print(searchKeyword)
        // デフォルト動作を行うのでtrueを返す(4)
        return true
```

最後に true を返すと、この後の処理も引き続き継続されます。false を返すと、これの後の処理はキャンセルされます


ここまでで一旦実行して見ましょう


### キーワードから緯度軽度を取得する

キーワードから緯度軽度を取得するには、コアロケーションフレームワークの CLGeocoder クラスにある以下のメソッドを使用します

``` swift
geocodeAddressString(_ addressString: String, completionHandler: @escaping CLGeocodeCompletionHandler)
```

引数

|引数            |型                |内容               |
|----------------|------------------|-------------------|
|addressString   |String            |検索する住所文字列　 |
|completionHandler|@escaping CLGeocodeCompletionHandler   |緯度軽度が取得された時に実行される処理 |

* CLGeocodeCompletionHandler

引数

|引数           |型               |内容                |
|---------------|-----------------|-------------------|
|第一引数        |[CLPlacemark]?   |該当の位置情報の配列、nilもありうる |
|第二引数        |Error?           |取得エラー時のエラー情報  |

今回は、クロージャーを使ってこの処理ブロックを構築します  
複数の位置情報が返りますが、最初のデータのみ使います 

CLPlacemark から緯度軽度は以下のようにして取得します

``` swift
    clplacemark.location?.coordinate
```

|プロパティ　　　|型          |内容             |
|---------------|-----------|-----------------|
|location       |CLLocation |ロケーションオブジェクト(緯度経度も含む)|
|coordinate     |CLLocationCoordinate2D|緯度経度   |

 
緯度軽度をデバッグエリアに表示して見ましょう


``` swift
        // CLGeocoderインスタンスを取得(5)
        let geocoder = CLGeocoder()
        // 入力された文字から位置情報を取得(6)
        geocoder.geocodeAddressString(searchKeyword!) { (placemarks:[CLPlacemark]?, error:Error?) in
            // 位置情報が存在する場合１件目の位置情報をplacemarkに取り出す(7)
            if let placemark = placemarks?[0] {
                // 位置情報から緯度経度が存在する場合、緯度軽度をtargetCoordinateに取り出す(8)
                if let targetCoordinate = placemark.location?.coordinate {
                    // 緯度軽度をデバッグエリアに表示(9)
                    print(targetCoordinate)
                }
            }
        }
```

### 緯度軽度の位置にピンを立てる

検索文字列から取得した緯度軽度の位置にピンを立て、そこから半径 500m の地図を表示して見ましょう

まず、ピンは MKPointAnnotation オブジェクトであらわしますので、これをインスタンス化します

``` swift
        // MKPointAnnotationインスタンスを取得し、ピンを生成(10)
        let pin = MKPointAnnotation()
```

次にこれに緯度経度情報をセットします

``` swift
        // ピンの置く場所に緯度軽度を設定(11)
        pin.coordinate = targetCoordinate
```

ピンのタイトルに検索文字列をセットします

``` swift
        // ピンのタイトルを設定(12)
        pin.title = searchKeyword
```

ピンを地図上に描画します

``` swift
        // ピンを地図に置く(13)
        self.dispMap.addAnnotation(pin)
```

このクロージャーの中は別スレッドになりますので、外の変数にアクセスするには self が必要です  

最後にピンの位置から半径 500m の地図を表示します。これにはマップキットフレームワークの以下のメソッドを利用します

``` swift
MKCoordinateRegionMakeWithDistance(_ centerCoordinate: CLLocationCoordinate2D, _ latitudinalMeters: CLLocationDistance, _ longitudinalMeters: COLocationDistance) -> MKCoordinateRegion
```

引数

|引数            |型                |内容               |
|----------------|------------------|-------------------|
|centerCoordinate|CLLocationCoordinate2D|画面中央に表示する緯度経度|
|latitudinalMeters|CLLocationDistance    |緯度方向の距離（単位はm）|
|longitudinalMeters|CLLocationDistance    |緯度方向の距離（単位はm）|

戻り値は MKCoordinateRegion (領域)です。これをマップにセットします

``` swift
    // 緯度軽度を中心にして半径500mの範囲を表示(14)
    self.dispMap.region = MKCoordinateRegionMakeWithDistance(targetCoordinate, 500.0, 500.0)
```

できたら実行して見てくださ
