# カスタマイズ編　マップの表示方法を切り替えてみよう

マップは航空写真や３D画像に切り替えて表示する事ができます  

|マップタイプ   |内容                   |
|--------------|-----------------------|
|.standard     |標準の地図   |
|.satelite     |航空写真   |
|.hybrid       |地図と航空写真の合成  |
|.sateliteFlyover|航空写真の３D表示  |
|.hybridFlyover |地図と航空写真の３D表示の合成 |





ボタンを追加して、表示を切り替えられるようにして見ましょう

## ボタンの追加

Main.storyboard にして、Button を画面右下に配置して下さい

## ボタンに制約の設定

ボタンに以下の制約を設定します  
ボタンをクリックして、pin画面を出して以下の制約を設定して下さい

|制約      |内容                   |
|----------|-----------------------|
|距離      |右:0,下:8 (マージンを含む) |
|幅        |46     |
|高さ      |30     |

## ボタンのアトリビュートの設定

ボタンをクリックして、アトリビューとインスペクタを出します  
以下の値をセットして下さい

|アトリビュート  |値                   |
|---------------|---------------------|
|Titleの下の欄   |切替                  |
|Background     |White Color          |

## ボタンにアクションの設定

optionキーを押しながら ViewController.swift をクリックしてアシスタントビューに切り替えます  
ボタンからプログラムソース上にIBActionを作成して下さい

|項目         |内容              |
|-------------|-----------------|
|Name         |changeMapButtonAction |

ボタンが１回押されるたびに mapType を切り替えます

``` swift
    @IBAction func changeMapButtonAction(_ sender: Any) {
        // mapTypeプロパティーをトグル
        // 標準(.standard) → 航空写真(.satelite) → 航空写真+標準(.hybrid)
        // → 3D Flyover(.sateliteFlyover) → 3D Flyover+標準(.hybridFlyver)
        if dispMap.mapType == .standard {
            dispMap.mapType = .satellite
        } else if dispMap.mapType == .satellite {
            dispMap.mapType = .hybrid
        } else if dispMap.mapType == .hybrid {
            dispMap.mapType = .satelliteFlyover
        } else if dispMap.mapType == .satelliteFlyover {
            dispMap.mapType = .hybridFlyover
        } else {
            dispMap.mapType = .standard
        }
    }
```

できたら実行して見て下さい


