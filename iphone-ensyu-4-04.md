# タイマー画面処理の作成

## IBOutlet,IBAction の作成

Main.storyboard でタイマー画面を中央に出します  
option を押しながら、ViewController.swift をクリックしてアシスタントビューに切り替えます

* 時間表示ラベルの IBOutlet の作成

時間表示ラベルから ViewController.swift に IBOutlet を作成します

|項目     |内容     |
|---------|---------|
|Name     |countDownLabel |

* 秒数設定ボタンの IBAction の作成

秒数設定ボタンから ViewController.swift に IBAction を作成します

|項目     |内容     |
|---------|---------|
|Name     |settingButtonAction |

* スタートボタンの IBAction の作成

スタートボタンから ViewController.swift に IBAction を作成します

|項目     |内容     |
|---------|---------|
|Name     |startButtonAction |

* ストップボタンの IBAction の作成

ストップボタンから ViewController.swift に IBAction を作成します

|項目     |内容     |
|---------|---------|
|Name     |stopButtonAction |


## タイマー処理

前回作成したタイマーアプリと同じようにします

[タイマーアプリの処理](recipe02-07)

### 変数の定義


``` swift
    // タイマーの変数を作成
    var timer: Timer?
    // 残り時間の変数を作成
    var remainCount = 0
    // 設定値を扱うキーを設定
    var settingKey = "timer_value"
```

* テキストでは経過時間(count)と残り時間(remainCount)を使っていますが、わかりにくいので残り時間(remainCount)だけで行きます
* このアプリでは、タイマーの時間を設定画面で設定できるようになっています。その設定値をアプリ内で保持する為に、UserDefaultという仕組みを使います。これはちょっとしたデータを簡単にアプリ内で保存しておく事ができます

UserDefaults を取得するには以下のようにします

``` swift
    UserDefaults.standard
```

プロパティを取得する事で、UserDefaults のインスタンスが取得できます


### 初期処理

最初に、UserDefaults に初期値を設定します
以下のメソッドで初期値を登録します

``` swift
    register(defaults registrationDictionary: [String : Any])
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|registrationDictionary | [String : Any] |初期値を辞書形式で指定する |


では初期値を設定します

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // UserDefaultsのインスタンスを生成
        let settings = UserDefaults.standard
        // UserDefaults にデフォルト値を設定する
        settings.register(defaults: [settingKey : 10])
    }
```

## UserDefaults から残り時間を取って来る処理

UserDefaults はキーと値でデータを保持しているので、キーを指定してデータが取り出せます。しかし、その値の型により、取得するメソッドが変わってきます

``` swift
    object(forKey: String)
    uri(forKey: String)
    array(forKey: String)
    dictionary(forKey: String)
    string(forKey: String)
    stringArray(forKey: String)
    data(forKey: String)
    bool(forKey: String)
    integer(forKey: String)
    float(forKey: String)
    double(forKey: String)
```

今回は数字なので、integer(forKey:) を使います



remainCountの値を最初にUserDefaults の値で初期設定するメソッドを作成します

``` swift
    // 残り時間の初期化
    func initRemainCount() {
        // UserDefaultsのインスタンスを生成
        let settings = UserDefaults.standard
        // 秒数を取得し、remainCountにセットする
        remainCount = settings.integer(forKey: settingKey)
    }
```

### 残り時間の表示処理

残り時間をラベルに表示するメソッドを作成します

``` swift
    // 画面の更新をする
    func displayUpdate() {
        // remainCount（残り時間）をラベルに表示
        countDownLabel.text = "残り\(remainCount)秒"
    }
```

### 画面描画時処理の作成

今作成した、残り時間の初期化と画面への表示処理を組み込むのですが、 ViewDidLoad に書くと、最初に１回だけしか、実行されません。今回は、設定画面があり、こちらで設定を変更された時も、この処理は実行したいので、 ViewDidAppear メソッドに書きます

``` swift
    // 画面切り替えのタイミングで処理を行う
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // remainCount（残り時間）を初期化する
        initRemainCount()
        // タイマーの表示を更新する
        displayUpdate()
    }
```

ここまでで一旦実行してみましょう。残り10秒とラベルに出れば成功です

### スタートボタン処理

スタートボタンが押された時の処理を作成します  
既に作成している @IBAction に処理を作成します  
Timerクラスの使い方は以前にタイマーアプリを作成した時と同じです[タイマーアプリの処理](recipe02-07)  
既に一度スタートボタンを押されて、タイマーがスタートしてたら、処理は行いません  



``` swift
    @IBAction func startButtonAction(_ sender: Any) {
        // timerをアンラップしてnowTimerに代入
        if let nowTimer = timer {
            // もしタイマーが、実行中だったらスタートしない
            if nowTimer.isValid == true {
                // 何も処理しない
                return
            }
        }
        // タイマーをスタート
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerInterrupt(_:)), userInfo: nil, repeats: true)
        
    }
```     

ここでは、1秒ごとに、timerInterrupt(_:)メソッドが呼ばれるようになっています  
次にこの処理を作成します

### Timer から1秒ごとに呼ばれる処理

timerInterrupt(_:)メソッドでは以下の処理を行います

* 残り時間(remainCount) を 1 減らす
* 画面表示を更新する
* 残り時間(remainCount)が 0 の時は、timer を停止し、残り時間を初期値に戻す

``` swift
    // 経過時間の処理
    func timerInterrupt(_ timer:Timer) {
        // remainCount（残り時間）を-1する
        remainCount -= 1
        // 残り時間の表示
        displayUpdate()
        // remainCount（残り時間）が0以下の時、タイマーを止める
        if remainCount <= 0 {
            // 初期化の処理
            initRemainCount()
            // タイマー停止
            timer.invalidate()
        }
    }
```

### ストップボタン処理

ストップボタンを押すと、タイマーの進行が一時停止します。スタートボタンを押すと、再びスタートします  

* タイマーを停止する

``` swift
    @IBAction func stopButtonAction(_ sender: Any) {
        // timerをアンラップしてnowTimerに代入
        if let nowTimer = timer {
            // もしタイマーが、実行中だったら停止
            if nowTimer.isValid == true {
                // タイマー停止
                nowTimer.invalidate()
            }
        }
    }
```

ここまでで実行してみてください

### 秒数設定ボタン処理の作成

タイマーが実行中の場合、停止してから、秒数設定画面へ遷移させます

#### プログラムでの画面遷移

ViewController のプログラムで画面遷移をさせる事ができます。ViewController からセグエを引き、セグエの ID を設定して置き、それを使って遷移をさせます  
以下のメソッドを使います

``` swift
    performSegue(withIdentifier identifier: String, sender: Any?)
```

引数

|引数名        |型           |セットする内容            |
|-------------|-------------|--------------------------|
|identifier   | String     |遷移するセグエのIdentifier   |
|sender       | Any?       |セグエ処理にデータを渡したい時にセットする |



では遷移処理を組み込んでみます。タイマー実行中だとタイマーを停止します



``` swift
    @IBAction func settingButtonAction(_ sender: Any) {
        // timerをアンラップしてnowTimerに代入
        if let nowTimer = timer {
            // もしタイマーが、実行中だったらタイマー停止
            if nowTimer.isValid == true {
                // タイマー停止
                nowTimer.invalidate()
            }
        }
        // 画面遷移を行う
        performSegue(withIdentifier: "goSetting", sender: nil)
    }
```

