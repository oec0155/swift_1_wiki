# 設定画面処理の作成

## IBOutlet,IBAction の作成

PickerView に IBOutlet,決定ボタンに IBAction を設定します

* PickerView に IBOutlet を設定

以下のように設定します

|項目    |内容         |
|--------|-------------|
|Name    |timerSettingPicker |

* 決定ボタンに IBAction を設定


以下のように設定します

|項目    |内容         |
|--------|-------------|
|Name    |decisionButtonAction |


## UIPickerView のプロトコル

設定画面では UIPickerView を使います。これは UITableView と同じようにプロトコルを持っています

UIPickerView では以下のプロトコルが準備されています

|プロトコル             |内容      |
|-----------------------|----------|
|UIPickerViewDataSource |データの表示に関するメソッドを定義  |
|UIPickerViewDelegate   |PickerView 上での操作に関するメソッドを定義 |

各プロトコルで定義されているメソッド



* UIPickerViewDataSource プロトコル

いかのメソッドが準備されています

``` swift
func numberOfComponents(in pickerView: UIPickerView) -> Int
func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? 
```








