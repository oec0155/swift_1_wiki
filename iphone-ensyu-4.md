# タイマーアプリ（iphone-ensyu-4）
## 1.[タイマーアプリの説明](iphone-ensyu-4-01)
## 2.[プロジェクトの作成](iphone-ensyu-4-02)
## 3.[画面の作成](iphone-ensyu-4-03)
## 4.[タイマー画面処理の作成](iphone-ensyu-4-04)
## 5.[設定画面処理の作成](iphone-ensyu-4-05)
## 6.[カスタマイズ](iPhone-ensyu-4-06)
## 7.[おまけ](iPhone-ensyu-4-07)