# CustomView の作り方

CustomView の作り方と、画面の切り替え、プログラムからのレイアウトの変更をやってみます

## プロジェクトの作成

* 新規プロジェクトを作成します
    * テンプレートは Single View Application で作成します

 | 項目                    |内容                        |
 |------------------------|----------------------------|
 |Product Name            |CustomView                     |
 |Organization Name       |oecxxxx(ユーザ名)            |
 |Organization Identifier | jp.co.oec_o.oecxxxx        |
 |Language                | Swift                      |
 |Devices                 | Universal                  |

* 上記の内容で新規プロジェクトを作成してください  

* 今回はiPad の作成なので、Main.storyboard を開いて、端末サイズを iPad に変更してください

## ビューの配置

### 画面上には以下のビューを配置します

|部品        |機能            |
|------------|---------------|
|Button      |ボタン１ |
|Button      |ボタン２ |
|Button      |ボタン３ |
|View        |左側ビュー  |
|View        |右側ビュー  |

### レイアウト

* 最初にボタンを縦に３つ配置します
* ３つのボタンを選択して、Stack に入れます
* StackView のアトリビュートを以下のように設定します

|項目     |内容             |
|---------|-----------------|
|Distribution | Fill Equally  |

* StackView の制約を Pin画面で以下のように設定します

|項目     |内容             |
|---------|-----------------|
|距離制約  | 上:0,左:0,下:0  |
|幅        |200        |

* StackView の右に View を２つ追加します

* 左側ビューに以下の制約を追加します

|項目     |内容             |
|---------|-----------------|
|距離制約  | 上:0,左:0,右:0,下:0  |

* 右側ビューに以下の制約を追加します

|項目     |内容             |
|---------|-----------------|
|距離制約  | 上:0,右:0,下:0  |
|幅        |200        |






* Button のアトリビュートを設定します

|部品    |項目      |内容           |
|--------|----------|--------------|
|ボタン１ |title     |ボタン１       |
|ボタン２ |title     |ボタン２       |
|ボタン３ |title     |ボタン３       |



## カスタムビューの作成

### カスタムビューの xib ファイルの作成

* Xcode のメニューから File -> New -> File を選択  
* User Interfaceの View を選択して Next を押す

|項目      |内容          |
|----------|-------------|
|Save As   |FirstView   |

上の様に入れたら  Create を押します

* Attribute の Size を FreeForm に変えてサイズを調整します

### カスタムViewクラスの作成

* 次にこレイアウト用の View を作成します

* Xcode のメニューから File -> New -> File を選択  
* テンプレートは iOS の Cocoa Touch Class を選択して Next を押す
* 次の画面で以下の様に入れる

|項目         |内容         |
|-------------|---------------|
|Class        |FirstView |
|Subclass of  |UIView  |
|Language     |Swift     |

上の様に入れたら Next を押す

* そのまま Create を押します

### 紐付け

上記の xibファイルとクラスを紐付けます

xibファイルの File's Owner を選択して、アイデンティティインスペクターを開いて以下のように入れます

|項目         |内容         |
|-------------|---------------|
|Class        |FirstView |


* 後の残り２つも同じようにして作成してください

* それぞれのレイアウトの中身を適当に作成してください

## ViewController の修正

* ViewController の各ボタンに IBAction を作成してください
* 2つのView の IBOutletを作成してください
* 右画面の幅制約の IBOutlet を作成してください

``` swift
class ViewController: UIViewController {

    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var rightViewWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func button1Tapped(_ sender: Any) {
    }

    @IBAction func button2Tapped(_ sender: Any) {
    }

    @IBAction func button3Tapped(_ sender: Any) {
    }
}
```

* FirstView.swift は以下のようにコーディングしてください

``` swift
import UIKit

@IBDesignable
class FirstView: UIView {

    let xibName = "FirstView"
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame : CGRect ) {
        super.init(frame: frame)
        loadFromNib(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib(frame: self.bounds)
    }
    
    private func loadFromNib(frame: CGRect) {
        let view = Bundle(for: type(of:self)).loadNibNamed(xibName, owner: self, options: nil)?.first as! UIView
        view.frame = frame
        addSubview( view )
        self.autoresizingMask = [ .flexibleWidth, .flexibleHeight]
    }

}
```

* その他の View も同じようにコーディングしてください


* ViewControlle に以下のようにコーディングしてください

``` swift
class ViewController: UIViewController {

    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var rightViewWidth: NSLayoutConstraint!
    
    var firstView:FirstView?
    var secondView:SecondView?
    var thirdView:ThirdView?
    var swRight = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rightViewWidth = 0
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func button1Tapped(_ sender: Any) {
        removeSubView()
        firstView = FirstView(frame: leftView.bounds)
        leftView.addSubview( firstView! )
    }

    @IBAction func button2Tapped(_ sender: Any) {
        removeSubView()
        secondView = SecondView(frame: leftView.bounds)
        leftView.addSubview( secondView! )
    }

    @IBAction func button3Tapped(_ sender: Any) {
        if swRight {
            if let view = thirdView {
                view.removeFromSuperview()
            }
            rightViewWidth.constant = 0
            rightView.layoutSubviews()
            swRight = false
        } else {
            rightViewWidth.constant = 300
            self.rightView.layoutSubviews()
            thirdView = ThirdView(frame: rightView.bounds)
            rightView.addSubview(thirdView!)
            leftView.layoutSubviews()
            swRight = true
        }
    }
    private func removeSubView () {
        if let view = firstView {
            view.removeFromSuperview()
            firstView = nil
        }
        if let view = secondView {
            view.removeFromSuperview()
            secondView = nil
        }
    }
}
```
