# 図鑑アプリのテーブルを自由にレイアウト

図鑑アプリではテーブルセルのレイアウトは事前に準備されているラベルを使用しました。しかし、自分で自由に View を貼り付けてレイアウトする事もできます

## 以下のようなレイアウトとする

|内容          オブジェクト  |位置              |
|-------------|-------------|--------------|
|行番号        |UILabel     |左端、幅は 40  |
|動物名        |UILabel     |行番号の右隣   |

## レイアウトの作成

Main.storyboard を開いて、UITableCell に　UILabelを２つ配置して以下のように制約を設定します

|内容          |制約                |
|--------------|----------------------|
|行番号         |垂直方向の中央、距離制約=(上:0　左:0　下:0)、幅=40  |
|動物名         |垂直方向の中央、距離制約=(上:0　左:0　右:0　下:0)    |


## カスタムセルクラスの作成

UITableViewCell クラスを継承した MyTableViewCell クラスを作成します  
* File -> New -> File を選択  
* Cocoa Touch Class を選択
* Class名を以下のように設定

|項目     |内容              |
|---------|------------------|
|Class    |MyTableViewCell   |
|Subclass of | UITableViewCell |
|Language | Swift            |

## カスタムセルクラスの設定

Main.storyboard 上のセルを今作成したクラスに変更します  

* Main.storyboard を開き、NameCellを選択する
* アイデンティティインスペクタを開き、Class に MyTableViewCell を設定する

## カスタムセルクラスのアウトレットの作成

今作成した MyTableViewCell にセル上のラベルのアウトレットを作成します

* MyTableViewCell.swift を Optionキーを押しながらクリックし、アシスタントエディタ画面にします
* 2つのラベルからアウトレットを作成します

|オブジェクト   |Name     |
|--------------|---------|
|行番号         |gyoNo   |
|動物名         |animalName |

``` swift
    @IBOutlet weak var gyoNo: UILabel!
    @IBOutlet weak var animalName: UILabel!
```


## カスタムセルへのデータのセット

ViewController.swift のデータをセットする処理を以下のように変更します

``` swift
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as! MyTableViewCell
        let item = items[indexPath.row]
        cell.gyoNo.text = "\(indexPath.row + 1)"
        cell.animalName.text = item.name
        return cell
    }
```

できたら実行してみてください











