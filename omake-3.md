# 写真ビューアプリの Cell 上の View へのアクセス方法変更

セル上のビューにアクセスするのに、今回はタグを使いましたが、カスタムセルを作成する方法もあります  

## カスタムセルを利用したアプリの作成

* アプリ名は PhotoViewer2 としてください
* UICollectionViewCell に UIImageView を入れるところまでは同じです。この後、タグ番号をつけるところから、以下のようにします
* 新規でクラスを作成します。
    * File -> New -> File を選択
    * CocoaTouchClass を選択
    * 以下のように入れる

    |項目         |内容              |
    |-------------|------------------|
    |Class        |MyCollectionViewCell |
    |Subclass of  |UICollectionViewCell  |
    |Language     |Swift                 |

* セルを MyCollectionViewCell にします
    * ImageCell を選択して、アイデンティティインスペクタで Class を MyCollectionViewCell にする

* ImageView のアウトレットを作成する
    * アシスタントエディタの右側を MyCollectionViewCell にします
    * ImageView から MyCollectionViewCell にアウトレットを作成します

    |項目         |内容              |
    |-------------|------------------|
    |Connection   | Outlet           |
    |Name         |imageView         |
    |Type         |UIImageView       |

* イメージデータを取得するところは今までと同じです
* イメージデータをセルのImageView にセットするところは以下のようになります

``` swift
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! MySCollectionViewCell
        let asset = photos[indexPath.item]
        let width = collectionView.bounds.size.width / 3
        manager.requestImage(
            for: asset,
            targetSize: CGSize(width: width, height: width),
            contentMode: .aspectFill,
            options: nil,
            resultHandler: { result, info in
                if let image = result {
                    cell.imageView!.image = image
                }
        })
        return cell
    }
```


## コレクションビューのアトリビュートを変更してみる

* アトリビュートの以下を設定しみてください

|項目         |内容              |
|-------------|------------------|
|Scroll Direction|Horizontal     |
|Paging Enabled  | チェック       |
 
