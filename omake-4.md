# PopOver 遷移

iPad で使われる PopOver 遷移を作成してみます

PopOver 遷移するには、Segue接続で行うのが簡単です。

## プロジェクトの作成

* 新規プロジェクトを作成します
    * テンプレートは Single View Application で作成します

 | 項目                    |内容                        |
 |------------------------|----------------------------|
 |Product Name            |PopOver                     |
 |Organization Name       |oecxxxx(ユーザ名)            |
 |Organization Identifier | jp.co.oec_o.oecxxxx        |
 |Language                | Swift                      |
 |Devices                 | Universal                  |

* 上記の内容で新規プロジェクトを作成してください  

* 今回はiPad の作成なので、Main.storyboard を開いて、端末サイズを iPad に変更してください

## ビューの配置

### 画面上には以下のビューを配置します

|部品        |機能            |
|------------|---------------|
|TextField    |データ入力用     |
|Label       |戻りデータ表示用     |
|Button      |画面遷移ボタン |


### レイアウト

* Align 画面で以下の制約を追加します
   
|部品       |制約                  |
|------------|---------------------|
|テキストフィールド  |水平方向位置: 0, 垂直方向位置: -100  |
|ラベル            |水平方向位置: 0, 垂直方向位置: 0|
|ボタン            |水平方向位置: 0, 垂直方向位置: 100 |

* Pin画面で横幅の制約を追加します

|部品       |制約                  |
|------------|---------------------|
|テキストフィールド  |幅: 200  |
|ラベル            |幅: 200  |
|ボタン            |幅: 100  |

### アトリビュートの設定

以下のアトリビュートを設定します

|部品       |制約                  |
|------------|---------------------|
|View        |背景色:yellow     |
|ボタン       |文字色:白,背景色:blue,title:実行１ |


## 遷移先画面の作成

Main.storyboard を開いてください  
ViewController を画面に追加してください  

### 画面サイズの調整

遷移先画面を選択して、サイズインスペクターを開いてください

以下のように設定します

|項目      |内容            |
|----------|----------------|
|Simulated Size| Freeform   |
|width      | 300 |
|height     | 400 |

## ビューの配置

### 画面上には以下のビューを配置します

|部品        |機能            |
|------------|---------------|
|Label       |受取データ表示用     |
|TextField    |データ入力用     |
|Button      |戻りボタン |


### レイアウト

* Align 画面で以下の制約を追加します
   
|部品       |制約                  |
|------------|---------------------|
|ラベル            |水平方向位置: 0, 垂直方向位置: -100|
|テキストフィールド  |水平方向位置: 0, 垂直方向位置: 0  |
|ボタン            |水平方向位置: 0, 垂直方向位置: 100 |

* Pin画面で横幅の制約を追加します

|部品       |制約                  |
|------------|---------------------|
|テキストフィールド  |幅: 200  |
|ラベル            |幅: 200  |
|ボタン            |幅: 100  |

### アトリビュートの設定

以下のアトリビュートを設定します

|部品       |制約                  |
|------------|---------------------|
|View        |背景色:green     |
|ボタン       |文字色:白,背景色:blue,title:戻る |


### 遷移先画面用の ViewController の作成 

まず新しい ViewController を作成します

* Xcode のメニューから File -> New -> File を選択  
* テンプレートは iOS の Cocoa Touch Class を選択して Next を押す
* 次の画面で以下の様に入れる

|項目         |内容         |
|-------------|---------------|
|Class        |pop1ViewController |
|Subclass of  |UIViewController  |
|Language     |Swift     |

上の様に入れたら Next を押す

* そのまま Create を押します

以上でプロジェクトナビゲータに新しく、pop1ViewController.swift が出来ていると思います  

次に第二画面と今作成した pop1ViewController.swift を接続します  
Main.storyboard を開いてください

* 第二画面を選択して、上部にある左端の黄色いアイコン（View Controller）を選択する
* アイデンティティインスペクタを選択する
* Custom Class の Class 欄の右のセレクトボタンを押すと、候補が出ます。この中から、先程作成した、pop1ViweController を選択してください




## セグエの作成

### ポップオーバーセグエ
* 第一画面のボタンより、第二画面へセグエ接続をします  
* セグエは Present As Popover を選択します
* セグエに名前をつけてください "pop1Segue" とします

### UnWind セグエ

* ViewController に戻り先のメソッドを作成します

``` swift
    @IBAction func returnFromPop1( segue: UIStoryboardSegue) {
    }
```

* 第二画面の戻るボタンより、Exit にセグエを引き、今作成したメソッドを選択してください

これで PopOver 遷移ができるようになりました。動作させてみてくださ


## PopOver画面の調整

* これで標準のポップオーバー画面ができました。標準では以下のようになっています  

* アンカーボタンを指し示す吹き出しの形をしている
* 位置はアンカーボタンの左端を起点としている  

このように、ポップオーバー画面は吹き出しのヘルプ用のような形になっています。このまま使う場合は良いのですが、画面中央に吹き出しの形ではなく、ダイアログのように出す方法は以下のようにします


``` swift
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pop1Segue" {
            let pop1ViewController = segue.destination as! Pop1ViewController
            if let pop1PresentationController = pop1ViewController.popoverPresentationController {
                pop1PresentationController.sourceView = self.view
                pop1PresentationController.sourceRect = self.view.frame
                pop1PresentationController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }
        }
    }
```

動作確認してみてください

できたら、値の受け渡しのコーディングを自分で追加してみてください



