# 計算を行うコードの記述

## 電卓の操作の流れ

電卓の操作の流れは以下の様になっています  

1. １つ目の数値を入力する
2. 演算の種類を入力する
3. ２つ目の数値を入力する
4. = ボタンをタップして結果を表示する

## プロパティの定義

= ボタンをタップするまでに、1,2,3,の操作を記憶する必要があります。これらを保存するプロパティを準備します

``` swift
var firstValue = 0
var secondValue = 0
var currentOperator
```

ここで、firstValue,secondValue は整数型ですが、演算子は ÷,×,-,+ の４つしかないので、列挙型(enum)としますので、列挙型を定義します  


``` swift
enum Operator {
    case undefined            // 未定義
    case addition             // 加法(+)
    case subtraction          // 減法(-)
    case multiplication       // 乗法(×)
    case division             // 除法(÷)
}
```

currentOperator の初期値は Operator.undefined とします

``` swift
var currentOperator = Operator.undefined
```

## 数値の入力

数字ボタンが押されたら、numberButtonTapped(_:)メソッドが呼ばれます。ここでどのキーが押されたかにより、数値を決めます

* ボタンのtitleより数値を設定する

``` swift
@IBAction func numberButtonTapped(_ sender: UIButton) {
    var value = 0
        switch sender.currentTitle! {
            case "0":
                value = 0
            case "1":
                value = 1
            case "2":
                value = 2
            case "3":
                value = 3
            case "4":
                value = 4
            case "5":
                value = 5
            case "6":
                value = 6
            case "7":
                value = 7
            case "8":
                value = 8
            case "9":
                value = 9
            default:
                value = 0
        }
}
```

数値を代入する変数 value を定義し、そこに数値をセットします  

* 数値１入力と数値２入力の判定

    * 数値１の入力か、数値２の入力か判定するのは、演算子が入力されているかどうかで判定します。  
    * currentOperator が undefined の時は数値１、そうでない時は数値２と判定します。
又、数値が連続入力された時は、今まで入っている数字を 10倍して、加算して、ラベルに表示します  

以下の様になります

``` swift
        if currentOperator == .undefined {
            firstValue = firstValue * 10 + value
            label.text = "\(firstValue)"
        } else {
            secondValue = secondValue * 10 + value
            label.text = "\(secondValue)"
        }
```

## 演算子の入力

演算子の入力があると operatorButtonTapped(_ :)メソッドが呼ばれます。数値ボタンの時と同じ様に、
ボタンの title でどのキーが押されたかを判定して currentOperator プロパティに設定します

``` swift
@IBAction func operatorButtonTapped(_ sender: UIButton) {
        switch sender.currentTitle! {
            case "+":
                currentOperator = .addition
            case "-":
                currentOperator = .subtraction
            case "×":
                currentOperator = .multiplication
            case "÷":
                currentOperator = .division
            default:
                currentOperator = .undefined
        }
    }
```

## 計算実行と結果の表示

= ボタンが押されたら、equalButtonTapped(_ :) メソッドが呼ばれます。ここで計算処理の実行とラベルへの結果表示を行います


``` swift
@IBAction func equalButtonTapped(_ sender: UIButton) {
        var value = 0
        switch currentOperator {
            case .addition:
                value = firstValue + secondValue
            case .subtraction:
                value = firstValue - secondValue
            case .multiplication:
                value = firstValue * secondValue
            case .division:
                value = firstValue / secondValue
            case .undefined:
                value = firstValue
        }
        label.text = "\(value)"
        firstValue = value
        secondValue = 0
        currentOperator = .undefined
    }
```

演算子がまだ押されていない時に = ボタンが推された時は firstValue をそのままセットします  
結果をラベルに表示して、クリアします

## オールクリア処理

AC ボタンが押されたら、allClearButtonTapped(_ :)メソッドが呼ばれます。
ここで、全てのプロパティをクリアし、ラベルもクリアします

``` swift
@IBAction func allClearButtonTapped(_ sender: UIButton) {
        firstValue = 0
        secondValue = 0
        currentOperator = .undefined
        label.text = "0"
    }
```

## 動作確認

できたら動作させてみて下さい  
