# アウトレットとアクションの接続
* ラベルとボタンにアウトレットとアクションを接続します
* 画面をアシスタントエディタに切り替えます。画面が狭くなるので、ドキュメントアウトラインエリアを消しても構いません

## ラベルにアウトレットを接続する
* ラベルに以下の様にアウトレットを接続します
* control を押しながらラベルをクリックして、プログラムの上の方Classの直下に落とします。ポップアップに以下の様に入れ、connect を押します


|項目         |内容            |
|-------------|----------------|
|Connection   |Outlet          |
|Name         |label           |
|Type         |UILabel         |
|Storage      |Weak            |

以下の様にできます

``` swift
    @IBOutlet weak var label: UILabel!
```

## ボタンにアクションを接続する
* ボタンにアクションを接続して、処理を記述します。今回は、各ボタンそれぞれに別々のアクションを接続します
* control を押しながら、10秒ボタンをクリックしてプログラムの最後の方にアクションを作成します
* ポップアップが出たら、以下の様に入れて connect ボタンを押します

|項目         |内容            |
|-------------|----------------|
|Connection   |Action          |
|Name         |tenSecButtonTapped     |
|Type         |UIButton         |
|Event        |Touch Up Inside  |

* 同じ様に残りのボタンも行います

|部品        |項目         |内容                   |
|-----------|-------------|-----------------------|
|3分ボタン   |Name         |threeMinButtonTapped     |
|5分ボタン   |Name         |fiveMinButtonTapped     |


* 以下の様にできます

``` swift
    @IBAction func tenSecButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func threeMinButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func fiveMinButtonTapped(_ sender: Any) {
    }
    
```
