# プログラムの記述
* これからタイマー処理を記述します
* タイマーを実現する為に、Timerクラスを利用します
* 1秒毎に呼び出される様にして残り時間を表示します

## タイマー処理の流れ
* タイマー処理の全体的な流れは以下の様になります

1. ボタンが押されると、タイマー開始処理 start(seconds: Int) メソッドが実行されます
    * どのボタンが押されたかにより、現在時間を初期設定します
    * 残り時間をラベルに表示します
    * Timerクラスをスタートします
        * Timer クラスには待機時間と実行メソッドを指定します
        * 待機時間は 1秒、実行メソッドは update() とします
2. タイマー実行処理 update() メソッドの処理
    * 現在時間を 1 マイナスします
    * ラベルに現在時間を表示します
    * 現在時間が 0 になったらアラームを鳴らします

## プロパティの定義
* まず以下の２つのプロパティを準備します

    * タイマークラスのインスタンスを保持するプロパティ
    * 現在時間を保持するプロパティ


``` swift
    var timer: Timer?;
    var currentSeconds = 0;
```

## タイマー開始処理
* start(sends: Int) メソッドを作成します

``` swift
    func start(seconds: Int) {
    }
```

引数は測定時間を秒で指定します

* このメソッドを各ボタンが押された時に呼ぶ様にセットします

``` swift
    @IBAction func tenSecButtonTapped(_ sender: UIButton) {
        start( seconds:10 );
    }
    
    @IBAction func threeeMinButtonTapped(_ sender: UIButton) {
        start( seconds:180 );
    }
    
    @IBAction func fiveMinButtonTapped(_ sender: Any) {
        start( seconds:300 );
    }
```

### どのボタンが押されたかにより、現在時間を初期設定します

``` swift
    func start(seconds: Int) {
        currentSeconds = seconds
    }
```

### 残り時間をラベルに表示します

``` swift
    label.text = "残り\(currentSeconds)"
```

### Timerクラスをスタートします
* Timer クラスの以下のメソッドを利用します

``` swift
    scheduledTimer(timeInterval: TimeInterval, target: Any, selector: Selector, userInfo: Any?, repeats:Bool) -> Timer
```
 
* 引数


|引数名        |型           |セットする内容            |
|-------------|-------------|--------------------------|
|timeInterval |TimeInterval |メソッドを実行するまでの待機時間を秒数で指定する |
|target       |Any          |実行するインスタンス |
|selector     |Selector     |実行するメソッド   |
|userInfo     |Any?         |タイマーに渡す情報、セットしなくても良い |
|repeats      |Bool         |繰り返すかどうか？ true:繰り返す、false:繰り返さない |


* selector 型
    * selector は以下の様に指定します。引数のメソッド名は存在しないとコンパイルエラーとなります

```
    #selector( クラス名.メソッド名 )
```

* 以下の様になります

``` swift
    func start(seconds: Int) {
        currentSeconds = seconds
        label.text = "残り\(currentSeconds)"
        timer = Timer.scheduledTimer(timeInterval: 1.0 , target: self, 
                 selector: #selector(ViewController.update), userInfo: nil, repeats: true)
    }
    func update() {
    }
```

## タイマー実行処理

### 現在時間を 1 マイナスします

``` swift
    func update() {
        currentSeconds -= 1
    }
```

### ラベルに現在時間を表示します

``` swift
        label.text = "残り\(currentSeconds)秒"
```

### 現在時間が 0 になったらアラームを鳴らします

* 現在時間が 0 になったらアラームを鳴らすのですが、その前に、タイマーをストップします
* タイマーを停止するには、以下のメソッドを呼びます

``` swift
    invalidate()
```

* アラームは AudioToolbox を使いますので、import しておきます

``` swift
import AudioToolbox
```

* アラーム音の再生

システム効果音を再生するには、以下の関数を使います

``` swift
    AudioServicesPlayAlertSound(_ inSystemSoundID: SystemSoundID )
```

* 引数

|引数名        |型          |セットする内容            |
|-------------|------------|--------------------------|
|inSystemSoundID|SystemSoundID|再生する効果音のID      |


SystemSoundID は 1000から1300 までの数字で表され、決められた音が準備されています

[[http://dev.classmethod.jp/smartphone/ios-systemsound/]]


* 次の様になります

``` swift
    func update() {
        currentSeconds -= 1
        label.text = "残り\(currentSeconds)秒"
        if currentSeconds == 0 {
            timer?.invalidate()
            let soundId: SystemSoundID = 1005
            AudioServicesPlayAlertSound( soundId )
        }
    }
```

### 動作確認
できたら動作確認してください。カスタマイズ、デバッッグしてみてください




