# プロジェクトの作成

* 以下のプロジェクトを作成します
    * テンプレートは Single View Application で作成します

 | 項目                    |内容                        |
 |------------------------|----------------------------|
 |Product Name            |MyMap                       |
 |Organization Name       |oecxxxx(ユーザ名)            |
 |Organization Identifier | jp.co.oec_o.oecxxxx        |
 |Language                | Swift                      |
 |Devices                 | iPhone                     |

* 上記の内容で新規プロジェクトを作成してください