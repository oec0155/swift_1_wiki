# マップビューのアウトレット接続

* マップビューのアウトレットを接続します

* control を押しながらマップビューをクリックして、プログラムの上の方Classの直下に落とします。ポップアップに以下の様に入れ、connect を押します


|項目         |内容            |
|-------------|----------------|
|Connection   |Outlet          |
|Name         |mapView         |
|Type         |MKMapView       |
|Storage      |Weak            |

以下の様にできます

``` swift
    @IBOutlet weak var mapView: MKMapView!
```
