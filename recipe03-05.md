# CoreLocation フレームワークを使った現在地の取得
* 位置情報を取得するには、ユーザの許可をもらう必要があります
* ユーザから許可を取得する為のコーディングを行います

## ユーザの許可の取得

### 許可には以下の種類があります

|許可の種類  |内容              |
|-----------|------------------|
|When In Use |使用中のみ許可をする |
|Always      |常に許可、バックグラウンドでも取得する事を許可する |
|Never       |許可しない        |

今回は、使用中のみ許可をもらいます

### 位置情報を取得する理由を設定する

ユーザから許可を取得する時にユーザに表示する文章を登録します

次の様にして画面を出します

* プロジェクトナビゲータの一番上のプロジェクトを選択する
* Info タブを選択する
* Custom iOS Target Properties セクションに次を追加する

|Key         |Value                      |
|------------|---------------------------|
|Privacy - Location when In Use Usage Description |現在地周辺のマップを表示する機能に使います。 |

新規にキーを追加するには、どれかの上にマウスを移動し、＋記号が現れたら、それをクリックします。  
キーは上下矢印で一覧が出ますので、その中から選んでください  
キーができたら、Value のところに上の文章を入力します

## ユーザ許可取得処理
ユーザに表示する文章が登録できたら、次に、許可を取得する処理をコーディングします。  
最初の画面が表示されたら最初に実行されるメソッド、ViewController.swift の ViewDidLoad() の中に書きます


* ロケーションマネージャーのインスタンスの作成

``` swift
    let manager = CLLocationManager();
```

* 現在の許可状態を取得するのは、以下のメソッドです

``` swift
    CLLocationManager.authorizationStatus() -> CLAuthorizationStatus
```

* CLAuthorizationStatus は列挙型で以下の値が定義されています

|列挙子     |内容                           |
|----------|-------------------------------|
|.notDetermined |まだ１度も許可の操作を行っていない |
|.restricted    |設定 > 一般 > 機能制限　で制限されている  |
|.denied        |明示的に拒否されている        |
|.authorizedAlways|常時許可されている         |
|.authorizedWhenInUse|アプリ実行中のみ許可されている|

現在の許可の状態が .authorizedWhenInUse でない時は、ダイアログを出して、ユーザから許可を取得します

* ダイアログを出すのは CLLocationManagerクラス の以下のメソッドです

``` swift
    requestWhenInUseAuthorization()
```

ViewController の viewDidLoad() メソッドに処理を組み込みます

``` swift
    let manager = CLLocationManager();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        }
    }
```



これで先ほど定義した文章がダイアログに出て、ユーザに許可を求めます  
（ここで、不許可にした場合、次回も同じダイアログが出そうですが、上記メソッドはステータスが .notdetermined の時だけダイアログを出します。従って、次回からはダイアログは出ません。一旦、不許可にしたものを、もう一度許可にしたい時は、端末の設定画面で行います）

ダイアログを出した後、処理は先に進みます。ユーザが入力を行うと、次のコールバックメソッドが呼ばれます  
呼ばれるコールバックメソッドは CLLocationManagerDelegate プロトコルで定義されています

``` swift
func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus )
```

引数

|引数          |型            |内容               |
|--------------|--------------|-------------------|
|manager       |CLLocationManager|  |CLLocationManagerのインスタンスが渡されます |
|status        |CLAuthorizationStatus|変更後の許可ステータスが渡されます |


* プロトコルの実装

ユーザからの入力を受け取る為には、上記のプロトコルを定義して、コールバックメソッドを実装します  

ViewController に上記のメソッドを実装します

``` swift
class ViewController: UIViewController , CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}
```

* コールバックメソッドの処理

CLAuthorizationStatus をチェックして許可されたかどうかチェックして .authorizedWhenInUse の場合は位置情報の取得を行います  


## 位置情報取得処理


位置情報の取得は CLLocationManagerクラス の以下のメソッドを呼びます

``` swift
    requestLocation()
```


それでは位置情報取得処理を組み込みます

``` swift
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
```

* 位置情報取得時処理

位置情報の取得も、少し時間がかかりますので、結果が取得できると以下のメソッドがコールバックされます  
プロトコルは既に設定されている CLLocationManagerDelegate です


|位置情報取得処理結果|コールバックメソッド        |
|------------------|---------------------------|
|成功               |locationManager(_: didUpdateLocations: )  |
|失敗               |locationManager(_: didFailWithError: )    |

``` swift
func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) 
func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
```



引数

|引数          |型            |内容               |
|--------------|--------------|-------------------|
|manager       |CLLocationManager |CLLocationManager のインスタンスが渡される |
|locations     |[CLLocation]  |現在の位置を表すCLLocationの配列が渡されます。つまり、複数の位置情報が取れる事があります|
|error         |Error         |エラーの内容が返ります |


では、上記のメソッドを実装します

``` swift
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations : \(locations)")
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error = \(error)")
    }
```

処理は、とりあえず取得した内容をコンソール表示するだけです  
位置情報は複数取れる事がありますので、配列で返ります  
今回は最初のデータを使用します


## マップビューに現在地を表示する

正常に位置情報が取得できたら、取得できた現在地のデータから、現在地周辺の地図を表示します  

位置情報から、地図上の範囲へは以下の関数で変換します

``` swift
MKCordinateRegionMakeWithDistance(_ centerCoordinate: CLLocationCoordinate2D, _ latitudinalMeters: CLLocationDistance, _ longitudinalMeters: CLLocationDistance) -> MKCoordinateRegion
```

引数

|引数               |型                    |内容     |
|-------------------|---------------------|------------------|
|centerCoordinate   |CLLocationCoordinate2D|中心座標    |
|latitudinalMeters  |CLLocationDistance   |緯度方向の長さ（メートル） |
|longitudinalMeters |CLLocationDistance   |経度方向の長さ（メートル） |



取得できた範囲の地図を表示する様にMapViewに指示するのは以下のメソッドです

``` swift
    setRegion(_ region: MKCoordinateRegion, animated: Bool)
```

引数

|引数     |型               |内容     |
|--------|-----------------|------------------|
|region  |MKCoordinateRegion|表示範囲    |
|animated|Bool               |表示する時にアニメーションするかどうか|

では表示処理を組み込んでみます

``` swift
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations : \(locations)")
        let current = locations[0];
        let region = MKCoordinateRegionMakeWithDistance(current.coordinate , 500, 500)
        mapView.setRegion(region, animated: true)
    }
```

## 動作確認

### 最初はシミュレータで実行してみます

位置がどこかの街になっているので、現在地を変更します

Debug → Location → Custom Location を選択

以下の様に設定

|緯度経度   |値           |
|----------|--------------|
|Latitude  |34.661989  |
|Longitude | 133.928963   |

これで再度実行してみてください  
天満屋岡山店周辺の地図が出ればOKです  

### 次に実機で実行してみます
iPhoneアプリを実機で事項するには、次の様にして、アプリにサインをする必要があります  


* プロジェクトナビゲータでMyAppプロジェクトを選択
* General タグを選択
* Signing セクションの Add Account... ボタンを押す

Xcode の設定画面が出るので AppleIDとパスワードを登録します  
Xcode の設定画面を閉じます

* Automatically manage signing にチェックが付いている事を確認する
* Team を(Personal Team) に変更する

以上で実行してみてください


