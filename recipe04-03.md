# 一覧画面の作成

一覧画面は UITableView を使います  

## UITableView の配置

Main.storyboard を開き、 Table View を貼り付けます  
制約を設定して画面一杯に広げます。（マージンからの距離）  
但し、上側の距離は、右の矢印を押して、デフォルトの Top Layout Guide からではなく、View を選択してください

## UITableViewCell の配置

次に、テーブルビューの中に　UITableViewCell を配置します。実際に表示する View はこの UITableViewCell に描画する様になります  
Table View Cell をドラッグして Table View の上に落とします

今落とした Table View Cell を選択して、アトリビュートインスペクタで名前をつけます

|アトリビュート名|内容             |
|---------------|----------------|
|identifier     |NameCell        |

## UITableView のアウトレット接続の作成

今までのアウトレットの作成と同じく、アシスタントエディタに切り替え、Table View から ViewController.swift に Control を押しながら、ドラッグドロップします  
以下の様に名前をつけます

|項目           |内容             |
|---------------|----------------|
|Connection     |Outlet          |
|Name           |tableView       |
|Type           |UITableView     |



