# データの作成

一覧と詳細画面に表示する為の動物データを作成します  
今回は簡単の為に、 ViewController.swift の中に、ストラクチャーを定義します。（class の外側に定義してください）  
定義できたら、その配列でデータを保持します  

## ストラクチャーの定義

以下の２つの値を保持するストラクチャーを定義します

* 動物名
* 動物の詳細情報

以下の様に定義します

``` swift
    struct AnimalInfo {
        var name: String
        var description: String
    }
    
```

このストラクチャーの配列を定義します

``` swift
    let items = [
        AnimalInfo(name: "ライオン", description: "百獣の王。一般的に最も強い動物として知られている。"),
        AnimalInfo(name: "サイ", description: "頭部に硬い角を持っている。巨体に似合わず最高時速50Kmで走る。"),
        AnimalInfo(name: "シマウマ", description: "白黒の縞模様を持つ動物。視覚や嗅覚、聴覚が優れている。"),
        AnimalInfo(name: "キリン", description: "もっとも背が高い動物。首が長いところが特徴。"),
        AnimalInfo(name: "ゾウ", description: "陸生動物では世界最大の動物。鼻は立っていても地面に届くほどに長い。")
    ]
```




