# デリゲートの設定

Table View では、いろいろな事を自動的にやってくれる様になっているのですが、各自がプログラムの中で行わないといけない事もあります。それらは、デリゲートで各自が実装する様になっています。  
それらは、以下の２つのプロトコルに定義されています

|プロトコル名   |内容                |
|--------------|--------------------|
|UITableViewDataSource |テーブルビューの内容表示に関する処理 |
|UITableViewDelegate   |テーブルビューのユーザ操作に伴う動きに関する処理  |

UITableViewDataSource プロトコルには、必須メソッドがあり、それは必ず実装する必要があります

通常これらのプロトコルは ViewController に定義して、その中にメソッドを実装させます。そして、UITableViewにどこに実装されているか知らせる為に、以下のプロパティに設定します

|プロパティ     |実装しているプロトコル |
|--------------|---------------------|
|dataSource    |UITableViewDataSource |
|delegate      |UITableViewDelegate   |



## UITableViewDataSource のメソッド

UITableViewDataSource プロトコルでは、以下で説明する実装必須メソッドが定義されています

とりあえず、UITableViewDataSource プロトコルを定義してみましょう

``` swift
class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }


}
```

実装必須メソッドが定義されていないので、エラーが出ます  
実装必須メソッドは以下の２つです

``` swift
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|tableView     |UITableView       |UITableView のインスタンスが渡される |
|section       |Int               |セクション番号  |
|indexPath     |IndexPath         |表示に使われる Cell の IndexPath（セクション、行）|



ここでセクションというのが出ていますが、iOSのテーブルは複数のグループのテーブル表示ができる機能を持っています。例えば以下の様なテーブルができます

|行                 |内容          |セクション |
|-------------------|--------------|---------|
|へダー　            |岡山県         |0|
|行                 |　岡山市       |0|
|行                 |　倉敷市        |0|
|フッター            |岡山県合計     |0|
|へダー　            |香川県         |1|
|行                 |　高松市       |1|
|行                 |　丸亀市        |1|
|フッター            |香川県合計     |1|


IndexPath は　セクションと行の両方の情報を持つクラスです


### tableView(_:numberOfRowsInSection:) -> Int メソッド

今回のサンプルはセクションは１つでへダー、フッターはありません  
動物一覧の配列のアイテム数なので以下の様になります

``` swift
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
```

### tableView(_:cellForRowAt:) -> UITableViewCell メソッド

次のメソッドは、指定の行の Cell を返すメソッドです。つまり、テーブルの各行の中身を作成して返します。このメソッドを呼んで、UITableViewはテーブルを表示しています  
この処理では、 UITableViewCell クラスのインスタンスを作成し、その上に、ビューをセットし、データの内容を表示する必要があります。しかし、簡単にそれらの処理ができる仕組みが準備されています

* UITableViewCell を作成するメソッド

``` swift
    dequeReusableCell(withIdentifier: String) -> UITableViewCell
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|withIdentifier|String           |セルの identifier を渡す |



UITableViewCell は各行毎にインスタンスが生成されて、データがセットされます。  
しかし、画面から外れた Cell は不要になりますので、再使用します。  
無ければ新たにインスタンスを生成し、返すし、既に不要となった Cell があれば、それを返すのがこのメソッドです

* IndexPath 

指定された行のデータを表示する為に、引数に　Indexpath を受け取ります  
IndexPath はテーブルの行を特定します。  
iOSのテーブルは、前に説明した様に、行だけで、特定できません。セクションと何行目かで表示するデータが特定できます。  
IndexPath はプロパティに section , row を持っているので、これで表示すべきデータが特定できます。  
今回のサンプルは１セクションなので、 section は使はず、行数( row )だけで特定します

``` swift
    indexPath.row
```

* UITableViewCell へデータの表示

UITableViewCell が取得でき、IndexPath で表示すべきデータもわかったので、どの様にして表示するかです。  
UITableViewCell は事前にデータ表示用のラベルを内部に持っており、そこにセットすると簡単に表示する事ができます。  
（表示内容を細かく設定する場合は、UITableViewCell を継承した独自Cell を作成し、そこに各種のビューをセットします）

* UITableViewCell のプロパティ

|プロパティ     |型       |内容           |
|--------------|---------|---------------|
|textLabel     |UILabel  |文字列表示     |
|detailTextLabel|UILabel |文字列表示     |
|imageView     |UIImageView|イメージ表示  |
|アクセサリ     |    |右矢印、詳細ボタン、チェックマークなどがある |

* アクセサリ

[[/uploads/AccesoryType.JPG]]


* これらの配置は style プロパティで変更する事ができます


[[/uploads/CellStyle.JPG]]



次のようにメソッドを定義してみます

``` swift
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NameCell")
        let item = items[indexPath.row];
        cell.textLabel?.text = item.name
        return cell
    }
```

必須メソッドが実装できたらエラーが消えますので、一旦起動してみてください

## UITableViewDelegate のメソッド

今回は使いませんが、もう一つのデリゲートには、実装必須メソッドはないのですが、重要なメソッドがありますので紹介しておきます  
テーブルの行を選択された時に実行されるメソッドです

``` swift
func tableView(_ tableView: UITableView, didSelectRowAt indexPath:IndexPath)
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|tableView     |UITableView       |UITableView のインスタンス  |
|indesxPath    |IndexPath         |選択された行の IndexPath  |


ある行が選択されると、IndexPath にセクションと行が設定されてこのメソッドが呼ばれます  
各アプリに応じて必要な処理をここに実装します

