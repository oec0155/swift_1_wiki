# 詳細画面の作成

## 詳細画面の作成

Main.storyboard 画面を出し、オブジェクトライブラリから ViewController を選択して、現在の画面の右側にドラッグドロップします  

これで画面は２つ出来たのですが、新しい画面用の ViewController がありません。これを作成します

### 詳細画面用の ViewController の作成 

まず新しい ViewController を作成します

* Xcode のメニューから File -> New -> File を選択  
* テンプレートは iOS の Cocoa Touch Class を選択して Next を押す
* 次の画面で以下の様に入れる

|項目         |内容         |
|-------------|---------------|
|Class        |DetailViewController |
|Subclass of  |UIViewController  |
|Language     |Swift     |

上の様に入れたら Next を押す

* 保存場所を聞いてきますので、Animalsの下に保存しますので、そのまま Create を押します

以上でプロジェクトナビゲータに新しく、DetailViewController.swift が出来ていると思います  

次に詳細画面と今作成した DetailViewController.swift を接続します  
Main.storyboard を開いてください

* 詳細画面を選択して、上部にある左端の黄色いアイコン（View Controller）を選択する
* アイデンティティインスペクタを選択する
* Custom Class の Class 欄の右のセレクトボタンを押すと、候補が出ます。この中から、先程作成した、DetailViweController を選択してください

これで詳細画面のViewController の接続ができました。最初に自動的に作成されていた画面と同じ様に、オブジェクトを貼り付けたり、アウトレットやアクションを作成する事ができます  


### 詳細画面のレイアウトの作成

詳細画面はラベルを画面に配置し、そこにアニマルの詳細情報を表示します

* オブジェクトライブラリから Label を詳細画面にドラッグドロップしてください
* Label に以下の制約を設定します

|オブジェクト  |制約                       |
|-------------|---------------------------|
|Label        |Pin画面　上:100、左:0、右:0 (マージンからの距離)  |

* Label に以下のアトリビュートを設定します

|項目         |内容                 |
|------------|---------------------|
|Lines       | 0 （何行でも表示する） |


次にアウトレットを作成しますので、DetailViewController.swift を Optionキーを押しながらクリックしてください  
アシスタントエディタ画面になります  
Label を control を押しながらクリックして、DetailViewController の class の直下にドロップしてください。

* Label に以下のアウトレットを作成します

|項目         |内容                 |
|------------|---------------------|
|Connection  |Outlet |
|Name        |label   |
|Type        |UILabel |

上の様に入れたら Connect ボタンを押します

