# 画面遷移時のデータの受け渡し

画面の遷移は出来ましたが、詳細画面にデータが表示されていません。データは一覧画面のViewControllerが保持しているので、DetailViewController へデータを渡す必要があります

## 変数の準備

DetailViewController に変数を定義します

``` swift
    var info: AnimalInfo!
```

## 値の受け渡し

画面遷移ではセグエを使います。セグエで遷移する直前に呼ばれるメソッドが準備されています。このメソッドでは遷移先の画面（DetailViewController）の値にアクセスできます

``` swift
    func prepare(for segue: UIStoryboardSegue, sender: Any?
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|for           |UIStoryboardSegue |セグエ遷移する時のセグエの情報が渡される    |
|sender        |Any?              |セグエ遷移を起動した時に渡されたパラメータオブジェクト |

ではこのメソッドを一覧画面のViewController に実装して、詳細画面の変数 info に AnimalInfo をセットします  
ViewController.swift を開いて以下のメソッドを追加してください

``` swift
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedRow = tableView.indexPathForSelectedRow {
            let controller = segue.destination as! DetailViewController
            controller.info = items[selectedRow.row]
        }
    }
```

* テーブルが選択されたのであれば、tableView.indexPathForSelectedRow でその IndexPath が取れるのですが、必ずセットされている保証はないので、Optional 型が帰りますので、Optional Binding で処理します。
* ここで、遷移先は segue.destination で取れるのですが、スーパークラスの UIViewController が取れます。ここでは実際には DetailViewController ですので強制的にダウンキャストする為に as! をつけます。  
* 次の行で、取得した遷移先のViewController の中で先程定義した info 値をセットしています。

## 受け取った値を使って描画

詳細画面では、View が表示された時のメソッド、ViewDidLoad() が既に作成されています。この中で、描画の処理を行います。  
描画は以下の様に行います

* ナビゲーションバーの中央にアニマル名を表示
* ラベルにアニマルの説明を表示

次の様にコーディングします

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = info.name
        label.text = info.description
    }
```

では実行してみてください


