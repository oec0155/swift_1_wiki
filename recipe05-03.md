# ユーザ許可の設定

iOSデバイス上の写真を取得するにはユーザの許可を取得する必要があります  

前にマップアプリで行ったのと同じ方法で行います  

## ユーザ許可の説明文の設定  

最初にユーザ許可の説明文の設定を行います  


* プロジェクトナビゲータの一番上のプロジェクトを選択する
* Info タブを選択する
* Custom iOS Target Properties セクションに次を追加する

|Key         |Value                      |
|------------|---------------------------|
|Privacy - Photo Library Usage Description |写真を一覧で表示する機能に使います。 |

新規にキーを追加するには、どれかの上にマウスを移動し、＋記号が現れたら、それをクリックします。  
キーは上下矢印で一覧が出ますので、その中から選んでください  
キーができたら、Value のところに上の文章を入力します

## 許可の種類

* PHAuthorizationStatus は列挙型で以下の値が定義されています

|列挙子     |内容                           |
|----------|-------------------------------|
|.notDetermined |まだ１度も許可の操作を行っていない |
|.restricted    |設定 > 一般 > 機能制限　で制限されている  |
|.denied        |明示的に拒否されている        |
|.authorized    |許可されている         |

ロケーションフレームワークとは少し違います。ロケーションアウレームワークの様にバックグラウンドで動作する事がないからです

## 許可の要求

ローケーションフレームワークとよく似ていますが、少し動きが違います

PHPhotosLibraryクラスの以下のメソッドで行います

``` swift
    PHPhotoLibrary.requestAuthorization(_ handler: @escaping (PHAuthorizationStatus) -> Void )
```

引数

|引数         |型            |内容                  |
|-------------|--------------|---------------------|
|handler      |@escaping (PHAuthorizationStatus) -> Void |ユーザが許可要求に答えた時に呼ばれる処理　|


引数は関数（クロージャー）を渡します  
この関数はユーザが答えた時に遅れて（非同期に）呼ばれます。


* @escaping の意味

処理（関数）が非同期に実行されるという意味です  

* @escaping のついた処理での注意事項

循環参照に気をつける事  
関数の外の変数にアクセスする時は self が必要
  

* ViewDidLoad()メソッド内に実装してみます

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        PHPhotoLibrary.requestAuthorization { status  in
            if status == PHAuthorizationStatus.authorized {
                // 写真データ取得処理
            }
        }

    }
```

