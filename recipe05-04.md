# UICollectionView の配置とレイアウト

UICollectionView は UiCollectionViewCell と一緒に使います。  
UITableView と UITableViewCell の関係と似ています。  

## UICollectionView の配置

* Main.storyboard を開きます
* オブジェクトライブラリから Collection View をドラッグドロップして貼り付けます

## UICollectionView のレイアウト

UICollectionView を画面一杯に広げます。周りのマージンは取らない様にします

* Collection View を選択する
* Pin画面を出す
* 以下の制約を設定する

|制約   |内容                     |
|-------|-------------------------|
|距離制約|　上:0、左:0、右:0、下:0　（マージンは取らない）|

Collection View が画面一杯に広がっていればOKです  

## アウトレットの作成

Collection View のアウトレットを作成します

* option キーを押しながら ViewController.swift をクリックして Assistant View に切り替えます
* Collection View の上で control を押しながらクリック
* class の直下にドラッグドロップする
* PopUp が出るので以下の様に入れる

|項目           |内容             |
|---------------|----------------|
|Connection     |Outlet          |
|Name           |collectionView       |
|Type           |UICollectionView     |









