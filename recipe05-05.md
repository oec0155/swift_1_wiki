# UICollectionViewCell の追加

UICollectionViewCell は UICollectionView を配置した時に、左上に既に入っています  


## アトリビュートの設定

これに名前をつけます。  

* UICollectionViewCell を選択
* アトリビュートインスペクタを開く
* 以下の様にアトリビュートを設定する  

|項目          |内容              |
|--------------|------------------|
|identifier    |ImageCell         |

## UICollectionVieCell のレイアウトを設定する

TableViewCell の時の様に、事前に準備されている物はありません。自分でレイアウトをします  
今回はこの Cell の中に写真を表示する為の、ImageView を貼り付けます。

* UICollectionViewCell を選択して、オブジェクトライブラリから Image View をドラッグドロップしてください

ImageView の Cell の中のレイアウトも同じ様に制約で設定します。

* ImageView を選択
* 以下の制約を登録する

|オブジェクト  |制約               |
|-------------|-------------------|
|ImageView    |距離制約　上:0、左:0、右:0、下:0　（マージンは含まず） |

ImageView が Cell の中に一杯に広がればOKです

## ImagView に Tag番号 を設定する

今回は Cell の中には ImageView が一つあるだけですが、この中には通常の View と同じ様に、自由に、複数のオブジェクトを配置する事ができます。その場合、それぞれのオブジェクトへアクセスする時に Tag番号をつけて、アクセスする事ができます

* ImageView を選択する
* アトリビュートインスペクタを出す

|オブジェクト  |制約               |
|-------------|-------------------|
|ImageView    |Tag: 1　 |

この番号でプログラムからアクセスします


なお、ImageViewには次のアトリビュートも指定します

|オブジェクト  |制約               |
|-------------|-------------------|
|ImageView    |Content Mode: Aspect Fill　 |

これはイメージデータと表示エリアの大きさが違う場合、どのように表示するかです



なお、ここでのやり方は、UITableView,UITableViewCell でも同じ様にできます


