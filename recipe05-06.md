# UICollectionView のデリゲートの使用

UICollectionView も UITableView と同じ様に以下のプロトコルが準備されています

## UICollectionView のプロトコル

UICollectionView には以下のプロトコルが準備されています


|プロトコル名   |説明                   |プロパティ |
|--------------|-----------------------|----------|
|UICollectionViewDataSource|要素の数を返すメソッドや、表示するセルを作成するメソッドなど |dataSource|
|UICollectionViewDelegate  |セルを選択し時の動作などを定義するメソッドなど | delegate |

ここまでは UITableView と同じです。しかし、UICollectionView には以下のプロトコルがあります

|プロトコル名   |説明                   |プロパティ |
|--------------|-----------------------|----------|
|UICollectionViewDelegateFlowLayout | セルのサイズやマージンを返すメソッドなど　| delegate |

このプロパティは UIColectionViewDelegate のサブクラスですので、こちらを定義したら、先のUICollectionViewDelegate は不要です

では、TableView の時と同じ様に、UICollectionViewDataSourceと UICollectionViewDelegateFlowLayout を ViewControllerに定義します

``` swift
class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
```

## UICollectionViewDataSource の実装必須メソッド

UICollectionViewDataSource には以下の実装必須メソッドがあります

``` swift
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|collectionView|UICollectionView  |UICollectionView のインスタンスが返される |
|section       |Int               |セクション番号 |
|indexPath     |IndexPath         |インデックスパス  |




テーブルの時とは違い、 row ではなく item になっています

これらを実装する前に、写真データを保持する変数を準備する必要があります。 
その為には Photos フレームワークを利用します

## Photos フレームワーク

写真データを扱うクラスは PHAsset クラスです
このインスタンスは写真のサイズ、拡張子、保存場所などを保持します  
今回はこの PHAsset の配列で写真データを保持します

``` swift
    var photos: [PHAsset] = []
```

## UICollectionViewDataSource の必須メソッドの実装

以下の様に実装します

``` swift
class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
       // 写真データを Cell にセットする処理        
        return cell
    }
```

この処理もほとんど Table View の時と同じです  
UICollectionViewCell もキューから再利用します  
利用するメソッドも UITableView とほぼ同じで、以下のメソッドが準備されています

``` swift
    dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|identifier    |String            |UICollectionViewCell の identifier |
|indexPath     |IndexPath         |インデックスパス    |




## UICollectionViewDelegateFlowLayout プロトコルのメソッド

セルのサイズやマージンを指定するメソッドを実装します  

* サイズやマージを指定するメソッド 


``` swift
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize 
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
```

引数


|引数          |型                |内容               |
|--------------|------------------|-------------------|
|collectionView |UICollectionView |UICollectionView のインスタンス  |
|layout        |UICollectionViewLayout |UICollectionViewLayout のインスタンス |
|indexPath     |IndexPath         |インデックスパス    |
|section       |Int               |セクション番号      |



|メソッド          |説明              |
|------------------|--------------------|
|collectionView(_:, layout:, sizeForItemAt:) -> CGSize |指定のアイテムのセルのサイズを表す CGSize インスタンスを返す |
|collectionView(_:, layout:, minimumLineSpacingForSectionAt:) -> CGFloat |指定のセクションのセルの縦のマージンを返す| 
|collectionView(_:, layout:, minimumInteritemSpacingForSectionAt:) -> CGFloat |指定のセクションのセルの横のマージンを返す| 


 
* セルのサイズは、縦、横同じで、画面サイズの 1/3 とします
* マージンは、縦、横共に 0 とします。このメソッドを実装しない場合は、規定値のマージンが取られます

以下の様に実装します

``` swift
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width / 3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
```

* 画面の幅は collectionView の bounds プロパティの size プロパティの width プロパティで求められます



