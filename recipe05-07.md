# Photos フレームワークを用いた写真データの取得

次に Photos フレームワークを使って、写真データを取得します  
写真データの取得処理は、既に作成している、以下のユーザから許可が得られた時に行います

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        PHPhotoLibrary.requestAuthorization { status  in
            if status == PHAuthorizationStatus.authorized {
                // 写真データ取得処理    ←　ここ
            }
        }
    }
```

少し処理が複雑なので、別にメソッドを作成し、そちらを呼ぶようにします

``` swift
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        PHPhotoLibrary.requestAuthorization { status  in
            if status == PHAuthorizationStatus.authorized {
                self.loadPhotos()
            }
        }
    }
    
    func loadPhotos() {

    }
```


この loadPhotos()メソッドの中で、写真データ取得処理を行います

前の章で少し説明した様に、写真データを保持するのは PHAsset クラスを使います  
PHAssetクラスには、写真データを取得する為のメソッドが準備されています

* iOS端末内の写真データを取得するメソッド

``` swift
PHAsset.fetchAssets(with mediaType: PHAssetMediaType, options: PHFetchOptions? ) -> PHFetchResult<PHAsset>
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|mediaType     |PHAssetMediaType  |取得するデータの種類を指定する |
|options       |PHFetchOptions?   |取得する条件を指定する |

* PHAssetMediaType には以下の値があります

|PHAssetMediaType |内容              |
|-----------------|------------------|
|.unknown         |不明              |
|.image           |写真データ         |
|.video           |動画データ         |
|.audio           |音声データ         |


取得した結果は PHAsset のインスタンスで返ります  

今回は、写真を取得するので、以下の様になります

``` swift
let result = PHAsset.fetchAssets(with: .image, options: nil)
```



PHFetchResult は取得したデータ<PHAsset>のリスト型です。  
PHFetchResult から PHAsset の配列を取り出すのには以下のメソッドを使います

``` swift
objects(at: IndextSet) -> [PHAsset]
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|at            |IndexSet          |取得するデータのインデックスのコレクション |


今回はすべてのデータを取り出します。  
PHFetchResult の中のデータ件数はArrayと同じように .countで取れます。  
IndexSet のイニシャライザを使って以下のようにして IndexSet を作成します

``` swift
let indexSet = IndexSet(integersIn: 0...result.count - 1)
```

この indexSet を使って、 PHFetchResult から PHAsset の配列が以下のようにして取れます

``` swift
let loadedPhotos = result.objects(at: indexSet)
```

loadedPhotos は [PHAsset] 型なので、これを変数 photos にセットします

``` swift
photos = loadedPhotos
```

これでデータが取得できたのですが、以下のようにして、UICollectionView の再描画を実行しないと、画面が反映しません

``` swift
    collectionView.reloadData()
```

しかし、これは画面描画に関する処理なので、許可処理は別スレッドで実行されていましたのでできません。  
メインスレッドキューに処理を登録して、メインスレッドで実行します。  
メインスレッドキューに処理を登録するのは次のようにします

``` swift
    DispatchQueue.main.sync {
         // メインスレッドで実行する処理
    }
```

sync は同期処理で実行する事を表します。async だと非同期で実行されます










