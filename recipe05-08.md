# セルごとの画像の表示

次に取得したデータを表示します  
UICollectionVIew の UICollectionViewDataSource プロトコル の 以下の部分を実装します


``` swift
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        // 写真データを Cell にセットする処理
        return cell
    }
```

ここでは UICollectionCell に画像データをセットします
写真データの情報は Photos に入っているので、ここから実際の画像データを取得します
画像データの情報(PHAsset)から画像データを取得するには以下のメソッドを使います

``` swift
requestImage(for asset: PHAsset,targetSize: CGSize,contentMode: PHImageContentMode,options: PHImageRequestOptions?,resultHandler: @escaping (UIImage?, [AnyHashable: Any]?) -> Void) -> PHImageRequestID
```

引数

|引数          |型                |内容               |
|--------------|------------------|-------------------|
|asset         |PHAsset           |対象の PHAsset      |
|targetSize    |CGSize            |画像のサイズ        |
|contentMode   |PHImageContentMode|リサイズが必要な場合、どのように行うか |
|options       |PHImageRequestOptions?|画像データ取得時のオプションを指定する。不要の場合はnil |
|resultHandler |@escaping(UIImage?,[AnyHashable: Any]?) -> Void) |処理後に呼び出される関数  |


* アスペクト比

実施の画像サイズと表示サイズが異なる場合、どのように変換するかを指定します。以下のものばあります

|PHImageContentMode |内容                  |
|-------------------|----------------------|
|.aspectFill        |長辺が一致するように拡大、縮小する|
|.aspectFit         |縦横のサイズが完全に一致するように拡大、縮小する |

* オプション

オプションは PHImageRequestOptions をインスタンス化し、プロパティをセットする。不要の場合は nil をセットすればよい  

データの取得を同期でするかとか、iCloudも対象にするかなどを指定します


* 処理後に呼び出される関数

この関数は以下の引数をとります

|型                |内容               |
|------------------|-------------------|
|UIImage?          |取得した画像データ  |
|[AnyHashable: Any]?|取得した画像データに関する情報 |

第一引数で取得したイメージが受け取れます。第二引数は情報が Dictionary で受け取れます。今回は情報は使いません。  
この受け取ったイメージデータをセルの ImageView にセットします。
Cellの中の View にアクセスするには、今回はタグを事前につけていますので、このタグを使ってアクセスします

``` swift
    cell.ViewWithTag(1) as! UIImageView
```

このメソッドでセルの中の指定のタグのついた View が取り出せます。取得できるのは UIView クラスなので、UIImageView に強制キャストします

では実際の処理を完成させてみます

``` swift
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        let asset = photos[indexPath.item]
        let width = collectionView.bounds.size.width / 3
        manager.requestImage(for: asset,
                             targetSize: CGSize(width: width, height: width),
                             contentMode: .aspectFill,
                             options: nil) { (result, info) in
            if let image = result {
                let imageView = cell.viewWithTag(1) as! UIImageView
                imageView.image = image
            }
        }
        return cell
    }
```

result は nil の可能性があるので、Optionalバインディングで処理します

## 動作確認

では実機で動作確認してみてください




