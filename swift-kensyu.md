# Swift入門研修

## １日目

### 1. [Macの操作方法](Mac-master)
### 2. [Swift言語入門 ](Swift)

## ２日目

### 3. [サンプルアプリの作成](Chapter02)

### 4. [電卓アプリ](recipe01)

## ３日目

### 5. [タイマーアプリ](recipe02)

### 6. [マップアプリ](recipe03)

### 7. [図鑑アプリ](recipe04)

### 8. [写真ビューアプリ](recipe05)

## おまけ

### 1. [図鑑アプリのテーブルを自由にレイアウト](omake-1)

### 2. [図鑑アプリの画面遷移を変更](omake-2)

### 3. [写真ビューアプリの Cell 上の View へのアクセス方法変更](omake-3)

### 4. [PopOver遷移](omake-4)

### 5. [CustomViewの作り方](make-5)

## おまけのおまけ

### 1. Todoアプリの説明

### 2. [mBaaSの説明](todo02)

### 3. [Todoアプリの作成](todo03)